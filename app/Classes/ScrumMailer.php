<?php

namespace App\Classes;

use Illuminate\Support\Facades\Mail;
// use App\Models\AdminGeneralsetting;
use PDF;

class ScrumMailer
{
    
    public function sendCustomMail(array $mailData)
    {

        // $setup = AdminGeneralsetting::find(1);

        $data = [
            'email_body' => $mailData['body']
        ];

        $objDemo = new \stdClass();
        $objDemo->to = $mailData['to'];
        $objDemo->from = 'info@scrumdigital.com';
        $objDemo->title = 'SkillsnMore';
        $objDemo->subject = $mailData['subject'];

        try{
            Mail::send('mailbody',$data, function ($message) use ($objDemo) {
                $message->from($objDemo->from,$objDemo->title);
                $message->to($objDemo->to);
                $message->subject($objDemo->subject);
            });
        }
        catch (Throwable $e){
            //return $e;
            report($e);
            //echo $e.message();
            die("Not sent");
        }
        return true;
    }

}