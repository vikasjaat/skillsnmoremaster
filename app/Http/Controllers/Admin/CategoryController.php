<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categories;
use App\Models\Courses;
use App\Models\InstructorCategory;
use App\Models\Instructors;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    //
    public function index()
    {
        $category = Categories::orderby('id', 'desc')->get();
        return view('admin.categories', compact('category'));
    }
    public function create($id)
    {
        if ($id == 'new') {
            $category = null;
        } else {
            $category = Categories::find($id);
            $arr = explode(" to ",$category->age_range);
            $category->start = str_replace("+","",$arr[0]);
            if(isset($arr[1])){
                $category->end = $arr[1];
            }else{
                $category->end = '';
            }
        }
        return view('admin.categories-create', compact('category'));
    }
    public function store(Request $request)
    {
        if ($request->id !=null) {
            # code...
            $request->validate([
                'name' => 'required',
                'description' => 'required',
                'first_range' => 'required',
                // 'icon' => 'required',
            ]);
        }
        else{
            $request->validate([
                'name' => 'required',
                'description' => 'required',
                'first_range' => 'required',
                'icon' => 'required',
            ]);
        }
        if ($request['id'] != null) {
            $Categories = Categories::find($request->id);
        } else {
            $Categories = new Categories();
        }
        $Categories->name = $request->name;
        $Categories['slug'] = Str::slug($request->name);
        if($request['last_range'] != ""){
            $Categories->age_range = $request['first_range'] . ' to ' . $request['last_range'];
        }else{
            $Categories->age_range = $request['first_range'] . '+';
        }
        // dd($Categories->toarray());
        if ($request->status == "") {
            $Categories->status = 0;
        } else {
            $Categories->status = $request->status;
        }
        $Categories->description = $request->description;
        if ($request->icon) {
            $file = $request->file("icon");
            $fname = str_replace('-', ' ', $file->getClientOriginalName());
            $newfilename = pathinfo($fname, PATHINFO_FILENAME) . '_' . time() . '.' . $file->getClientoriginalExtension();
            $file->storeAs('public/category', $newfilename);
            if ($request->id != null) {
                if (Storage::exists('public/category/' . $Categories->icon)) {
                    Storage::delete('public/category/' . $Categories->icon);
                }
            }
            $Categories->icon = $newfilename;
        }
        $Categories->save();
        if ($request['id'] != null) {
            session()->flash('message', 'Successfully updated Category.');
            return redirect()->route('admin-categories');
        } else {
            session()->flash('message', 'Successfully Saved Category.');
            return redirect()->route('admin-categories');
        }
    }
    public function categories_status(Request $request)
    {
        $user = Categories::find($request->user_id);
        $user->status = $request->status;
        $user->save();
        return response()->json(['success' => 'Instructor status change successfully.']);
    }
    public function delete_category($id)
    {
        $counts = Courses::where('category', $id)->count();
        $instcounts = InstructorCategory::where('category', $id)->count();
        if ($counts == 0 && $instcounts == 0) {
            $category =  Categories::find($id);
            if (Storage::exists('public/category/' . $category->icon)) {
                Storage::delete('public/category/' . $category->icon);
            }
            $category->delete();
            $message = ['success' => 'Record deleted successfully!'];
        }else{
            $message = ['error' => 'This Category has some Courses or Intructors'];
        }
        return response()->json($message);
    }
}
