<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Courses;
use App\Models\Instructors;
use App\Models\InstructorCategory;
use PhpParser\Node\Expr\FuncCall;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Models\Schedule;
use Illuminate\Support\Facades\Storage;
use App\Models\Batch;
use Carbon\Carbon;
use App\Models\UserSubscription;
use App\Models\StudentBatch;

class CoursesController extends Controller
{
    public function index()
    {
        // ->join('categories', 'courses.category', '=', 'categories.id')
        //     ->select('courses.*', 'categories.name as cat_name')

        $courseslist = Courses::orderby('id', 'desc')->join('categories', 'courses.category', '=', 'categories.id')
            ->select('courses.*', 'categories.name as cat_name')
            ->get();
        // dd($courseslist->toarray());
        return view('admin.courses', compact('courseslist',));
    }
    public function fetchinstructor($id)
    {
        $instructors['instructors'] = InstructorCategory::where('category', $id)
            ->join('instructors', 'instructor_category.instructor', '=', 'instructors.id')
            ->select('instructors.*')->get();
        return response()->json($instructors);
    }
    public function create($id)
    {
        $categories = Categories::get();
        if ($id == 'new') {
            $courses = null;
        } else {
            $courses = Courses::find($id);
            // dd($courses->toArray());
        }
        return view('admin.courses-create', compact('categories', 'courses'));
    }
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'category' => 'required',
            'instructor' => 'required',
            'first_range' => 'required',
            'price' => 'required',
            'duration' => 'required',
            'frequency' => 'required',
        ]);
        if ($request['id'] != null) {
            $courses = Courses::find($request->id);
        } else {
            $courses = new Courses();
        }
        $instructorsname = Instructors::find($request->instructor);
        $data = $instructorsname->name;
        $name = ucwords($request->name) . ' by ' . ucwords($data);
        $courses->name = $name;
        $courses['slug'] = Str::slug($request->name . '-' . $data);
        $courses->category = $request->category;
        $courses->instructor = $request->instructor;
        // $courses->age_range = $request->age_range;
        $courses->age_range = $request['first_range'] . '-' . $request['last_range'];
        $courses->price = $request->price;
        $courses->duration = $request->duration;
        $courses->frequency = $request->frequency;
        if ($request->image) {
            $file = $request->file("image");
            $fname = str_replace('-', ' ', $file->getClientOriginalName());
            $newfilename = pathinfo($fname, PATHINFO_FILENAME) . '_' . time() . '.' . $file->getClientoriginalExtension();
            $file->storeAs('public/courses', $newfilename);
            if ($request->id != null) {
                if (Storage::exists('public/courses/' . $courses->image)) {
                    Storage::delete('public/courses/' . $courses->image);
                }
            }
            $courses->image = $newfilename;
        }
        if ($request->status == "") {
            $courses->status = '0';
        } else {
            $courses->status = $request->status;
        }
        $courses->learning = $request->learning;
        $courses->summary = $request->summary;
        $courses->save();
        if ($request['id'] != null) {
            session()->flash('message', 'Successfully updated Courses.');
            return redirect()->route('admin-courses');
        } else {
            session()->flash('message', 'Successfully Saved Courses.');
            return redirect()->route('admin-courses');
        }
    }

    public function courses_status(Request $request)
    {
        $status = Courses::find($request->user_id);
        $status->status = $request->status;
        $status->save();
        return response()->json(['success' => 'courses status change successfully.']);
    }

    public function delete_courses($id)
    {
        $courses =  Courses::find($id);
        if (Storage::exists('public/courses/' . $courses->image)) {
            Storage::delete('public/courses/' . $courses->image);
        }
        $courses->forceDelete();
        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
    }
    public function schedule($id)
    {
        $course_id = Courses::find($id);
        $cat = Categories::find($course_id->category);
        return view('admin.schedule', compact('cat', 'course_id'));
    }
    public function schedulestore(Request $request)
    {
        $request->validate([
            'on_date' => 'required',
            'batch_name' => 'required',
            'class_frequency' => 'required',
            'event_name' => 'required',
            'week' => 'required',
        ]);

        $batch = $request->batch_name . ',' . rand(0, 99999);
        $day = Carbon::createFromFormat('Y-m-d', $request->on_date);
        foreach (range(1, 7 * $request->week) as $key => $next) {
            $day = $day->addDays();
            foreach ($request->schedule as $key => $val) {
                if (strtolower($day->format('l')) == 'sunday' && $request->schedule[$key] == 'sunday') {
                    $time = new Schedule();
                    $time->course_id = $request->course_id;
                    if ($request->status == "") {
                        $time->status = 0;
                    } else {
                        $time->status = $request->status;
                    }
                    $time->on_date = $request->on_date;
                    $time->batch_name = $batch;
                    $time->class_frequency = $request->class_frequency;
                    $time->event_name = $request->event_name;
                    $time->date = $day->toDateString();
                    $time->on_time = $request->on_time[$key];
                    $time->end_time = $request->end_time[$key];
                    $time->save();
                }
                if (strtolower($day->format('l')) == 'monday' && $request->schedule[$key] == 'monday') {
                    $time = new Schedule();
                    $time->course_id = $request->course_id;
                    if ($request->status == "") {
                        $time->status = 0;
                    } else {
                        $time->status = $request->status;
                    }
                    $time->on_date = $request->on_date;
                    $time->batch_name = $batch;
                    $time->class_frequency = $request->class_frequency;
                    $time->event_name = $request->event_name;
                    $time->date = $day->toDateString();
                    $time->on_time = $request->on_time[$key];
                    $time->end_time = $request->end_time[$key];
                    $time->save();
                }
                if (strtolower($day->format('l')) == 'tuesday' && $request->schedule[$key] == 'tuesday') {
                    $time = new Schedule();
                    $time->course_id = $request->course_id;
                    if ($request->status == "") {
                        $time->status = 0;
                    } else {
                        $time->status = $request->status;
                    }
                    $time->on_date = $request->on_date;
                    $time->batch_name = $batch;
                    $time->class_frequency = $request->class_frequency;
                    $time->event_name = $request->event_name;
                    $time->date = $day->toDateString();
                    $time->on_time = $request->on_time[$key];
                    $time->end_time = $request->end_time[$key];
                    $time->save();
                }
                if (strtolower($day->format('l')) == 'wednesday' && $request->schedule[$key] == 'wednesday') {
                    $time = new Schedule();
                    $time->course_id = $request->course_id;
                    if ($request->status == "") {
                        $time->status = 0;
                    } else {
                        $time->status = $request->status;
                    }
                    $time->on_date = $request->on_date;
                    $time->batch_name = $batch;
                    $time->class_frequency = $request->class_frequency;
                    $time->event_name = $request->event_name;
                    $time->date = $day->toDateString();
                    $time->on_time = $request->on_time[$key];
                    $time->end_time = $request->end_time[$key];
                    $time->save();
                }
                if (strtolower($day->format('l')) == 'thursday' && $request->schedule[$key] == 'thursday') {
                    $time = new Schedule();
                    $time->course_id = $request->course_id;
                    if ($request->status == "") {
                        $time->status = 0;
                    } else {
                        $time->status = $request->status;
                    }
                    $time->on_date = $request->on_date;
                    $time->batch_name = $batch;
                    $time->class_frequency = $request->class_frequency;
                    $time->event_name = $request->event_name;
                    $time->date = $day->toDateString();
                    $time->on_time = $request->on_time[$key];
                    $time->end_time = $request->end_time[$key];
                    $time->save();
                }
                if (strtolower($day->format('l')) == 'friday' && $request->schedule[$key] == 'friday') {
                    $time = new Schedule();
                    $time->course_id = $request->course_id;
                    if ($request->status == "") {
                        $time->status = 0;
                    } else {
                        $time->status = $request->status;
                    }
                    $time->on_date = $request->on_date;
                    $time->batch_name = $batch;
                    $time->class_frequency = $request->class_frequency;
                    $time->event_name = $request->event_name;
                    $time->date = $day->toDateString();
                    $time->on_time = $request->on_time[$key];
                    $time->end_time = $request->end_time[$key];
                    $time->save();
                }
                if (strtolower($day->format('l')) == 'saturday' && $request->schedule[$key] == 'saturday') {
                    $time = new Schedule();
                    $time->course_id = $request->course_id;
                    if ($request->status == "") {
                        $time->status = 0;
                    } else {
                        $time->status = $request->status;
                    }
                    $time->on_date = $request->on_date;
                    $time->batch_name = $batch;
                    $time->class_frequency = $request->class_frequency;
                    $time->event_name = $request->event_name;
                    $time->date = $day->toDateString();
                    $time->on_time = $request->on_time[$key];
                    $time->end_time = $request->end_time[$key];
                    $time->save();
                }
            }
        }
        return redirect('admin/courses');
    }
    public function batchindex($id)
    {
        $coursedata = Courses::find($id);
        $schedule = Schedule::where('course_id', $id)->get();
        
        return view('admin.batch', compact('schedule', 'coursedata'));
    }
    public function schedule_status(Request $request)
    {
        $schedulestatus = Schedule::find($request->user_id);
        $schedulestatus->status = $request->status;
        //  dd($schedulestatus->toarray());
        $schedulestatus->save();
        return response()->json(['success' => 'Schedule status change successfully.']);
    }
    public function batchschedule($batch_name)
    {
        $courseschedule = Schedule::where('batch_name', $batch_name)->join('courses', 'schedules.course_id', '=', 'courses.id')->get();
        $showstd = StudentBatch::where('schedule_id', $batch_name)
            ->select('schedule_id', 'user_subsc_id', 'status')->get();
        $user = [];
        foreach ($showstd as $key => $item) {
            $showstd[$key]->stud_name = UserSubscription::find($item->user_subsc_id)->toarray();
        }
        $timetable = Schedule::where('batch_name', $batch_name)->get();
        $users =[];
        foreach ($timetable as $key=>$item) {
            $users[$key] = Courses::where('id', $item->course_id)
                ->select('name', 'duration', 'image')
                ->get();
        }
        return view('admin.batch-schedule', compact('courseschedule', 'timetable', 'users', 'showstd', 'user'));
    }

    public function batchstudent($batch_name)
    {
        $batch = Schedule::where('batch_name', $batch_name)
            ->select('id', 'batch_name', 'class_frequency', 'course_id')->get();
        foreach ($batch as $item) {
            $course = UserSubscription::where('course_id', $item->course_id)
                ->select('studentName', 'course_id', 'id', 'status', 'schedule_status')->get();
            // dd($course);
        }
        return view('admin.student', compact('batch', 'course'));
    }

    public function storestudent($id, $batch_name)
    {
        $course = Schedule::where('batch_name', $batch_name)
            ->select('id', 'course_id')->get();
        $user = UserSubscription::find($id);
        $user->schedule_status = 1;
        // dd($user->toarray());
        $user->save();
        $userschedule = new StudentBatch();
        $userschedule->schedule_id = $batch_name;
        $userschedule->course_id = $user->course_id;
        $userschedule->user_subsc_id = $user->id;
        $userschedule->status = 'active';
        // dd($userschedule->toarray());
        $userschedule->save();
        return redirect()->back();
    }
}
