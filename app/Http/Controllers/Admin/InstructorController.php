<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Courses;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\FuncCall;
use Whoops\Run;
use App\Models\Instructors;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Hash;
use App\Models\InstructorCategory;
use Illuminate\Support\Facades\Storage;


class InstructorController extends Controller
{
    //
    public function index()
    {
        $instructor = Instructors::orderby('id', 'desc')->get();
        return view('admin.instructor-list', compact('instructor'));
    }
    public function create($id)
    {
        $Categories =  Categories::get();
        if ($id == 'new') {
            $instrut = null;
        } else {
            $instrut = Instructors::find($id);
            $instrut->cat = InstructorCategory::where('instructor', $id)->select('category')->get();
            $instrut->cat = array_column($instrut->cat->toArray(), 'category');
        }
        // dd($instrut->cat);
        return view('admin.instructor-create', compact('instrut', 'Categories'));
    }
    public function store(Request $request)
    { 
        
        if ($request['id'] != null) {
            $request->validate([
                'name' => 'required',
                'email' => 'required',
                "category" => 'required'
            ]);
        } else {
            $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:instructors',
                'password' => ["required", 'regex:/^.{8,}$/'],
                "category" => 'required'

            ]);
        }
        if ($request['id'] != null) {
            $instructore = Instructors::where('id', $request->id)->first();
        } else {
            $instructore = new Instructors();
        }
        $instructore->name = $request->name;
        $instructore->email = $request->email;
        $instructore->password = Hash::make($request->password);
        if ($request->status == "") {
            $instructore->status = 0;
        } else {
            $instructore->status = $request->status;
        }
        if ($request->verified == "") {
            $instructore->verified = 0;
        } else {
            $instructore->verified = $request->verified;
        }
        $instructore->summary = $request->summary;
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $filename = time() . rand(1, 100) . '.' . $file->extension();
            $file->storeAs('public/instructor', $filename);
            if ($request->id != null) {
                if (Storage::exists('public/instructor/' . $instructore->photo)) {
                    Storage::delete('public/instructor/' . $instructore->photo);
                }
            }
            $instructore->photo = $filename;
        }
        $instructore->save();
        InstructorCategory::where('instructor', $request->id)->delete();
        foreach ($request->category as $key => $value) {
            $instructor_category = new InstructorCategory();
            $instructor_category->category = $value;
            $instructor_category->instructor = $instructore->id;
            $instructor_category->save();
        }
        if ($request['id'] != null) {
            session()->flash('message', 'Successfully updated instructor.');
            return redirect()->route('admin-instructor');
        } else {
            session()->flash('message', 'Successfully saved instructor.');
            return redirect()->route('admin-instructor');
        }
    }
    public function instatus(Request $request)
    {
        $user = Instructors::find($request->user_id);
        $user->status = $request->status;
        $user->save();
        return response()->json(['success' => 'Instructor status change successfully.']);
    }

    public function insdelete($id)
    {  
        $course  = Courses::where('instructor',$id)->count();
        if ($course == 0) {
            InstructorCategory::where("instructor",$id)->delete();
           $instructor = Instructors::find($id);
           if (Storage::exists('public/instructor/' . $instructor->photo)) {
               Storage::delete('public/instructor/' . $instructor->photo);
           }
           $instructor->delete();
           $message = ['success' => 'Record deleted successfully!'];
        }
        else{
            $message = ['error' => 'This Category has some Courses '];
        }
        return response()->json($message);
    }
}
