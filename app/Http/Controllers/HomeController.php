<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Classes\ScrumMailer;
use App\Models\Courses;
use App\Models\Categories;
use App\Models\Instructors;
use App\Models\StripePayment;
use Response;
use Illuminate\Support\Facades\DB;

class HomeController extends BaseController
{
   public function index()
   {
      // no more
      $menu_index = "index_menu";
      $categories = Categories::orderBy('name')->get();
      $courses = Courses::groupBy('category')->select('name','summary','image','slug','price','category')->with('coursecategory')->orderBy('id','desc')->get();
      $instructors = Instructors::with('categories')->where('status','1')->get();
      return view('Pages.index', compact('menu_index','categories','courses','instructors'));
   }
   public function aboutus()
   {
      $menu_index = "aboutus_menu";
      $instructors = Instructors::with('categories')->get();
      return view('Pages.about-us', compact('menu_index','instructors'));
   }
   public function contactus()
   {
      $menu_index = "contact_menu";
      $instructors = Instructors::with('categories')->get();
      return view('Pages.contact-us',compact('menu_index','instructors'));
   }
 public function termscondition(){
   return view('Pages.terms-and-condition');
 }

public function privacypolicy()
{
   return view('Pages.privacy-policy');
}

   public function faq()
   {
      $instructors = Instructors::with('categories')->get();
      return view('Pages.faq',compact('instructors'));
   }


   public function instructorprofile($slug)
   {
      $instructor = Instructors::where('slug',$slug)->with('categories')->with('gallery')->first();
      $courses = Courses::where('instructor',$instructor->id)->get();
      return view('front.instructor-profile',compact('instructor','courses'));
   }
   public function signupTeacher(Request $request)
   {
      $input = $request->all();
      // dd($input);
      $response = "";
      foreach ($input as $key => $detail) {
         $response .= $key . ": " . $detail . "<br>";
      }
      $subject = env('email_subject');
      $msg = serialize($input);

      $data = [
         'to' => env('email_to'),
         'subject' => $subject,
         'body' => $response,
      ];

      $mailer = new ScrumMailer();
      $mailer->sendCustomMail($data);

      return response()->json(['status' => 'success', 'message' => 'Thank you for connecting with us! One of our representative will get in touch with you, whitin next 24 hours.']);
   }

   public function courseDetailPage($course_name)
   {
      $course = Courses::where('slug',$course_name)->with('instructorDetail')->firstorFail();
      $country_list = DB::table('countries_list')->get();
      $courses = Courses::with('coursecategory')->orderBy('id','desc')->paginate('10');
    //   echo "<pre>";print_r($courses);die;
      return view('front.courseDetail',compact('course','country_list','courses'));
   }

   public function courseCategory($category  = null)
   {
      $category = Categories::where('slug',$category)->first();
      $categories = Categories::get();
      // dd($categories);
      $courses = Courses::when($category,function($q) use($category){
         $q->where('category',$category->id);
      })->paginate('50');
      return view('front.categories',compact('courses','categories','category'));
   }

   public function getcities(Request $request)
   {
     $stateid = $request->stateid;
     $city_list = DB::table('cities')->where('state_id','=',$stateid)->get();
     return $city_list;
   }
   public function getstates(Request $request)
   {

     $country = $request->countryid;

     $state_list = DB::table('states')->where('country_id','=',$country)->get();
     return $state_list;
   }

   public function scheduleDemo($course_name = null){
      $country_list = DB::table('countries_list')->orderBy('name')->get();
      $categories = Categories::get();
      $current = date('Y-m-d');
      $saturday = date('Y-m-d',strtotime($current.'next saturday'));
      $nextsaturday = date('Y-m-d',strtotime($saturday.'next saturday'));
      $dateArr = [];
      $dateArr[] = date('M d, Y',strtotime($saturday));
      $dateArr[] = date('M d, Y',strtotime($saturday.'+ 1 day'));
      $dateArr[] = date('M d, Y',strtotime($nextsaturday));
      $dateArr[] = date('M d, Y',strtotime($nextsaturday.'+ 1 day'));
    //   echo "<pre>";print_r($dateArr);die;
      return view('front.bookDemo',compact('categories','country_list','course_name','dateArr'));
   }

}
