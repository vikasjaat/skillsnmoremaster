<?php

namespace App\Http\Controllers\User;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use DB;
use Response;
use App\Classes\ScrumMailer;
use App\Models\User;
use App\Models\UserSubscription;
use App\Models\Courses;
use App\Models\StripePayment;

class UserController extends BaseController
{
   public function index(Request $request)
   {
      $input = $request->all();

      // echo "<pre>";
      // print_r($input);
      // die();

      $user = User::where('email', $input['email'])->first();

      if($user == null)
      {
         $user = new User;

         $user->email = $input['email'];
         $user->password = Hash::make($input['email']);
         $user->name = $input['parentName'];
         $user->save();
         
      }

      if($user != null)
      {  
         if($user->stripe_id == null)
            $stripeCustomer = $user->createAsStripeCustomer();
         $userSubscription = new UserSubscription();
         $userSubscription->user_id = $user->id;
         $userSubscription->subscription_id = Str::random(32);
         $userSubscription->status = 'pending';

         $userSubscription->fill($input);
         $userSubscription->save();
         
         $stripeDetail = StripePayment::where('course_id', $input['course_id'])->where('frequency', $input['frequency'])->firstorFail();
         
         $userSubscription->stripe_price = $stripeDetail->price_id;
         $userSubscription->save();

         

         return response()->json(['status' => 'success', 'subscription_id' => $userSubscription->subscription_id]);
      }
      
      return response()->json(['status' => 'error']);

      // $user = User::findorFail(1);
      // return $user()->redirectToBillingPortal();
      
   }

   public function createPayment($subscriptionId){
      $stripe_key = env('STRIPE_KEY');

      $userSubscription = UserSubscription::where('subscription_id', $subscriptionId)->where('status', 'pending')->firstorFail();

      $course = Courses::where('id',$userSubscription->course_id)->with('instructor')->firstorFail();
      $states = DB::table('states')->where('country_id',231)->get();
      return view('front.payments-page',compact('stripe_key','userSubscription','course','states'));
   }

   public function savePaymentMethod(Request $request){

      $input = $request->all();

      $userSubscription = UserSubscription::where('subscription_id', $input['susbcriptionId'])->where('status', 'pending')->first();

      if($userSubscription != null)
      {
         $user = User::findorFail($userSubscription->user_id);

         $subscription = $user->newSubscription('default', $userSubscription->stripe_price)->create($input['paymentMethod']);

         $userSubscription->status = 'active';
         $userSubscription->stripe_subscription = $subscription->stripe_id;
         $userSubscription->billing_name = $input['name'];
         $userSubscription->billing_address = $input['address'].', '.$input['state'].', '.$input['pincode'];

         $userSubscription->save();

         return response()->json(['status' => 'success', 'subscriptionId' => $userSubscription->subscription_id]);
      }
      else
      {
         return response()->json(['status' => 'error']);
      }     
   }

   public function thankYou($subscriptionId)
   {
      $sub = UserSubscription::where('subscription_id',$subscriptionId)->where('status', 'active')->firstorFail();
      return view('front.thankyou', compact('sub'));
   }
}
