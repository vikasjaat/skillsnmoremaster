<?php

namespace App\Http\Controllers\inst\auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\InstRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('instructor.instlogin');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(InstRequest $request)
    {   
        $request->authenticate();

        $request->session()->regenerate();

        return redirect()->intended(RouteServiceProvider::INST_HOME);
    }

   
    public function destroy(Request $request)
    {
        Auth::guard('inst')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
