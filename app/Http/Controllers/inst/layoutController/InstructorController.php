<?php

namespace App\Http\Controllers\inst\layoutController;

use App\Models\Categories;
use App\Models\Courses;
use App\Models\Instructors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use App\Models\Schedule;
use Stripe\Coupon;
use function PHPUnit\Framework\isNull;

class InstructorController extends Controller
{
    public function index(Request $request)
    {
        $search = $request['search'] ?? "";
        $courselist = courses::where('instructor', '=', Auth::guard('inst')->user()->id)->join('categories', function ($join) use ($search) {
            $join->on('courses.category', '=', 'categories.id');
        })->where(function ($query) use ($search) {
            $query->where('courses.name', 'LIKE', "%$search%")
                ->orWhere('categories.name', 'LIKE', "%$search%");
        })->select("courses.*", "categories.name as cat_name")->get();
        $cat = Categories::all();
        $data = compact('search', 'courselist', 'cat');
        return view('instructor.layouts.course')->with($data);
    }
    public function courseview($id)
    {
        $courselist = Courses::join('categories', function ($join) {
            $join->on('courses.category', '=', 'categories.id');
        })->select("courses.*", "categories.name as cat_name")->find($id);
        return view('instructor.layouts.course-view', ['courselist' => $courselist]);
    }
    public function addcourse()
    {
        $url = url('/instructor/Save-Course');
        $cat = Categories::all();
        $title = "Add Course Form";
        $data = compact('url', 'cat', 'title');
        return view('instructor.layouts.add-course')->with($data);
    }
    public function store(Request $request)
    {
        $request->validate([
            'categories' => 'required',
            'cname' => 'required',
            'slug' => 'required',
            'editor' => 'required',
            'learning' => 'required',
            'arangeto' => 'required',
            'arange' => 'required',
            'price' => 'required',
            'duration' => 'required',
            'frequency' => 'required',
            'image' => 'required',
        ]);
        $course = new Courses();
        $course->category = $request->categories;
        $course->instructor = Auth::guard('inst')->user()->id;
        $course->name = ucwords($request->cname) . ' ' . 'by' . ' ' . ucwords(Auth::guard('inst')->user()->name);
        $course->slug = ($request->slug) . '-' . str_replace(' ', '-', Auth::guard('inst')->user()->name);
        $course->summary = $request->editor;
        $course->learning = $request->learning;
        $course->age_range = 'Perfect for kids of age range' . ($request->arangeto) . '-' . ($request->arange);
        $course->price = $request->price;
        $course->duration = $request->duration;
        $course->frequency = $request->frequency;
        if ($request->status == "") {
            $course->status = 0;
        } else {
            $course->status = $request->status;
        }
        if ($request->image) {
            $file = $request->file("image");
            $fname = str_replace('-', ' ', $file->getClientOriginalName());
            $newfilename = pathinfo($fname, PATHINFO_FILENAME) . '_' . time() . '.' . $file->getClientoriginalExtension();
            $file->storeAs('public/courses', $newfilename);
            $course->image = $newfilename;
        }
        $course->save();
        return redirect('/instructor/course');
    }
    public function trash($id)
    {
        $delete = Courses::find($id);
        if (!is_null('$delete')) {
            storage::delete('public/' . $delete['image']);
            $delete->delete($id);
        }
        return redirect('/instructor/course');
    }
    public function edit($id)
    {
        $data = Courses::find($id);
        if (is_null('$id')) {
            return redirect('/instructor/course');
        } else {
            $cat = Categories::all();
            $title = 'Update Course Form';
            $url = url('instructor/update') . "/" . $id;
            $data = compact('data', 'url', 'cat', 'title');
            return view('instructor.layouts.add-course')->with($data);
        }
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'categories' => 'required',
            'cname' => 'required',
            'slug' => 'required',
            'editor' => 'required',
            'learning' => 'required',
            'arangeto' => 'required',
            'arange' => 'required',
            'price' => 'required',
            'duration' => 'required',
            'frequency' => 'required',
        ]);
        $course = Courses::find($id);
        $course->category = $request->categories;
        $course->instructor = Auth::guard('inst')->user()->id;
        $course->name = ($request->cname) . ' ' . 'by' . ' ' . Auth::guard('inst')->user()->name;
        $course->slug = ($request->slug) . '-' . str_replace(' ', '-', Auth::guard('inst')->user()->name);
        $course->summary = $request->editor;
        $course->learning = $request->learning;
        $course->age_range = ($request->arangeto) . ',' . ($request->arange);
        $course->price = $request->price;
        $course->duration = $request->duration;
        $course->frequency = $request->frequency;
        if ($request->status == "") {
            $course->status = 0;
        } else {
            $course->status = $request->status;
        }
        $course->save();
        return redirect('instructor/course');
    }
    public function trashview()
    {
        $courselist = Courses::onlyTrashed('instructor', Auth::guard('inst')->user()->id)->join('categories', function ($join) {
            $join->on('courses.category', '=', 'categories.id');
        })->select("courses.*", "categories.name as cat_name")->get();
        $cat = Categories::all();
        $data = compact('courselist', 'cat');
        return view('instructor.layouts.course-trash')->with($data);
    }
    public function restore($id)
    {
        $delete = Courses::withTrashed()->find($id);
        if (!is_null('$delete')) {
            $delete->restore($id);
        }
        return redirect('/instructor/trash-course');
    }
}