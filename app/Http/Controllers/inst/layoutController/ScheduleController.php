<?php

namespace App\Http\Controllers\inst\layoutcontroller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Courses;
use App\Models\Categories;
use Illuminate\Support\Facades\Auth;
use App\Models\Schedule;
use App\Models\Event;
use App\Models\TimetableModel;
use Carbon\Carbon;
use App\Models\StudentBatch;

use App\Models\UserSubscription;

class ScheduleController extends Controller
{  
    public function showschedule($id)
    {
        $course = Courses::find($id);
        $schedule = Schedule::where('course_id',$id)->get();
        // dd($schedule);
      
        $data = compact('course', 'schedule');
        return view('instructor.layouts.add-event')->with($data);
    }
    public function storeschedule(Request $request)
    {
        $request->validate([
            'course_id' => 'required',
            'start_at' => 'required',
            'status' => 'required',
            'batch_name' => 'required',
            'class_frequency' => 'required',
            'eventname' => 'required',
            'week' => 'required',
            'on_time' => 'required',
            'end_time' => 'required',
        ]);
        $batch=  $request->batch_name.','.rand(0,99999);    
        $day = Carbon::createFromFormat('Y-m-d', $request->start_at);
            foreach (range(1, 7 * $request->week) as $key => $next) {
                $day = $day->addDay();
                foreach ($request->schedule as $key => $val) {
                    if (strtolower($day->format('l')) == 'sunday' && $request->schedule[$key] == 'sunday') {
                        $time = new  Schedule();
                        $time->course_id = $request->course_id;
                        $time->on_date = $request->start_at;
                        $time->status = $request->status;
                        $time->batch_name =$batch;
                        $time->class_frequency = $request->class_frequency;
                        $time->event_name = $request->eventname;
                        $time->date = $day->toDateString();
                        $time->on_time = $request->on_time[$key];
                        $time->end_time = $request->end_time[$key];
                        $time->save();
                    }
                    if (strtolower($day->format('l')) == 'monday' && $request->schedule[$key] == 'monday') {
                        $time = new  Schedule();
                        $time->course_id = $request->course_id;
                        $time->status = $request->status;
                        $time->on_date = $request->start_at;
                        $time->batch_name =$batch;
                        $time->class_frequency = $request->class_frequency;
                        $time->event_name = $request->eventname;
                        $time->date = $day->toDateString();
                        $time->on_time = $request->on_time[$key];
                        $time->end_time = $request->end_time[$key];
                        $time->save();
                        }
                    if (strtolower($day->format('l')) == 'tuesday' && $request->schedule[$key] == 'tuesday') {
                        $time = new  Schedule();
                        $time->course_id = $request->course_id;
                        $time->status = $request->status;
                        $time->on_date = $request->start_at;
                        $time->batch_name =$batch;
                        $time->class_frequency = $request->class_frequency;
                        $time->event_name = $request->eventname;
                        $time->date = $day->toDateString();
                        $time->on_time = $request->on_time[$key];
                        $time->end_time = $request->end_time[$key];
                        $time->save();
                    }
                    if (strtolower($day->format('l')) == 'wednesday' && $request->schedule[$key] == 'wednesday') {
                        $time = new  Schedule();
                        $time->course_id = $request->course_id;
                        $time->status = $request->status;
                        $time->on_date = $request->start_at;
                        $time->batch_name =$batch;
                        $time->class_frequency = $request->class_frequency;
                        $time->event_name = $request->eventname;
                        $time->date = $day->toDateString();
                        $time->on_time = $request->on_time[$key];
                        $time->end_time = $request->end_time[$key];
                        $time->save();
                    }
                    if (strtolower($day->format('l')) == 'thrusday' && $request->schedule[$key] == 'thrusday') {
                        $time = new  Schedule();
                        $time->course_id = $request->course_id;
                        $time->status = $request->status;
                        $time->on_date = $request->start_at;
                        $time->batch_name = $batch;
                        $time->class_frequency = $request->class_frequency;
                        $time->event_name = $request->eventname;
                        $time->date = $day->toDateString();
                        $time->on_time = $request->on_time[$key];
                        $time->end_time = $request->end_time[$key];
                        $time->save();
                    }
                    if (strtolower($day->format('l')) == 'friday' && $request->schedule[$key] == 'friday') {
                        $time = new  Schedule();
                        $time->course_id = $request->course_id;
                        $time->status = $request->status;
                        $time->on_date = $request->start_at;
                        $time->batch_name = $batch;
                        $time->class_frequency = $request->class_frequency;
                        $time->event_name = $request->eventname;
                        $time->date = $day->toDateString();
                        $time->on_time = $request->on_time[$key];
                        $time->end_time = $request->end_time[$key];
                        $time->save();
                    }
                    if (strtolower($day->format('l')) == 'saturday' && $request->schedule[$key] == 'saturday') {
                        $time = new  Schedule();
                        $time->course_id = $request->course_id;
                        $time->status = $request->status;
                        $time->on_date = $request->start_at;
                        $time->batch_name = $batch;
                        $time->class_frequency = $request->class_frequency;
                        $time->event_name = $request->eventname;
                        $time->date = $day->toDateString();
                        $time->on_time = $request->on_time[$key];
                        $time->end_time = $request->end_time[$key];
                        $time->save();
                    }
                }
        }
        return redirect('instructor/course');
    }
    public function createschedule($id)
    {
        $data = Courses::find($id);
        $cat  = Categories::find($data->category);
        $data = compact('data', 'cat');
        return view('instructor.layouts.schedule.add-event')->with($data);
    }
    public function showclass($batch_name)
    {
        $showstd = StudentBatch::where('schedule_id',$batch_name)
        ->select('schedule_id','user_subsc_id','status')->get(); 
        $data = [];
        $user = [];
        foreach($showstd as $key=>$item){
            $user[$key] = UserSubscription::find($item->user_subsc_id)->toarray();
        }      
        $timetable = Schedule::where('batch_name', $batch_name)->get();
            foreach($timetable as $item){
            $users = Courses::where('id',$item->course_id)
            ->select('name','duration','image')
            ->get();
        }       
        return view('instructor.layouts.schedule.show-timetable', compact('timetable','users','showstd','user'));
    }
    // --------------------Add Student in Schedule-------------------
    public function addstudent($batch_name){
      
        $batch= Schedule::where('batch_name',$batch_name)
        ->select('id','batch_name','course_id')->get();
        foreach($batch as $item){
            $course = UserSubscription::where('course_id',$item->course_id)
            ->select('studentName','course_id','id','status','schedule_status')->get();
        } 
        $data= compact('batch','course');
        return view('instructor.layouts.schedule.add-student')->with($data);
    }
    public function storestudent($id,$batch_name){
        $course = Schedule::where('batch_name',$batch_name)
        ->select('id','course_id')->get();
        $user = UserSubscription::find($id);
        $user->schedule_status = 1;
        $user->save();
        $userschedule = new StudentBatch();
        $userschedule->schedule_id= $batch_name;
        $userschedule->course_id = $user->course_id;
        $userschedule->user_subsc_id = $user->id;
        $userschedule->status ='active';
        $userschedule->save();
        return redirect()->back();
    }
}
