<?php

namespace App\Http\Controllers\inst\layoutController;


use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Courses;
use App\Models\InstructorCategory;
use App\Models\User;
use App\Models\UserSubscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    public function index(){
        $courses = Courses::where("instructor",Auth::guard("inst")->user()->id)->join("user_subscription",'courses.id','=', 'user_subscription.course_id')->join('users', function ($join) {
            $join->on('users.id', '=', 'user_subscription.user_id');
        })->select('courses.*', 'user_subscription.user_id', 'users.*')->get();
        $data = compact('courses');
    
        return view('instructor.layouts.student-course')->with($data);
    }
    public function store(Request $request){
        
    }
}
