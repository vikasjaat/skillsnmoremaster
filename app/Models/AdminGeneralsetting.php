<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminGeneralsetting extends Model
{
    
    protected $table = 'admin_general_setting';
    
    protected $fillable = ['from_email','from_subject'];
    public $timestamps = false;
}
	