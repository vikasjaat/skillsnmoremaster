<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Courses extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'courses';
    
    protected $fillable = ['name', 'slug', 'summary', 'learning', 'instructor', 'age_range', 'price', 'duration', 'frequency', 'image'];
    public $timestamps = false;

    public function instructorDetail()
    {
        return $this->hasOne(Instructors::class, 'id', 'instructor');
    }
    public function coursecategory()
    {
        return $this->hasOne(Categories::class, 'id', 'category');
    }
    public function stripeproduct()
    {
        return $this->hasMany(StripePayment::class, 'course_id');
    }
   
}
