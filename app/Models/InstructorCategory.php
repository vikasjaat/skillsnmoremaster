<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstructorCategory extends Model
{
    
    protected $table = 'instructor_category';
    
    protected $fillable = ['instructor', 'category'];
    public $timestamps = false;

    public function category() {
        return $this->belongsTo('App\Models\Categories', 'category');
    }
}

