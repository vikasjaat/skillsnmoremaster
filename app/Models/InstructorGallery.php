<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstructorGallery extends Model
{
    
    protected $table = 'gallery';
    
    protected $fillable = ['instructor', 'image'];
    public $timestamps = false;

}

