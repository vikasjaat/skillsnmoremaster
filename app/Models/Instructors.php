<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Instructors extends Authenticatable
{
    
    protected $guard = 'inst';
    protected $table = 'instructors';
    public $timestamps = false;

    protected $fillable = ['email', 'email_verified_at', 'password', 'name', 'slug', 'photo', 'status', 'summary','remember_token'];
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function courses() {
        return $this->belongsTo('App\Models\Courses', 'courses');
    }

    public function categories(){
        return $this->hasMany('\App\Models\InstructorCategory','instructor');
    }
    public function getNameAttribute($value)
    {
        return ucwords($value);
    }
    public function gallery(){
        return $this->hasMany('\App\Models\InstructorGallery','instructor_id');
    }
}