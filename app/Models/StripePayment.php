<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StripePayment extends Model
{
    
    protected $table = 'stripe_product';
    
    protected $fillable = ['course_id', 'frequency', 'product_id', 'price_id'];

    public $timestamps = false;
}