<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentBatch extends Model
{
    use HasFactory;
    protected $table = 'student_batches';
    protected $fillable = ['schedule_id	', 'course_id', 'user_subsc_id', 'status'];
}
