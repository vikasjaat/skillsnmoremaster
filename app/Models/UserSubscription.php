<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    
    protected $table = 'user_subscription';
    
    protected $fillable = ['user_id', 'subscription_id', 'email', 'pricePerClass', 'status', 'frequency', 'course_id', 'parentName', 'studentName', 'age', 'school', 'phone', 'address', 'state', 'city','billing_name', 'billing_address','stripe_subscription'];

    public $timestamps = false;
}