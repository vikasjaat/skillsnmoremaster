<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Categories;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.header', function($view)
        {
            $menu_categories = Categories::orderBy('name')->get();
            $view->with('menu_categories',$menu_categories);
        });
        view()->composer('layouts.footer', function($view)
        {
            $menu_categories = Categories::orderBy('name')->get();
            $modal_state_list = DB::table('states')->where('country_id',231)->get();
            $view->with(['modal_categories'=>$menu_categories, 'modal_state_list'=>$modal_state_list]);
        });
        view()->composer('includes.categories', function($view)
        {
            $include_categories = Categories::orderBy('name')->get();
            
            $view->with('include_categories',$include_categories);
        });
    }
}
