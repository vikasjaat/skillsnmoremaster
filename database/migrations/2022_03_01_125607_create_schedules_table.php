<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->id();
            $table->string('course_id');
            $table->string('status');
            $table->enum('class_frequency', ['D', 'W', 'M']);
            $table->date('on_date');
            $table->string('batch_name');
            $table->string('event_name');
            $table->date('date');
            $table->time('on_time');
            $table->time('end_time');       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
