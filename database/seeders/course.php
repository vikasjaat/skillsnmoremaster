<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Courses;
use Illuminate\Support\Facades\Auth;
USE Faker\Factory as Faker;

class course extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker= Faker::create();
        for($i=0; $i <=5 ; $i++){
        $course = new courses;
        $course->category ='2';
        $course->instructor = '7';
        $course->name = $faker->name.' '.'by vikas jaat';
        $course->slug =$faker->slug. '-' . str_replace(' ', '-','vikas jaat');
        $course->summary = $faker->name;
        $course->learning = $faker->name;
        $course->age_range =$faker->numberBetween(25, 50);
        $course->price = $faker->randomDigit;
        $course->duration = $faker->randomDigit;
        $course->frequency = $faker->randomDigit;
        $course->status = $faker->numberBetween(0, 1);
        $course->save();
        }   
    }
}
