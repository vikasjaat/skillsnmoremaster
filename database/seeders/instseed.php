<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Instructors;
use App\Models\Student;
use Illuminate\Support\Facades\Hash;
class instseed extends Seeder
{   
    public function run()
    {

        Instructors::insert([
            "name"=>"vikas jaat",
            "email"=>"inst@gmail.com",
            "password"=> Hash::make("password"),
        ]);
    }
}
