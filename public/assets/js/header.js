$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $(".t-header").addClass("sticky-header");
    } else {
        $(".t-header").removeClass("sticky-header");
    }
});


$(function(){
    $('.navbar-toggler').on("click", function () {
      $('#custom-overlay').toggleClass("show-bg-overlay");
      $('body').toggleClass("scroll-stop");

    });

  });
