const mediaQuery = window.matchMedia('(max-width: 991px)')
    // Check if the media query is true
    if (mediaQuery.matches) {
        $(".course-slider-section").addClass("course-slider");
        $(".price-slider-section").addClass("price-slider");
        $(".blg-slider-section").addClass("blg-slider");
        $(".academics-slider-section").addClass("academics-slider");

    }

    $(".testimonial-slider").slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        arrows: false,
        autoplay: true,
        slidesToScroll: 1
    });

    $(".experts-slider").slick({
        infinite: true,
        autoplay: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: false,
        dots: true,
        responsive: [{
            breakpoint: 991,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }
        ]
    });








    $(".partners-slider").slick({
        dots: false,
        infinite: false,
        autoplay: true,
        slidesToShow: 5,
        arrows: false,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }
        ]
    });

    $(".course-slider").slick({
        dots: true,
        infinite: true,
        autoplay: true,
        slidesToShow: 2,
        arrows: false,
        slidesToScroll: 2,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    });

    $(".course-slider-inners").slick({
        dots: true,
        infinite: true,
        autoplay: true,
        slidesToShow: 3,
        arrows: false,
        slidesToScroll:3,
        responsive: [{
            breakpoint:992,
            settings: {
                slidesToShow: 2,
                slidesToScroll:2
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    });

    $(".price-slider").slick({
        dots: true,
        infinite: true,
        autoplay: true,
        slidesToShow: 2,
        arrows: false,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    });

    $(".blg-slider").slick({
        dots: true,
        infinite: true,
        autoplay: true,
        slidesToShow: 2,
        arrows: false,
        slidesToScroll: 2,
        responsive: [{
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    });

    $(".academics-slider").slick({
        dots: true,
        infinite: true,
        slidesToShow: 2,
        arrows: false,
        autoplay: true,
        slidesToScroll: 2,
        responsive: [{
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    });
