
$(document).ready(function($) {

   // Whole Script Strict Mode Syntax
   "use strict";
});

   // Banner Moving JS Start
   var lFollowX = 0,
      lFollowY = 0,
      x = 0,
      y = 0,
      friction = 1 / 30;

   function moveBackground() {
      x += (lFollowX - x) * friction;
      y += (lFollowY - y) * friction;

      //  translate = 'translateX(' + x + 'px, ' + y + 'px)';
      translate = 'translateX(' + x + 'px) translateY(' + y +'px)';

      $('.bannershape-animte').css({
      '-webit-transform': translate,
      '-moz-transform': translate,
      'transform': translate
      });

      window.requestAnimationFrame(moveBackground);
   }

   $(window).on('mousemove click', function(e) {
      
      var isHovered = $('.bannershape-animte:hover').length > 0;
      
      //if(!$(e.target).hasClass('animate-this')) {
      if(!isHovered) {
         var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX)),
               lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));

         lFollowX = (20 * lMouseX) / 100;
         lFollowY = (10 * lMouseY) / 100;
      }
   });

   moveBackground();
   // Banner Moving JS End