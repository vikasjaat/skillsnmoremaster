@extends('dashboard')
@section('content')
<!-- banner Start -->
<section class="inner-page-banner">
   <span class="banner-shape-1 bannershape-animte">
   <img src="{{asset('assets/images/shape-1.png')}}" alt="shape">
   </span>
   <span class="banner-shape-2 bannershape-animte">
   <img src="{{asset('assets/images/shape-2.png')}}" alt="shape">
   </span>
   <span class="banner-shape-3 bannershape-animte">
   <img src="{{asset('assets/images/shape-3.png')}}" alt="shape">
   </span>
   <span class="banner-shape-4 bannershape-animte">
   <img src="{{asset('assets/images/shape-4.png')}}" alt="shape">
   </span>
   <div class="container">
      <h1>About Us</h1>
   </div>
</section>
<!-- banner End -->
<!-- Breadcum Start -->
<div class="theme-breadcum-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <div class="theme-breadcrum">
               <ul>
                  <li><a href="{{route('home-page')}}">Home</a></li>
                  <li><i class="fal fa-chevron-right"></i></li>
                  <li>About Us</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Breadcum End -->
<!-- About Section Start -->
<section class="abt-section abt-pb bg-white">
   <div class="container">
      <div class="row align-items-center">
         <div class="col-lg-6">
            <div class="abt-section-img">
               <img src="{{asset('assets/images/about.png')}}" class="img-fluid" alt="About Image">
               <div class="aliment-1">
                  <div class="aliment-icon-red">
                     <img src="{{asset('assets/images/learning.svg')}}" alt="icon">
                  </div>
                  <div class="aliment-content">
                     <h3 class="h3-title">Focused Learning</h3>
                     <p>Customized Courses</p>
                  </div>
               </div>
               <div class="aliment-3">
                  <div class="aliment-icon-green">
                     <img src="{{asset('assets/images/virtual.svg')}}" alt="icon">
                  </div>
                  <div class="aliment-content">
                     <h3 class="h3-title">Virtual Classes</h3>
                     <p>Classes conducted virtually </p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-6">
            <div class="abt-right">
               <div class="sub-heading-section">
                  <h5 class="text-uppercase">ABOUT US</h5>
                  <h2>We Deliver the most effective learning experience</h2>
                  <p>SkillsnMore is the perfect place if you are looking for some extracurricular activities for kids. We have introduced a vast variety of courses from arts and crafts and music to coding. Our courses are designed to be interactive and interesting and help kids stay engaged.</p>
               </div>
               <ul class="abt-section-list">
                  <li> Personalised classes across different age groups, skill levels and interests.</li>
                  <li>Our teachers provide playful learning across various subjects like coding, painting, drawing, sketching as well as music.</li>
                  <li> Courses offered by SkillsnMore are easy to access, with Kid-friendly navigation for independent play.</li>
               </ul>
               <!-- <a href="javascript:void(0)" class="theme-btn">Explore More</a> -->
            </div>
         </div>
      </div>
   </div>
</section>
<!-- About Section End -->
<!-- Explore Our Courses Start -->
@include('includes.categories')
<!-- Explore Our Courses end -->
<!-- ====Founders section start=== -->
<section class="expert-team">
   <div class="container">
      <div class="sub-heading-section text-center">
         <h5 class="text-uppercase">Founders</h5>
         <h2>Meet our Founders</h2>
      </div>
      <div class="found-card">
      <div class="row  align-items-center">
         <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="f-img">
               <img src="{{asset('assets/images/founder/monika-founder.jpg')}}" alt="category" class="img-fluid main-founder">
               <div>
                  <img src="{{asset('assets/images/founder/yello-bg.png')}}" alt="category" class=" yellow-bg" >
                  <img src="{{asset('assets/images/founder/two-sp.png')}}" alt="category" class=" two-sp" >
               </div>
            </div>
         </div>
         <div class="col-lg-8 col-md-8 col-sm-12">
               <div class="card__corner">
                  <div class="card__corner-triangle"></div>
               </div>
               <div class="found-content">
                  <h3>Monika Agarwal</h3>
                  <p>A successful digital marketing expert and a teacher with experience of over five years Monika Agarwal is an avid believer in authenticity. After completing her M.tech in Electronics and Communication she went on to teach kids and discovered her passion for teaching. A proud mother she understands the value of online learning for kids as a new forte for maximizing learning. Through Skillsnmore she is set out to begin her entrepreneurial journey.</p>
               </div>
            </div>
      </div>
    </div>
    <!-- <div class="found-card">
      <div class="row  align-items-center">
         <div class="col-lg-8 col-md-8 col-sm-12 order-2 order-lg-1 order-md-1">
            <div class="card__corner-l">
               <div class="card__corner-triangle-l"></div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 order-1 order-lg-2 order-md-2">
               <div class="f-img">
                  <img src="{{asset('assets/images/founder/Swati-Kaushal-Founder.jpg')}}" alt="category" class="img-fluid main-founder">
                  <div>
                     <img src="{{asset('assets/images/founder/yello-bg.png')}}" alt="category" class=" yellow-bg">
                     <img src="{{asset('assets/images/founder/two-sp.png')}}" alt="category" class=" two-sp">
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div> -->

   </div>
</section>
<!-- ====Founders section start=== -->
<!--==== expert slider Start=== -->
@include('includes.expert')
<!--==== expert slider end=== -->
<!-- Testimonial Section Start -->
@include('includes.testimonial')
<!-- Testimonial Section End -->
<!-- Cta Section Start -->
@include('includes.cta')
<!-- Cta Section End -->
@endsection