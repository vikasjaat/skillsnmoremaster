@extends('dashboard')
@section('content')
<!-- banner Start -->
<section class="inner-page-banner">
   <span class="banner-shape-1 bannershape-animte">
      <img src="{{asset('assets/images/shape-1.png')}}" alt="shape">
   </span>
   <span class="banner-shape-2 bannershape-animte">
      <img src="{{asset('assets/images/shape-2.png')}}" alt="shape">
   </span>
   <span class="banner-shape-3 bannershape-animte">
      <img src="{{asset('assets/images/shape-3.png')}}" alt="shape">
   </span>
   <span class="banner-shape-4 bannershape-animte">
      <img src="{{asset('assets/images/shape-4.png')}}" alt="shape">
   </span>
   <div class="container">
      <h1>Blog Detail</h1>
   </div>
</section>
<!-- banner End -->
<!-- Breadcum Start -->
<div class="theme-breadcum-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <div class="theme-breadcrum">
               <ul>
                  <li><a href="{{route('home-page')}}">Home</a></li>
                  <li><i class="fal fa-chevron-right"></i></li>
                  <li><a href="#">Blog</a></li>
                  <li><i class="fal fa-chevron-right"></i></li>
                  <li>Blog Detail</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Breadcum End -->
<!-- Our Courses Section Start -->
<section class="blog-details-page courses-details-page-section">
   <div class="container">
      <div class="row">
         <div class="col-xl-8 col-lg-7">
            <div class="blog-left-section">
               <div class="blog-list-section">
                  <div class="blg-lst-img">
                     <img src="{{asset('assets/images/blog-img1.jpg')}}" >
                  </div>
               </div>
               <div class="name-date-comment">
                  <img src="{{asset('assets/images/client1.jpg')}}" class="me-2"><a href="javascript:void(0);" class="name">John Doe</a>
                  <i class="fal fa-calendar me-2"></i><a href="javascript:void(0);" class="name">07 Jan 2022</a>
                  <i class="far fa-comment me-2"></i><a href="javascript:void(0);">3 Comments</a>
               </div>
               <h3 class="inner-heading">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</h3>
               <p>Donec sit amet eros non massa vehicula porta. Nulla facilisi. Suspendisse ac aliquet nisi, lacinia mattis magna. Praesent quis consectetur neque, sed viverra neque. Mauris ultrices massa purus, fermentum ornare magna gravida vitae. Nulla sit amet est a enim porta gravida.</p>
               <p>Integer consectetur diam vitae imperdiet iaculis. In faucibus, sem sit amet tincidunt egestas, magna ligula interdum leo, quis lacinia mauris odio nec lectus.</p>
               <h3 class="inner-heading mt-2">Educater is the only theme you will ever need</h3>
               <ul class="abt-section-list">
                 <li>Suspendisse nunc massa, pellentesque eu nibh eget.</li>
                 <li>Suspendisse nunc massa, pellentesque eu nibh eget.</li>
                 <li>Suspendisse nunc massa, pellentesque eu nibh eget.</li>
              </ul>
              <div class="blog-inner-text-box">
               <h3 class="inner-heading">Integer eu lorem augue. Curabitur luctus, orci in dictum accumsan, nulla enim aliquam risus.</h3>
               <h3 class="inner-heading inner-heading2 mb-0">William Son</h3>
            </div>
            <p>Donec sit amet eros non massa vehicula porta. Nulla facilisi. Suspendisse ac aliquet nisi, lacinia mattis magna. Praesent quis consectetur neque, sed viverra neque. Mauris ultrices massa purus, fermentum ornare magna gravida vitae. Nulla sit amet est a enim porta gravida</p>
            <hr>
            <div class="row">
               <div class="col-lg-7">
                  <ul class="blg-category-link">
                     <li class="d-inline-block">Tags:</li>
                     <li class="d-inline-block"><a href="javascript:void(0);" class="d-inline-block">Corporate</a></li>
                     <li class="d-inline-block"><a href="javascript:void(0);" class="d-inline-block">Blog</a></li>
                     <li class="d-inline-block"><a href="javascript:void(0);" class="d-inline-block">Marketing</a></li>
                  </ul>
               </div>
               <div class="col-lg-5"></div>
            </div>
         </div>
      </div>
      <div class="col-xl-4 col-lg-5">
         <div class="sidbar-details-courses">
            <div class="sidbar-courses">
               <form class="news-letter-frm">
                  <input type="email" name="email" class="form-control" placeholder="Email Address" required="">
                  <button type="submit"><i class="fa fa-search"></i></button>
               </form>
            </div>
            <div class="sidbar-courses-categories">
               <h3 class="inner-heading side-bar-heading">Categories</h3>
               <ul class="blg-category-list">
                  <li><a href="javascript:void(0);">Business(10)</a></li>
                  <li><a href="javascript:void(0);">Case Study(13)</a></li>
                  <li><a href="javascript:void(0);">Insights</a></li>
                  <li><a href="javascript:void(0);">World(3)</a></li>
                  <li><a href="javascript:void(0);">Text Solutions(12)</a></li>
                  <li class="mb-0"><a href="javascript:void(0);">Creative(6)</a></li>
               </ul>
            </div>
            <div class="sidbar-courses-categories">
               <h3 class="inner-heading side-bar-heading">Categories</h3>
               <ul class="related-crs-list">
                  <li>
                     <div class="related-crs-list-img">
                        <img src="{{asset('assets/images/recent-course1.jpg')}}" alt="recent course">
                     </div>
                     <div class="related-crs-list-text">
                        <p class="mb-0"><a href="course-detail.html">Integer at fermentum sem, nec tincidunt massa.</a></p>
                        <span class="fs-6 fw-normal"><i class="fal fa-calendar me-2"></i>07 Jan, 2022</span>
                     </div>
                  </li>
                  <li>
                     <div class="related-crs-list-img">
                        <img src="{{asset('assets/images/recent-course2.jpg')}}" alt="recent course">
                     </div>
                     <div class="related-crs-list-text">
                        <p class="mb-0"><a href="course-detail.html">Integer at fermentum sem, nec tincidunt massa.</a></p>
                        <span class="fs-6 fw-normal"><i class="fal fa-calendar me-2"></i>07 Jan, 2022</span>
                     </div>
                  </li>
                  <li>
                     <div class="related-crs-list-img">
                        <img src="{{asset('assets/images/recent-course3.jpg')}}" alt="recent course">
                     </div>
                     <div class="related-crs-list-text">
                        <p class="mb-0"><a href="course-detail.html">Integer at fermentum sem, nec tincidunt massa.</a></p>
                        <span class="fs-6 fw-normal"><i class="fal fa-calendar me-2"></i>07 Jan, 2022</span>
                     </div>
                  </li>
                  <li>
                     <div class="related-crs-list-img">
                        <img src="{{asset('assets/images/recent-course4.jpg')}}" alt="recent course">
                     </div>
                     <div class="related-crs-list-text">
                        <p class="mb-0"><a href="course-detail.html">Integer at fermentum sem, nec tincidunt massa.</a></p>
                        <span class="fs-6 fw-normal"><i class="fal fa-calendar me-2"></i>07 Jan, 2022</span>
                     </div>
                  </li>
               </ul>
            </div>
            <div class="sidbar-courses-categories">
               <h3 class="inner-heading side-bar-heading">Categories</h3>
               <ul class="blg-category-link">
                  <li class="d-inline-block text-center"><a href="javascript:void(0);" class="d-inline-block">Business</a></li>
                  <li class="d-inline-block text-center"><a href="javascript:void(0);" class="d-inline-block">Corporate</a></li>
                  <li class="d-inline-block text-center"><a href="javascript:void(0);" class="d-inline-block">Blog</a></li>
                  <li class="d-inline-block text-center"><a href="javascript:void(0);" class="d-inline-block">Marketing</a></li>
                  <li class="d-inline-block text-center"><a href="javascript:void(0);" class="d-inline-block">Creative</a></li>
                  <li class="d-inline-block text-center"><a href="javascript:void(0);" class="d-inline-block">Web</a></li>
                  <li class="d-inline-block text-center"><a href="javascript:void(0);" class="d-inline-block">Workers</a></li>
                  <li class="d-inline-block text-center"><a href="javascript:void(0);" class="d-inline-block">Morden</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
</section>
<!-- Our Courses Section End -->
<!-- cta-section start-->
<section class="cta">
   <div class="container">
      <div class="row align-items-center">
         <div class="col-lg-6">
            <h2 class="text-white mb-0">Start Your Best Online Classes With Us</h2>
         </div>
         <div class="col-lg-6 text-end">
            <a href="javascript:void(0);" class="theme-btn">Contact Us</a>
         </div>
      </div>
   </div>
</section>
<!-- cta-section end-->
@endsection
