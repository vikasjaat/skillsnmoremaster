@extends('dashboard')
@section('content')

<!-- banner Start -->
<section class="inner-page-banner">
   <span class="banner-shape-1 bannershape-animte">
      <img src="{{asset('assets/images/shape-1.png')}}" alt="shape">
   </span>
   <span class="banner-shape-2 bannershape-animte">
      <img src="{{asset('assets/images/shape-2.png')}}" alt="shape">
   </span>
   <span class="banner-shape-3 bannershape-animte">
      <img src="{{asset('assets/images/shape-3.png')}}" alt="shape">
   </span>
   <span class="banner-shape-4 bannershape-animte">
      <img src="{{asset('assets/images/shape-4.png')}}" alt="shape">
   </span>
   <div class="container">
      <h1>Contact Us</h1>
   </div>
</section>
<!-- banner End -->
<!-- Breadcum Start -->
<div class="theme-breadcum-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <div class="theme-breadcrum">
               <ul>
                  <li><a href="{{route('home-page')}}">Home</a></li>
                  <li><i class="fal fa-chevron-right"></i></li>
                  <li>Contact Us</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Breadcum End -->
<!-- Contact Details Section Start -->
<section class="contact-section">
   <div class="container">
      <div class="row align-items-center">
         <div class="col-lg-5">
            <div class="cntct-details">
               <h3 class="text-white">How to Connect with Us</h3>
               <p class="text-white">Give us a call or drop by anytime, we endeavour to answer all enquiries within 24 hours on business days. We will be happy to answer your questions.</p>
               <ul>
                  <li class="d-flex align-items-center">
                     <div class="icon">
                        <img src="{{asset('assets/images/location.svg')}}" alt="location">
                     </div>
                     <div class="content">
                        <p class="text-white text-uppercase">Our Address :</p>
                        <h4 class="mb-0">9318, Shadowglen Court, Highlands Ranch,<br> CO 80126, United States</h4>
                     </div>
                  </li>
                  <li class="d-flex align-items-center">
                     <div class="icon">
                        <img src="{{asset('assets/images/mail.svg')}}" alt="mail">
                     </div>
                     <div class="content">
                        <p class="text-white text-uppercase">Our Mailbox :</p>
                        <h4 class="mb-0"><a href="mailto:contact@skillsnmore.com">contact@skillsnmore.com</a></h4>
                     </div>
                  </li>
                  <li class="d-flex align-items-center">
                     <div class="icon">
                        <img src="{{asset('assets/images/call.svg')}}" alt="Call">
                     </div>
                     <div class="content">
                        <p class="text-white text-uppercase">Our Phones :</p>
                        <h4 class="mb-0"><a href="tel:+1 -800-456-478-23">+1 (720) 213-4103</a></h4>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
         <div class="col-lg-7">
            <div class="get-touch-box">
               <div class="sub-heading-section">
                  <h5 class="text-uppercase">Drop Your Details </h5>
                  <h2>We'll Get Back to You in a While</h2>
               </div>
               <div class="get-touch-section">
                  <form action="" method="get">
                     <div class="input-box">
                        <input type="text" name="Full Name" class="form-control" placeholder="Full Name" required="">
                     </div>
                     <div class="input-box">
                        <input type="email" name="EmailAddress" class="form-control" placeholder="Email Address" required="">
                     </div>
                     <div class="input-box">
                        <input type="text" name="Phone No" class="form-control phone" placeholder="Phone No." required="">
                     </div>
                     <div class="input-box">
                        <textarea class="form-control" placeholder="Message..."></textarea>
                     </div>
                     <div class="">
                        <button type="submit" class="theme-btn"><span>Submit Now</span></button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Contact Details Section End -->

<!-- Map Section Start -->
<section class="conatct-page-map">
   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3076.595009437959!2d-104.97231108467105!3d39.54618927947581!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x876c83a4f7a621ff%3A0x1d9cc4a34a60aafc!2s9318%20Shadowglen%20Ct%2C%20Littleton%2C%20CO%2080126%2C%20USA!5e0!3m2!1sen!2sin!4v1642485967042!5m2!1sen!2sin" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</section>
<!-- Map Section End -->

@endsection
