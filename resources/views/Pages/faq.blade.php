@extends('dashboard')
@section('content')
<!-- banner Start -->
<section class="inner-page-banner">
   <span class="banner-shape-1 bannershape-animte">
      <img src="{{asset('assets/images/shape-1.png')}}" alt="shape">
   </span>
   <span class="banner-shape-2 bannershape-animte">
      <img src="{{asset('assets/images/shape-2.png')}}" alt="shape">
   </span>
   <span class="banner-shape-3 bannershape-animte">
      <img src="{{asset('assets/images/shape-3.png')}}" alt="shape">
   </span>
   <span class="banner-shape-4 bannershape-animte">
      <img src="{{asset('assets/images/shape-4.png')}}" alt="shape">
   </span>
   <div class="container">
      <h1>FAQ</h1>
   </div>
</section>
<!-- banner End -->
<!-- Breadcum Start -->
<div class="theme-breadcum-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <div class="theme-breadcrum">
               <ul>
                  <li><a href="">Home</a></li>
                  <li><i class="fal fa-chevron-right"></i></li>
                  <li><a href="">FAQ</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Breadcum End -->
<section class="faq-section">
   <div class="container">
      <div class="sub-heading-section">
         <h5 class="text-center text-uppercase">OUR FAQ</h5>
         <h2 class="text-center">Most Frequent Questions</h2>
      </div>
      <div class="row">
         <div class="col-lg-6">
            <div class="accordion" id="accordionExample">
               <div class="accordion-item">
                  <h2 class="accordion-header" id="headingOne">
                     <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                     Pellentesque commodo nec tellus sit amet tincidunt ?
                     </button>
                  </h2>
                  <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                     <div class="accordion-body">
                        <p>Sed nec rhoncus mi. Fusce sollicitudin tempus tellus sed facilisis. Praesent eu ante ac magna efficitur pretium sit amet ut quam. Praesent pharetra purus lorem, hendrerit faucibus purus elementum.</p>
                     </div>
                  </div>
               </div>
               <div class="accordion-item">
                  <h2 class="accordion-header" id="headingTwo">
                     <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Etiam nec lacus sit amet mi facilisis sodales ?</button>
                  </h2>
                  <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                     <div class="accordion-body">
                        <p>Sed nec rhoncus mi. Fusce sollicitudin tempus tellus sed facilisis. Praesent eu ante ac magna efficitur pretium sit amet ut quam. Praesent pharetra purus lorem, hendrerit faucibus purus elementum.</p>
                     </div>
                  </div>
               </div>
               <div class="accordion-item">
                  <h2 class="accordion-header" id="headingThree">
                     <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Nunc blandit ac diam sit amet commodo ?</button>
                  </h2>
                  <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                     <div class="accordion-body">
                        <p>Sed nec rhoncus mi. Fusce sollicitudin tempus tellus sed facilisis. Praesent eu ante ac magna efficitur pretium sit amet ut quam. Praesent pharetra purus lorem, hendrerit faucibus purus elementum.</p>
                     </div>
                  </div>
               </div>
               <div class="accordion-item">
                  <h2 class="accordion-header" id="headingFour">
                     <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Nullam auctor sapien magna, at luctus risus feugiat ?</button>
                  </h2>
                  <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                     <div class="accordion-body">
                        <p>Sed nec rhoncus mi. Fusce sollicitudin tempus tellus sed facilisis. Praesent eu ante ac magna efficitur pretium sit amet ut quam. Praesent pharetra purus lorem, hendrerit faucibus purus elementum.</p>
                     </div>
                  </div>
               </div>
               <div class="accordion-item">
                  <h2 class="accordion-header" id="headingFive">
                     <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">Aliquam cursus lectus lacinia rhoncus imperdiet ?</button>
                  </h2>
                  <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                     <div class="accordion-body">
                        <p>Sed nec rhoncus mi. Fusce sollicitudin tempus tellus sed facilisis. Praesent eu ante ac magna efficitur pretium sit amet ut quam. Praesent pharetra purus lorem, hendrerit faucibus purus elementum.</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-6">
            <div class="accordion" id="accordionExample1">
               <div class="accordion-item">
                  <h2 class="accordion-header" id="headingSix">
                     <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                     Pellentesque commodo nec tellus sit amet tincidunt ?</button>
                  </h2>
                  <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix" data-bs-parent="#accordionExample1">
                     <div class="accordion-body">
                        <p>Sed nec rhoncus mi. Fusce sollicitudin tempus tellus sed facilisis. Praesent eu ante ac magna efficitur pretium sit amet ut quam. Praesent pharetra purus lorem, hendrerit faucibus purus elementum.</p>
                     </div>
                  </div>
               </div>
               <div class="accordion-item">
                  <h2 class="accordion-header" id="headingSeven">
                     <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">Etiam nec lacus sit amet mi facilisis sodales ?</button>
                  </h2>
                  <div id="collapseSeven" class="accordion-collapse collapse" aria-labelledby="headingSeven" data-bs-parent="#accordionExample1">
                     <div class="accordion-body">
                        <p>Sed nec rhoncus mi. Fusce sollicitudin tempus tellus sed facilisis. Praesent eu ante ac magna efficitur pretium sit amet ut quam. Praesent pharetra purus lorem, hendrerit faucibus purus elementum.</p>
                     </div>
                  </div>
               </div>
               <div class="accordion-item">
                  <h2 class="accordion-header" id="headingEight">
                     <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">Nunc blandit ac diam sit amet commodo ?</button>
                  </h2>
                  <div id="collapseEight" class="accordion-collapse collapse" aria-labelledby="headingEight" data-bs-parent="#accordionExample1">
                     <div class="accordion-body">
                        <p>Sed nec rhoncus mi. Fusce sollicitudin tempus tellus sed facilisis. Praesent eu ante ac magna efficitur pretium sit amet ut quam. Praesent pharetra purus lorem, hendrerit faucibus purus elementum.</p>
                     </div>
                  </div>
               </div>
               <div class="accordion-item">
                  <h2 class="accordion-header" id="headingNine">
                     <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">Nullam auctor sapien magna, at luctus risus feugiat ?</button>
                  </h2>
                  <div id="collapseNine" class="accordion-collapse collapse" aria-labelledby="headingNine" data-bs-parent="#accordionExample1">
                     <div class="accordion-body">
                        <p>Sed nec rhoncus mi. Fusce sollicitudin tempus tellus sed facilisis. Praesent eu ante ac magna efficitur pretium sit amet ut quam. Praesent pharetra purus lorem, hendrerit faucibus purus elementum.</p>
                     </div>
                  </div>
               </div>
               <div class="accordion-item">
                  <h2 class="accordion-header" id="headingTen">
                     <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">Aliquam cursus lectus lacinia rhoncus imperdiet ?</button>
                  </h2>
                  <div id="collapseTen" class="accordion-collapse collapse" aria-labelledby="headingTen" data-bs-parent="#accordionExample1">
                     <div class="accordion-body">
                        <p>Sed nec rhoncus mi. Fusce sollicitudin tempus tellus sed facilisis. Praesent eu ante ac magna efficitur pretium sit amet ut quam. Praesent pharetra purus lorem, hendrerit faucibus purus elementum.</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- testimonial-section-start -->
<section class="testmonila-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-6">
            <div class="sub-heading-section">
               <h5 class="text-uppercase">Testimonial</h5>
               <h2 class="">What Our Client Says About Us</h2>
               <p>Proin et lacus eu odio tempor porttitor id vel augue. Vivamus volutpat vehicula sem, et imperdiet enim tempor id. Phasellus lobortis efficitur nisl eget vehicula. Donec viverra blandit nunc, nec tempor ligula ullamcorper venenatis.</p>
            </div>
         </div>
         <div class="col-lg-6">
            <div class="testimonial-slider">
               <div>
                  <div class="testimonials-details">
                     <p>"Proin feugiat tortor non neque eleifend, at fermentum est elementum. Ut mollis leo odio vulputate rutrum. Nunc sagittis sit amet ligula ut eleifend. Mauris consequat mauris sit amet turpis commodo fermentum. Quisque consequat tortor ut nisl finibus".</p>
                     <div class="testimonials-content-img">
                        <div class="testimonial-img">
                           <img src="{{asset('assets/images/client1.jpg')}}" alt="client">
                        </div>
                        <div class="testimonial-content">
                           <h4>Christine Rose</h4>
                           <p>Customer</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div>
                  <div class="testimonials-details">
                     <p>"Proin feugiat tortor non neque eleifend, at fermentum est elementum. Ut mollis leo odio vulputate rutrum. Nunc sagittis sit amet ligula ut eleifend. Mauris consequat mauris sit amet turpis commodo fermentum. Quisque consequat tortor ut nisl finibus".</p>
                     <div class="testimonials-content-img">
                        <div class="testimonial-img">
                           <img src="{{asset('assets/images/client1.jpg')}}" alt="client">
                        </div>
                        <div class="testimonial-content">
                           <h4>Christine Rose</h4>
                           <p>Customer</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div>
                  <div class="testimonials-details">
                     <p>"Proin feugiat tortor non neque eleifend, at fermentum est elementum. Ut mollis leo odio vulputate rutrum. Nunc sagittis sit amet ligula ut eleifend. Mauris consequat mauris sit amet turpis commodo fermentum. Quisque consequat tortor ut nisl finibus".</p>
                     <div class="testimonials-content-img">
                        <div class="testimonial-img">
                           <img src="{{asset('assets/images/client1.jpg')}}" alt="client">
                        </div>
                        <div class="testimonial-content">
                           <h4>Christine Rose</h4>
                           <p>Customer</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- testimonial-section-end -->
<!-- Contact Details Section Start -->
<section class="contact-section">
   <div class="container">
      <div class="row align-items-center">
         <div class="col-lg-6">
            <div class="contact-left-img">
               <img src="{{asset('assets/images/contact-sec-img.png')}}" alt="Contact ">
            </div>
         </div>
         <div class="col-lg-6">
            <div class="get-touch-box">
               <div class="sub-heading-section">
                  <h5 class="text-uppercase">SUBMIT TICKET</h5>
                  <h2>Not Found Answer Contact Us</h2>
               </div>
               <div class="get-touch-section">
                  <form action="" method="get">
                     <div class="input-box">
                        <input type="text" name="Full Name" class="form-control" placeholder="Full Name" required="">
                     </div>
                     <div class="input-box">
                        <input type="email" name="EmailAddress" class="form-control" placeholder="Email Address" required="">
                     </div>
                     <div class="input-box">
                        <input type="text" name="Phone No" class="form-control" placeholder="Phone No." required="">
                     </div>
                     <div class="input-box">
                        <textarea class="form-control" placeholder="Message..."></textarea>
                     </div>
                     <div class="">
                        <button type="submit" class="theme-btn"><span>Submit Now</span></button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Contact Details Section End -->
<!-- cta-section start-->
<section class="cta">
   <div class="container">
      <div class="row align-items-center">
         <div class="col-lg-6">
            <h2 class="text-white mb-0">Start Your Best Online Classes With Us</h2>
         </div>
         <div class="col-lg-6 text-end">
            <a href="javascript:void(0);" class="theme-btn">Contact Us</a>
         </div>
      </div>
   </div>
</section>
<!-- cta-section end-->
@endsection