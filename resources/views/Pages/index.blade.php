@extends('dashboard')
@section('content')
<!-- banner Start -->
<section class="hm-pge-banner">
     <span class="banner-shape-1 bannershape-animte">
      <img src="{{asset('assets/images/shape-1.png')}}" alt="shape">
   </span>
   <span class="banner-shape-2 bannershape-animte">
      <img src="{{asset('assets/images/shape-2.png')}}" alt="shape">
   </span>
   <span class="banner-shape-3 bannershape-animte">
      <img src="{{asset('assets/images/shape-3.png')}}" alt="shape">
   </span>
   <span class="banner-shape-4 bannershape-animte">
      <img src="{{asset('assets/images/shape-4.png')}}" alt="shape">
   </span>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="banner-content">
                    <h2 class="text-uppercase">Experience the best of immersive online learning</h2>
                    <h1>Learn new skills from the <span>safety <img src="assets/images/text-line.png" alt="line"></span> of your home</h1>
                    <p>Pursue new skills and explore your potential and interests! SkillsnMore offers development courses in recreational activities through engaging online lessons amidst the pandemic.</p>
                    <div>
                        <a href="#couse-exp" class="theme-btn">Explore Courses</a>
                        <button class="theme-btn theme-btn2" type="button" onclick="document.location.href='{{route('front-course-schedule-demo')}}'">Book Your Free Demo</button>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="hm-pge-banner-right">
                    <div class="aliment-1">
                        <div class="aliment-icon-red">
                            <img src="{{asset('assets/images/banner-aliment-icon-1.svg')}}" alt="icon">
                        </div>
                        <div class="aliment-content">
                            <h3 class="h3-title"> Wide Curriculum </h3>
                            <p>Wide topics to choose and learn </p>
                        </div>
                    </div>
                    <div class="aliment-2">
                        <div class="aliment-icon-purple">
                            <img src="{{asset('assets/images/banner-aliment-icon-2.svg')}}" alt="icon">
                        </div>
                        <div class="aliment-content">
                            <h3 class="h3-title">Small Batches</h3>
                            <p>Maximum 4 Students per batch</p>
                        </div>
                    </div>
                    <div class="aliment-3">
                        <div class="aliment-icon-green">
                            <img src="{{asset('assets/images/banner-aliment-icon-3.svg')}}" alt="icon">
                        </div>
                        <div class="aliment-content">
                            <h3 class="h3-title">Progress Tracking</h3>
                            <p>Easy Evaluation</p>
                        </div>
                    </div>
                    <div class="aliment-4">
                        <img src="{{asset('assets/images/banner-aliment-icon-4.svg')}}" alt="icon">
                    </div>
                    <div class="banner-img">
                        <img src="{{asset('assets/images/banner-img.png')}}" alt="banner">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="category-section-new" id="couse-exp">
    <div class="container">
        <div class="sub-heading-section">
            <h5 class="text-center text-uppercase">Course Categories</h5>
            <h2 class="text-center ">Explore Our Courses</h2>
        </div>
        <ul class="row justify-content-center">
            <li class="col-xl-3 col-lg-4 col-md-6 col-6">
                <div class="category-inner">
                    <div class="category-inner-img">
                        <a href="{{route('front-course-category-all')}}"><img src="{{asset('assets/images/course-icon.svg')}}" alt="category"></a>
                    </div>
                    <div class="content">
                        <h3 class="mb-0"><a href="
                          ">All Courses</a></h3>
                        <p class="mb-0"><span class="age-text">Age 5+ years</span></p>
                        <a href="{{route('front-course-category-all')}}" class="theme-btn">Explore<i class="fal fa-chevron-right"></i></a>
                    </div>
                </div>
            </li>
            @foreach($categories as $category)
            <li class="col-xl-3 col-lg-4 col-md-6 col-6">
                <div class="category-inner">
                    <div class="category-inner-img">
                        <a href="{{route('front-course-category',$category->slug)}}"><img src="{{asset('storage/category/'.$category->icon)}}" alt="category"></a>
                    </div>
                    {{-- {{asset('storage/'.$item->icon) }} --}}
                    <div class="content">
                        <h3 class="mb-0"><a href="{{route('front-course-category',$category->slug)}}">{{$category->name}}</a></h3>
                        <p class="mb-0"><span class="age-text">Age {{$category->age_range ?? "5 to 16"}} years</span></p>
                        <a href="{{route('front-course-category',$category->slug)}}" class="theme-btn">Explore<i class="fal fa-chevron-right"></i></a>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
    </div>
    <div class="bg-shapess">
        <img src="{{asset('assets/images/bg-icons/sky-arrow.svg')}}" alt="category" class="sky-arrow">
        <img src="{{asset('assets/images/bg-icons/math.svg')}}" class="math">
        <img src="{{asset('assets/images/bg-icons/star-1.svg')}}" class="star1">
        <img src="{{asset('assets/images/bg-icons/star2.svg')}}" class="star2">
        <img src="{{asset('assets/images/bg-icons/trign-pink.svg')}}" class="tringle-pink">
        <img src="{{asset('assets/images/bg-icons/abcd.svg')}}" class="abcd-shape">
    </div>
</section>
<!-- About Section Start -->
<section class="abt-section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="abt-section-img">
                    <img src="{{asset('assets/images/about.png')}}" alt="About" class="img-fluid">
                    <div class="aliment-1">
                        <div class="aliment-icon-red">
                            <img src="{{asset('assets/images/learning.svg')}}" alt="icon">
                        </div>
                        <div class="aliment-content">
                            <h3 class="h3-title">Focused Learning</h3>
                            <p>Customized Courses</p>
                        </div>
                    </div>
                    <div class="aliment-3">
                        <div class="aliment-icon-green">
                            <img src="{{asset('assets/images/virtual.svg')}}" alt="icon">
                        </div>
                        <div class="aliment-content">
                            <h3 class="h3-title">Virtual Classes</h3>
                            <p>Classes conducted virtually </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="abt-right">
                    <div class="sub-heading-section">
                        <h5 class="text-uppercase">ABOUT US</h5>
                        <h2>Step into Digital Learning with SkillsnMore</h2>
                        <p>SkillsnMore is an online learning platform for kids of 5-16 yrs age groups. We offer online coding classes, online programming courses, music classes for children , and sketching classes . These recreational activities give your kids a chance to be creative and expressive.</p>
                        <p>Enjoy learning online art & craft classes for kids through interactive lessons. Step up your kid's analytical skills with the best online coding classes! Unveil wonderful kid's music classes and Drawing & sketching classes for kids! </p>
                    </div>
                    <ul class="abt-section-list">
                        <li>Lessons delivered by Experienced Domain Experts</li>
                        <li>Engaging Courses to guide you step-by-step</li>
                        <li>Certifications after successful completion of the course</li>
                    </ul>
                    <!-- <a href="javascript:void(0)" class="theme-btn">Explore More</a> -->
                </div>
            </div>
        </div>
    </div>
    <div class="abt-bg-shapess">
        <img src="{{asset('assets/images/bg-icons/code.svg')}}" alt="category" class="code-shape">
        <img src="{{asset('assets/images/bg-icons/music-icon.svg')}}" class="music-icon">
        <img src="{{asset('assets/images/bg-icons/paint.svg')}}" class="paint-shap">
        <img src="{{asset('assets/images/bg-icons/raket.svg')}}" class="raket-shap">


    </div>
</section>
<!-- About Section End -->
<!-- Partners Logo Section Start -->
<section class="partnres-logo-section">
    <!-- Cta Section Start -->
    @include('includes.cta')
    <!-- Cta Section End -->
</section>

<!-- Partners Logo Section End -->
<!-- Online Courses Section Start -->
<section class="online-course-sec">
    <div class="container">
        <div class="row align-items-center mb-4">
            <div class="col-lg-8">
                <div class="sub-heading-section">
                    <h5 class="text-uppercase">OUR ONLINE COURSES</h5>
                    <h2 class="pb-0 mb-0">Find The Right Online Course For You</h2>
                </div>
            </div>
            <div class="col-lg-4 d-none d-lg-block">
                <div class="online-courses-title-btn text-end">
                    <a href="{{route('front-course-category-all')}}" class="theme-btn">View All Courses</a>
                </div>
            </div>
        </div>

        <div class="row course-slider-section">
            @foreach($courses as $course)
            <div class="col-lg-4 col-md-6 col-12 Starting">
                <div class="course-item">
                    <div class="course-img">
                        <a href="{{route('front-course-detail',$course->slug)}}" class="img">
                            <img src="{{asset('storage/courses/'.$course->image)}}" alt="{{$course->name}}">
                        </a>
                        <ul>
                            <li><a href="javascript:void(0);" class="course-tag-blue">{{$course->coursecategory->name}}</a></li>
                            <!-- <li><a href="javascript:void(0);" class="course-tag-blue">Marketing</a></li> -->
                        </ul>
                    </div>
                    <div class="course-content">
                        <h3 class="h3-title text-truncate"><a href="{{route('front-course-detail',$course->slug)}}">{{$course->name}}</a></h3>
                        <div class="course-des">
                            <p>{!! substr(strip_tags($course->summary),0,500) !!}</p>
                        </div>
                        <div class="course-line"></div>
                        <div class="course-footer">
                            <div class="course-price">
                                Starting from <span>${!! number_format($course->price,2) !!}</span>/class
                            </div>
                            <!-- <div class="course-review">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <p>5.0 (2k)</p>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
        <div class="text-center d-block d-lg-none pt-5">
            <a href="{{route('front-course-category-all')}}" class="theme-btn">View All Course</a>
        </div>
    </div>

</section>
<!-- Online Courses Section End -->


<!-- Expert Section Start -->
@include('includes.expert')
<!-- Expert Section End -->

<!-- Testimonial Section Start -->
@include('includes.testimonial')
<!-- Testimonial Section End -->

<!-- Blog Section Start -->
<div class="d-none">
    @include('includes.blog')
</div>
<!-- Blog Section End -->
<section class="cta-bg">
    <!-- Cta Section Start -->
    @include('includes.cta')
    <!-- Cta Section End -->
</section>

@endsection