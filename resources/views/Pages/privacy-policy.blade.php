@extends('dashboard')
@section('content')

    <style type="text/css">

    </style>
    <!-- banner Start -->
    <section class="inner-page-banner">
        <span class="banner-shape-1 bannershape-animte">
            <img src="{{asset('assets/images/shape-1.png')}}" alt="shape">
        </span>
        <span class="banner-shape-2 bannershape-animte">
            <img src="{{asset('assets/images/shape-2.png')}}" alt="shape">
        </span>
        <span class="banner-shape-3 bannershape-animte">
            <img src="{{asset('assets/images/shape-3.png')}}" alt="shape">
        </span>
        <span class="banner-shape-4 bannershape-animte">
            <img src="{{asset('assets/images/shape-4.png')}}" alt="shape">
        </span>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Privacy Policy</h1>
                </div>
            </div>
        </div>
    </section>
    <!-- banner End -->
    
    <!-- Privacy Policy Page Content Start -->
    <section class="terms-content">
        <div class="container">
            <p>SkillsnMore is a reliable e-learning platform that prioritizes the privacy of our visitors. This Privacy Policy document contains disclosure about the collection of information by SkillsnMore and its usage.</p>
            <p>In case of any additional query or information about our Privacy Policy, do not hesitate to contact us.</p>
            <p>This Privacy Policy is applicable for our online activities only. All the terms are valid for visitors of our website and the information shared by the website visitor and/or collected in SkillsnMore. Any information collected through other channels (offline or social media) do not fall under this policy.</p>
            <h3 class="trms-cont-heading">Consent</h3>
            <p>By using our services or visiting the website, you hereby consent to our Privacy Policy and agree to comply with its terms.</p>
            <h3 class="trms-cont-heading">Information we collect</h3>
            <p>When the website collects your personal information with your consent, you are notified about the reasons why you are asked to provide them at the time of personal information collection.</p>
            <p>On personal contact with the SkillsnMore team, we may further get access to additional information about you such as your name, email address, phone number, the contents of the information shared by you in form of messages and/or attachments.</p>
            <p>At the time of registration on SkillsnMore, you will be asked to provide your contact information, which includes your name, company name, address, email address, and telephone number.</p>
            <h3 class="trms-cont-heading" >How we use your information</h3>
            <p>The information we collect through our website can be used in the following ways.</p>
            <ul class="abt-section-list mb-4">
                <li>To operate, and maintain our website</li>
                <li>For improving, personalizing, and expanding our website</li>
                <li>To understand how you use our website and analyze it to drive data</li>
                <li>In the development of  new products, services, features, and functionality</li>
                <li> For contacting you, directly or through one of our staff. It may be used for contacting customer service,  for providing you with updates and other information relating to the website, and/or for marketing and promotional purposes.</li>
                <li>For  sending you emails and newsletters</li>
                <li>For preventing fraudulent activities and identifying fraud.</li>
                <li>As Log Files</li>
            </ul>
            <p>SkillsnMore ensures to follow a standard procedure of using log files. These log files are created when visitors visit our website. It is a common feature provided by hosting companies and these log files are used for analytics by hosting services. The log files contain the following information: internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date and time stamp, referring/exit pages, and possibly the number of clicks. However, this data collection does not comprise any personally identifiable information. The sole purpose of this data collection is to analyze user behaviour and trends. It is also used for administering activities on the site and tracking users' navigation on the website. Which may also be used to gather information about demography.</p>
            <h3 class="trms-cont-heading">Cookies and Web Beacons</h3>
            <p>Like any other website, SkillsnMore uses 'cookies'. These cookies collect and store information such as visitors' preferences, and the pages on the website that the visitor visited. The data collected is used to improve the users' experience through optimization of our web page content as per the visitors' browser type and/or based on other derived information.</p>
            <h3 class="trms-cont-heading">Advertising Partners Privacy Policies</h3>
            <p>The Privacy Policy enlists the terms for every advertising partner of SkillsnMore.</p>
            <p>Third-party ad servers or ad networks associated with SkillsnMore may use cookies, JavaScript, Web Beacons, or other technologies in their respective advertisements and links that may appear on users' browsers. These ads or links automatically retrieve your IP address to measure, monitor and analyze the effectiveness of their advertising campaigns and/or to optimize and customize the advertising content on websites that you have visited online.</p>
            <p>SkillsnMore declares that we have no access to or control over these cookies that are used by third-party advertisers.</p>
            <h3 class="trms-cont-heading">Third-Party Privacy Policies</h3>
            <p>SkillsnMore's Privacy Policy is not applicable to other advertisers or websites. Therefore, we highly recommend that you consult the respective Privacy Policies of these third-party ad servers for adequate information. You may find ways to opt-out from such data usage through their respective instructions and more details about their practices.</p>
            <p>You have the choice of disabling cookies by clicking on the ‘Disable cookies’ option on your individual browser. </p>
            <h3 class="trms-cont-heading">CCPA Privacy Rights ( Selling or NOT Selling My Personal Information)</h3>
            <p>According to the CCPA privacy rights, California consumers have the  following rights:</p>
            <p>You have the right to request disclosure about a consumer's personal data collected by a business.  It must disclose the categories and specific pieces of personal data collected by a business.</p>
            <p>You can request for deletion of any personal data about the consumer that had been collected by a business.</p>
            <p>You have the right to request that the consumer’s personal data collected by a business are not to be sold even if the business is known to be selling a consumer's personal data.</p>
            <p>In case you want to exercise any of the above-mentioned rights, you may make a request. Please contact us if any such requests come up in your mind.</p>
            <h3 class="trms-cont-heading">GDPR Data Protection Rights</h3>
            <p>In the purview of cybersecurity threats, SkillsnMore would appreciate it if the visitors are fully aware of all of your data protection rights. Here are a few pointers that cover GDPR Data Protection Rights for every user:</p>
            <ul class="abt-section-list mb-4">
                <li>The right to access – Every user has the right to request copies of their personal data.  However, a nominal charge may be levied for availing of this service.</li>
                <li>The right to rectification – Users have the right to request correction of any inaccurate personal information.  You may also request such rectification if you believe that the collected data is incomplete.</li>
                <li>The right to erasure – Every user has the right to request the deletion of their personal data that we may have collected. However, this right is applicable under certain conditions.</li>
                <li>The right to restrict processing – Every user has the right to request a restriction on the processing of your personal data, under certain conditions.</li>
                <li>The right to object to processing – Every user has the right to express objection to our processing of your personal data, under certain conditions.</li>
                <li>The right to data portability – Every user has the right to request the transfer of the collected data to another organization, or directly to the user, under certain conditions.</li>
            </ul>
            <p>If you make a request, SkillsnMore has a month for a response. In any instance, if you feel the need to exercise any of these rights, we would request you to contact us.</p>
            <h3 class="trms-cont-heading">Children's Information</h3>
            <p>Ensuring protection for children while using the internet is one of the major elements in our privacy policy document. During the online interactions, we encourage parents and guardians to monitor and guide their kids' online activity.</p>
            <p>SkillsnMore does not knowingly collect any Personal Identifiable Information from children under the age of 13. In case your child has provided any personal information on our website that you would not like to be collected, we strongly encourage you to contact us immediately. SkillsnMore will surely look into the case and will do our best efforts to promptly remove such information from our records.</p>
        </div>
    </section>
    <!-- Privacy Policy Page Content End -->

@endsection
@section('script')

@endsection