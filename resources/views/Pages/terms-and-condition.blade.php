@extends('dashboard')
@section('content')

<style type="text/css">

</style>
<!-- banner Start -->
<section class="inner-page-banner">
    <span class="banner-shape-1 bannershape-animte">
        <img src="{{asset('assets/images/shape-1.png')}}" alt="shape">
    </span>
    <span class="banner-shape-2 bannershape-animte">
        <img src="{{asset('assets/images/shape-2.png')}}" alt="shape">
    </span>
    <span class="banner-shape-3 bannershape-animte">
        <img src="{{asset('assets/images/shape-3.png')}}" alt="shape">
    </span>
    <span class="banner-shape-4 bannershape-animte">
        <img src="{{asset('assets/images/shape-4.png')}}" alt="shape">
    </span>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Terms And Conditions</h1>
            </div>
        </div>
    </div>
</section>
<!-- banner End -->
<section class="terms-content">
    <div class="container">
        <p>Welcome to SkillsnMore. The Website is subjected to certain Terms and Conditions regarding usage which you need to comply with in order to use our website.</p>
        <p>Browsing Our website henceforth would mean, you agree to our terms and conditions and you are bound to follow them.</p>
        <p>If you disagree with any of the terms and conditions set forth, please bar from using this website.</p>
        <p>The following Terms and Conditions are to be followed when using the SkillsnMore website</p>
        <h3 class="trms-cont-heading-sub">Regarding Purchases and Subscription</h3>
        <p>Association with our services will require some information that you need to provide. You need to provide your e-mail address and other contact details via which we can connect to you. Please ensure that you use this email address regularly and professionally.</p>
        <p>All the interactions will be continued using this information from time to time to ensure sound communication between both parties. Sound communication is essential to complete the projects and financial transactions between the parties to ensure long-term association.</p>
        <h3 class="trms-cont-heading">Regarding Content</h3>
        <p>All the content expressed on the website has variable access and ownership. The content, if used without prior permission, may lead to legal actions. The content can be subjected to change whenever required without any notice.</p>
        <h3 class="trms-cont-heading" >Regarding External Links and links to other websites</h3>
        <p>Our website can undertake Third-party dealings and thus may have several backlinks to them. It is to be noted that we have no control over such backlinks.</p>
        <p>It should not be assumed that any links to a third party are endorsed by our Website. We don’t hold any accountability for the content of the websites that are linked.</p>
        <h3 class="trms-cont-heading">Regarding Changes</h3>
        <p>Only We, the website holders, are responsible for the content and changes of the content expressed on the website. No other individual /third party is liable to make any changes in the content of the website. Any legal and inevitable change cannot be put into effect without our consent. </p>
        <h3 class="trms-cont-heading">Contact Us</h3>
        <p>For any Confusion, query or assistance, contact us by sending an email to the following email ID:</p>
    </div>
</section>

@endsection
@section('script')

@endsection