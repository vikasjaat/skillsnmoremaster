@extends('admin.layouts.master')
@section('title', 'Skill And More Admin Panel')
 @push('css')
     <style>
         .course{
            width: 140px;
            height:140px;
         }
     </style>
 @endpush
@section('content')
    <div class="layout-px-spacing">
        <div class="seperator-header layout-top-spacing">
            <h4 class="">Courses</h4>
        </div>
        <div class="row layout-spacing">
            <div class="col-lg-12">
                <div class="statbox widget box box-shadow">
                    @if (Session::has('message'))
                        <div class="alert alert-light-primary border-0 mb-4" role="alert"> <button type="button"
                                class="close" data-dismiss="alert" aria-label="Close"> <svg
                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg></button>{{ Session::get('message') }} </div>
                    @endif
                    <div class="col-lg-10">
                        <h4>Schedule Time Table</h4>
                        <b>Schedule</b> /
                        <b>Time Table Details</b>
                    </div><br>
                    <div class="row">
                        <div class="col-12 col-sm-2">
                            <div class="form-group">
                                <span><img class="course" src="{{ url('storage/courses/' . $courseschedule[0]->image) }}"
                                        class=" profile-img" alt="avatar"></span>
                            </div>
                        </div>
                        <div class="col-12 col-sm-2">
                            <div class="form-group">Course:</div>
                            <div class="form-group">Batch Name:</div>
                            <div class="form-group">Duration:</div>
                            <div class="form-group">Frequency:</div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">{{course($courseschedule[0]->name)}}</div>
                            <div class="form-group">{{batchname($courseschedule[0]->batch_name)}}</div>
                            <div class="form-group">{{$courseschedule[0]->duration}} (Minute)</div>
                            <div class="form-group"> 
                                @if ($courseschedule[0]->class_frequency == 'D')
                                Every Day
                                @elseif ($courseschedule[0]->class_frequency == 'W')
                                Weekly
                                @elseif ($courseschedule[0]->class_frequency == 'M')
                                Monthly 
                            @endif
                      </div> 
                       </div>
                    </div>
                    <div class="widget-content widget-content-area border-tab">
                       <ul class="nav nav-tabs mt-3" id="border-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="border-home-tab" data-toggle="tab" href="#border-home" role="tab" aria-controls="border-home" aria-selected="true"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg> Student</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="border-profile-tab" data-toggle="tab" href="#border-profile" role="tab" aria-controls="border-profile" aria-selected="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg> Schedule</a>
                            </li>
                       </ul>
                        <div class="tab-content mb-4" id="border-tabsContent">
                            <div class="tab-pane fade show active" id="border-home" role="tabpanel" aria-labelledby="border-home-tab">
                                <div class="widget-content widget-content-area">
                                    <table id="style-3" class="table style-3  table-hover">
                                        <thead>
                                            <tr>
                                                <th class="checkbox-column dt-no-sorting"> Record Id </th>
                                                <th>Id</th>
                                                <th>Student Name</th>
                                                <th>Course Name</th>
                                                <th>Batch Name</th>
                                                <th>Start Date</th>
                                            </tr>
                                        </thead>
                                       <tbody>
                                            @foreach ($showstd as $key => $item)
                                                <tr>
                                                    <td class="checkbox-column"> 1 </td>
                                                    <td>{{ $key + 1 }}</td>
                                                    <td>{{$item->stud_name['studentName']}}</td>
                                                    <td>{{course($courseschedule[0]->name)}}</td>
                                                    <td>{{batchname($item->schedule_id)}}</td>
                                                    <td>{{\Carbon\Carbon::parse($timetable[0]->on_date)->format('d-M-Y')}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                             </div>
                           </div>
                            <div class="tab-pane fade" id="border-profile" role="tabpanel" aria-labelledby="border-profile-tab">
                                <table id="style-2" class="table style-2  table-hover">
                                    <thead>
                                        <tr>
                                            <th class="checkbox-column dt-no-sorting"> Record Id </th>
                                            <th>Id</th>
                                            <th>Date</th>
                                            <th>Day</th>
                                            <th>Start Time</th>
                                            <th>End Time</th>
                                        </tr>
                                    </thead>
                                   <tbody>
                                        @foreach ($timetable as $key => $item)
                                            <tr>
                                                <td class="checkbox-column"> 1 </td>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{Carbon\Carbon::parse($item->date)->format('d F Y')}}</td>
                                                <td>{{ Carbon\Carbon::parse($item->date)->format('l')}}</td>
                                                <td>{{\Carbon\Carbon::parse($item->on_time)->format('h:i: A.')}}</td>
                                                <td>{{\Carbon\Carbon::parse( $item->end_time)->format('h:i: A.')}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('script')
    <script src="{{asset('adminassets/js/custom-search.js')}}"></script>
    @endpush
@endsection
