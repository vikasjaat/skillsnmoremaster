@extends('admin.layouts.master')
@section('title', 'Skill And More Admin Panel')
@section('content')
    <div class="layout-px-spacing">
        <div class="seperator-header layout-top-spacing">
            <h4 class="">Batch</h4>
        </div>
        <div class="row layout-spacing">
            <div class="col-lg-12">
                <div class="statbox widget box box-shadow">
                    @if (Session::has('message'))
                        <div class="alert alert-light-primary border-0 mb-4" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                            {{ Session::get('message') }}
                        </div>
                    @endif

                    <div class="col-lg-10">
                        <h4>Courses Schedule</h4>
                        <b>Courses Schedule</b> /
                        <b>Schedule Details</b>
                    </div><br>
                    <div class="row">
                        <div class="col-12 col-sm-2">
                            <div class="form-group">
                                <span><img style="width: 100px;" src="{{ url('storage/courses/' . $coursedata->image) }}"
                                        class=" profile-img" alt="avatar"></span>
                            </div>
                        </div>
                        <div class="col-12 col-sm-1">
                            <div class="form-group">Course:</div>
                            <div class="form-group">Duration:</div>
                            <div class="form-group">Frequency:</div>
                        </div>
                        <div class="col-12 col-sm-4">
                           <div class="form-group">{{course($coursedata->name)}}</div>
                           <div class="form-group">{{$coursedata->duration}}</div>
                           <div class="form-group">{{$coursedata->frequency}}</div>
                       </div>
                        <div class="widget-content widget-content-area">
                            <table id="style-2" class="table style-2  table-hover">
                                <thead>
                                    <tr>
                                        <th class="checkbox-column dt-no-sorting"> Record Id </th>
                                        <th>Batch Name</th>
                                        <th>Start Date</th>
                                        <th>Total Student</th>
                                        <th>Status</th>
                                        <th>Duration</th>
                                        <th>Schedule</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  
                                        
                                
                                    @foreach ($schedule as $key => $item)
                                        @if (isset($schedule[$key - 1]) && $schedule[$key]->batch_name != $schedule[$key - 1]->batch_name)
                                            <tr>
                                                <td class="checkbox-column"> 1 </td>
                                                <td>{{ batchname($item->batch_name) }}</td>
                                                <td>{{ \Carbon\Carbon::parse($item->on_date)->format('d-M-Y')}}</td>
                                                <td>50</td>
                                                <td class="text-center">
                                                    <label class="switch s-icons s-outline  s-outline-primary">
                                                        <input type="checkbox" class="toggle-class-schedule" name="status"
                                                            data-id="{{$item->id}}" {{ $item->status == '1' ? 'checked' : '' }}>
                                                        <span class="slider round"></span>
                                                        </span>
                                                </td>
                                                <td>
                                                    @if ($item->class_frequency == 'W')
                                                        Week
                                                    @elseif($item->class_frequency == 'D')
                                                        Every Day
                                                    @elseif($item->class_frequency == 'M')
                                                        Month
                                                    @endif
                                                </td>
                                                
                                                <td>
                                                    <a id="btn-compose-mail" class="btn-theme btn-w"
                                                    href="{{ route('batch-student-show', [$item->batch_name]) }}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-plus">
                                                        <line x1="12" y1="5" x2="12" y2="19"></line>
                                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                                    </svg>
                                                </a>
                                                    <a id="btn-compose-mail" class="bg-info btn-w"
                                                        href="{{ route('batch-schedule-show', [$item->batch_name]) }}"><svg
                                                            xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                            class="feather feather-eye">
                                                            <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                            <circle cx="12" cy="12" r="3"></circle>
                                                        </svg>
                                                    </a>
                                                </td>
                                                <td>
                                                    <meta name="csrf-token" content="{{ csrf_token() }}">
                                                    <a class="deletecourses" href="javascript:void(0)" data-id=""><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i></a> |
                                                    <a href=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @elseif($key == 0)
                                            <tr>
                                                <td class="checkbox-column"> 1 </td>
                                                <td>{{ batchname($item->batch_name) }}</td>
                                                <td>{{ \Carbon\Carbon::parse($item->on_date)->format('d-M-Y')}}</td>
                                                <td>50</td>
                                                <td class="text-center">
                                                    <label class="switch s-icons s-outline  s-outline-primary">
                                                        <input type="checkbox" class="toggle-class-schedule" name="status"
                                                            data-id="{{$item->id}}" {{ $item->status == '1' ? 'checked' : '' }}>
                                                        <span class="slider round"></span>
                                                        </span>
                                                </td>
                                                <td>
                                                    @if ($item->class_frequency == 'W')
                                                        Week
                                                    @elseif($item->class_frequency == 'D')
                                                        Every Day
                                                    @elseif($item->class_frequency == 'M')
                                                        Month
                                                    @endif
                                                </td>
                                                <td>
                                                <a id="btn-compose-mail" class="btn-theme btn-w"
                                                    href="{{ route('batch-student-show', [$item->batch_name]) }}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-plus">
                                                        <line x1="12" y1="5" x2="12" y2="19"></line>
                                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                                    </svg>
                                                </a>
                                                    <a id="btn-compose-mail" class="bg-info btn-w"
                                                        href="{{ route('batch-schedule-show', [$item->batch_name]) }}"><svg
                                                            xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                            class="feather feather-eye">
                                                            <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                            <circle cx="12" cy="12" r="3"></circle>
                                                        </svg>
                                                    </a>
                                                </td>
                                                <td>
                                                    <meta name="csrf-token" content="{{ csrf_token() }}">
                                                    <a class="deletecourses" href="javascript:void(0)" data-id=""><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i></a> |
                                                    <a href=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @push('script')
        <script>
            $(function() {
                $(document).on("change",'.toggle-class-schedule',function() {
                    var status = $(this).prop('checked') == true ? 1 : 0;
                    var user_id = $(this).data('id');
                    $.ajax({
                        url: "{{ route('admin-schedule-status') }}",
                        type: "POST",
                        data: {
                            "_token":"{{csrf_token()}}",
                            status: status,
                            user_id: user_id
                        },
                        success: function(data) {
                            console.log(data.success)
                        }
                    });
                })
            })
            </script>
            <script src="{{asset('adminassets/js/custom-search.js')}}"></script>
        @endpush
    @endsection
