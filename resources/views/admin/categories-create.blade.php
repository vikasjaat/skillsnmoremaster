@extends('admin.layouts.master')
@section('title', 'Skill And More Admin Panel')
@section('content')
    <div class="layout-px-spacing">
        <div class="seperator-header layout-top-spacing">
        </div>
        <div id="flFormsGrid" class="col-lg-12 layout-spacing">
            <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>Forms Categories</h4>
                </div>
            </div>
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                </div>
                <div class="widget-content widget-content-area">
                    <form  action="{{ route('admin-categories-store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{$category !=null && $category->id ? $category->id :'' }}">
                        <div class="form-row mb-4">
                            <div class="form-group col-md-4">
                                <label for="inputName" class="form-label"> Name</label>
                                <input type="name" name="name" value="{{$category !=null && $category->name ? $category->name :''}}" placeholder="Name" class="form-control "
                                    id="categries_Name">
                                @error('name')
                                    <div style="color: red">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputName" class="form-label"> Slug</label>
                                <input type="text" value="{{$category !=null && $category->slug ? $category->slug :''}}" name="slug" class="form-control "
                                    id="categories_Slug">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputName" class="form-label">Age Range</label>
                                
                                <div class="input-group">
                                        <input type="number" class="form-control"
                                        value="@isset($category->start){{$category->start}}@endisset"
                                        name="first_range" placeholder="Age Range...">
                                    <span class="input-group-text">To</span>
                                    <input type="number" class="form-control" name="last_range"
                                        value="@isset($category->end){{$category->end}}@endisset"
                                        placeholder="Age Range...">
                                </div>
                                <span class="text-danger">
                                    @error('first_range')
                                        {{ $message }}
                                    @enderror
                                </span>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="status">Status</label>
                                <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                                    <label class="switch s-icons s-outline  s-outline-primary  mb-4 mr-2">
                                        @if ($category !=null)
                                        <input type="checkbox" value="1" {{$category !=null && $category->status == '1' ?  'checked' :''}} name="status" id="inputstatus">
                                            @else
                                            <input type="checkbox" value="1" name="status" id="inputstatus" checked>
                                        @endif
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="status">Image</label>
                            <div class="invoice-logo">
                                <div class="upload">
                                    @if ($category != null && $category->icon ? $category->icon :'' )
                                    <input type="file" name="icon" data-allowed-file-extensions="png jpg jpeg svg" id="input-file-max-fs" class="dropify" data-default-file="{{ url('storage/category/' . $category->icon) }}" data-max-file-size="2M" />
                                    @else
                                    <input type="file" name="icon" data-allowed-file-extensions="png jpg jpeg svg" id="input-file-max-fs" class="dropify" data-default-file="" data-max-file-size="2M" />
                                @endif
                                </div>
                                @error('icon')
                                <div style="color: red">{{ $message }}</div>
                            @enderror
                            </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="description">Description</label>
                                <textarea id="editor" name="description">
                                    {{$category !=null && $category->description ? $category->description :''}}
                                        </textarea>
                                @error('description')
                                    <div style="color: red">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-theme" id="btnSubmit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('script')
      <script>
            $("#categries_Name").keyup(function() {
                var Text = $(this).val();
                Text = Text.toLowerCase();
                Text = Text.replace(/[^a-zA-Z0-9]+/g, '-');
                $("#categories_Slug").val(Text);
            });
      </script>
        
    @endpush
@endsection
