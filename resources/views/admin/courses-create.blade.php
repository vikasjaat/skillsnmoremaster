@extends('admin.layouts.master')
@section('title', 'Skill And More Admin Panel')
<style>
    .error {
        color: red;
    }

</style>
@section('content')
    <div class="layout-px-spacing">
        <div class="seperator-header layout-top-spacing">
        </div>
        <div id="flFormsGrid" class="col-lg-12 layout-spacing">
            <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>Forms Courses</h4>
                </div>
            </div>
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                </div>
                <div class="widget-content widget-content-area">
                    <form action="{{ route('admin-courses-store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{ $courses != null && $courses->id ? $courses->id : '' }}">
                        <div class="row">
                            <div class="col-12">
                                <h5 class="form-title"><span>Courses Information</span></h5>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Categories</label>
                                    <select class="form-control select" name="category" id="catgories-dd">
                                        <option value="">Select Categories</option>
                                        @foreach ($categories as $item)
                                            <option value="{{ $item->id }}"
                                                {{ $courses != null && $item->id == $courses->category ? 'selected' : '' }}>
                                                {{ $item->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('category')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Instructors</label>
                                    <select class="form-control select" name="instructor" id="instructor-dd">

                                    </select>
                                    @error('instructor')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text"
                                        value="{{ course($courses != null && $courses->name ? $courses->name : '') }}"
                                        name="name" id="courses_Name" placeholder="Name" class="form-control">
                                    {{-- <input type="hidden" name="iname" id="editinst" value="{{ $courses->instructor }}"> --}}
                                    @error('name')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Slug</label>
                                    <input type="text"
                                        value="{{ $courses != null && $courses->slug ? $courses->slug : '' }}" name="slug"
                                        id="courses_Slug" class="form-control">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <label for="inputSummary" class="form-label">Age Range</label>
                                <div class="input-group">
                                    @if ($courses != null)
                                        @php
                                            $courses->age_range = explode('-', $courses->age_range);
                                        @endphp
                                    @endif
                                    <input type="number" class="form-control"
                                        value="{{ $courses != null && $courses->age_range ? $courses->age_range[0] : '' }}"
                                        name="first_range" placeholder="Age Range...">
                                    <span class="input-group-text">To</span>
                                    <input type="number" class="form-control" name="last_range"
                                        value="{{ $courses != null && $courses->age_range ? $courses->age_range[1] : '' }}"
                                        placeholder="Age Range...">
                                    <span class="text-danger">
                                        @error('first_range')
                                            {{ $message }}
                                        @enderror
                                    </span>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Price</label>
                                    <input type="number" name="price"
                                        value="{{ $courses != null && $courses->price ? $courses->price : '' }}"
                                        placeholder="Price" class="form-control">
                                    @error('price')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label>Duration</label>
                                    <input type="number"
                                        value="{{ $courses != null && $courses->duration ? $courses->duration : '' }}"
                                        name="duration" class="form-control" placeholder="Duration">
                                    @error('duration')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label>Frequency</label>
                                    <div>
                                        <input type="number" name="frequency"
                                            value="{{ $courses != null && $courses->frequency ? $courses->frequency : '' }}"
                                            class="form-control" placeholder="Frequency">
                                        @error('frequency')
                                            <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label>Learning</label>
                                    <textarea name="learning" class="form-control"
                                        rows="6">{{ $courses != null && $courses->learning ? $courses->learning : '' }}</textarea>
                                    @error('learning')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="status">Image</label>
                                <div class="invoice-logo">
                                    <div class="upload">
                                        @if ($courses != null && $courses->image ? $courses->image : '')
                                            <input type="file" name="image" id="input-file-max-fs" class="dropify"
                                                data-default-file="{{ asset('storage/courses/' . $courses->image) }}"
                                                data-max-file-size="2M" />
                                        @else
                                            <input type="file" name="image" id="input-file-max-fs" class="dropify"
                                                data-default-file="" data-max-file-size="2M" />
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label>Status</label>
                                <div class="form-group">
                                    <label class="switch s-icons s-outline  s-outline-primary">
                                        <input type="checkbox" value="1" name="status"
                                            @if ($courses != null) {{ $courses->status == 1 ? 'checked' : '' }}
                                            @else 
                                        checked 
                                        @endif>
                                        <span class="slider round"></span>
                                        </span>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12">
                                <div class="form-group">
                                    <label>Summary</label>
                                    <textarea type="text" name="summary" id="editor" class="form-control">
                                     {{ $courses != null && $courses->summary ? $courses->summary : '' }}
                                    </textarea>
                                    @error('summary')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-theme">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @isset($courses)
        <input type="hidden" id="editinst" value="{{ $courses->instructor }}">
    @endisset
    @push('script')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
            $("#courses_Name").keyup(function() {
                var Text = $(this).val();
                Text = Text.toLowerCase();
                Text = Text.replace(/[^a-zA-Z0-9]+/g, '-');
                $("#courses_Slug").val(Text);
            });
            $(document).ready(function() {
                function loadinst(cat_id) {
                    var idCountry = cat_id;
                    $("#instructor-dd").html('');
                    $.ajax({
                        url: "{{ url('admin/category-instructor/') }}" + '/' + idCountry,
                        type: "GET",
                        dataType: 'json',
                        success: function(result) {
                            $('#instructor-dd').html('<option value="">Select Instructor</option>');
                            $.each(result.instructors, function(key, value) {
                                let select = '';
                                if ($('#editinst').val() == value.id) {
                                    select = 'selected';
                                } else {
                                    select = '';
                                }
                                $("#instructor-dd").append('<option ' + select + ' value="' + value
                                    .id + '">' +
                                    value.name + '</option>');
                            });
                        }
                    });
                }
                $('#catgories-dd').on('change', function() {
                    loadinst(this.value);
                });
                loadinst($('#catgories-dd').val());
            });
        </script>
    @endpush
@endsection
