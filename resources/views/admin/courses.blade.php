@extends('admin.layouts.master')
@section('title', 'Skill And More Admin Panel')
@push('css')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" />
    <!-- <style>
        #btn-compose-mail {
            transform: none;
            background: rgb(111, 64, 211);
            border: none !important;
            padding: 7px 9px;
            font-size: 14px;
            font-weight: 700;
            letter-spacing: 1px;
            color: #fff !important;
            width: 40px;
            margin: 0 auto;
            box-shadow: 0px 5px 10px 0px rgb(59 63 92 / 38%);
        }

    </style> -->
@endpush
@section('content')
    <div class="layout-px-spacing">
        <div class="seperator-header layout-top-spacing">
            <h4 class="">Courses</h4>
        </div>
        <div class="row layout-spacing">
            <div class="col-lg-12">
                <div class="statbox widget box box-shadow">
                    @if (Session::has('message'))
                        <div class="alert alert-light-primary border-0 mb-4" role="alert"> <button type="button"
                                class="close" data-dismiss="alert" aria-label="Close"> <svg
                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg></button>{{ Session::get('message') }} </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-10">
                            <b>Courses</b>
                        </div>
                        <div class="col-lg-2 text-right">
                            <a href="{{ route('admin-courses-create', 'new') }}" type="button" class="btn btn-theme">Add
                                New</a>
                        </div>
                    </div>
                    @if (isset($courseslist))
                        <div class="widget-content widget-content-area">
                            <table id="style-2" class="table style-2  table-hover">
                                <thead>
                                    <tr>
                                        <th class="checkbox-column dt-no-sorting"> Record Id </th>
                                        <th>Id</th>
                                        <th>Course Name</th>
                                        <th>Category Name</th>
                                        <th>Image</th>
                                        <th>Schedule</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($courseslist as $key => $item)
                                        <tr>
                                            <td class="checkbox-column"> 1 </td>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ course($item->name) }}</td>
                                            <td>{{ $item->cat_name }}</td>
                                            <td class="text-center">
                                                <span><img src="{{ url('storage/courses/' . $item->image) }}"
                                                        class="rounded-circle profile-img" alt="avatar"></span>
                                            </td>
                                            <td>
                                                {{-- {{ route('schedul-bach-list', [$item->id]) }} --}}
                                                <a id="btn-compose-mail" class="btn-theme btn-w"
                                                    href="{{ route('schedule-add', [$item->id]) }}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-plus">
                                                        <line x1="12" y1="5" x2="12" y2="19"></line>
                                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                                    </svg>
                                                </a>
                                                <a id="btn-compose-mail" class="bg-info btn-w"
                                                    href="{{ route('batch-show' ,[$item->id]) }}"><svg
                                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-eye">
                                                        <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                        <circle cx="12" cy="12" r="3"></circle>
                                                    </svg>
                                                </a>
                                            </td>

                                            <td class="text-center">
                                                <label class="switch s-icons s-outline  s-outline-primary">
                                                    <input type="checkbox" class="toggle-class-courses" name="status"
                                                        data-id="{{ $item->id }}"
                                                        {{ $item->status == '1' ? 'checked' : '' }}>
                                                    <span class="slider round"></span>
                                                    </span>
                                            </td>
                                            <td class="text-center actions-tb-box">
                                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                                <a class="deletecourses"
                                                    data-id="{{ $item->id }}"><i class="fa fa-trash-o"
                                                        aria-hidden="true"></i></a> |
                                                <a href="{{ route('admin-courses-create', $item->id) }}"><i
                                                        class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @push('script')
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script>
            $(".deletecourses").click(function() {
                var ele = $(this).parent().parent();
                var id = $(this).data("id");
                var token = $("meta[name='csrf-token']").attr("content");
                var url = "{{ route('delete-courses', ':id') }}";
                url = url.replace(':id', id);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: url,
                            type: 'DELETE',
                            data: {
                                "id": id,
                                "_token": token,
                            },
                            success: function() {
                                console.log("it Works");
                            }
                        });
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                        $(ele).hide(2000);
                    }
                })
            });
            $(function() {
                $(document).on("change",'.toggle-class-courses',function() {
                    var status = $(this).prop('checked') == true ? 1 : 0;
                    var user_id = $(this).data('id');
                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: "{{ route('courses-status') }}",
                        data: {
                            'status': status,
                            'user_id': user_id
                        },
                        success: function(data) {
                            console.log(data.success)
                        }
                    });
                })
            })
            $(document).ready(function() {
                @isset($schedule)
                    new bootstrap.Modal(document.getElementById('exampleModal')).show();
                @endisset
                $("#myInput").on("click", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable tr").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>
      <script src="{{asset('adminassets/js/custom-search.js')}}"></script>
    @endpush
@endsection
