@extends('admin.layouts.master')
@section('title', 'Skill And More Admin Panel')
@section('content')
    <div class="layout-px-spacing">
        <div class="seperator-header layout-top-spacing">
        </div>
        <div id="flFormsGrid" class="col-lg-12 layout-spacing">
            <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>Forms Instructor</h4>
                </div>
            </div>
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                </div>
                <div class="widget-content widget-content-area">
                    <form action="{{ route('admin-instructor-store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{ $instrut != null && $instrut->id ? $instrut->id : '' }}">
                        <div class="form-row mb-4">
                            <div class="form-group col-md-6">
                                <label for="inputName" class="form-label"> Name</label>
                                <input type="name" name="name"
                                    value="{{ $instrut != null && $instrut->name ? $instrut->name : '' }}"
                                    placeholder="Name" class="form-control" id="inputName">
                                @error('name')
                                    <div style="color: red">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Email</label>
                                <input type="email" class="form-control"
                                    value="{{ $instrut != null && $instrut->email ? $instrut->email : '' }}" name="email"
                                    id="inputEmail" placeholder="Email">
                                @error('email')
                                    <div style="color: red">{{ $message }}</div>
                                @enderror
                            </div>
                          @if ($instrut ==null)
                          <div class="form-group col-md-6">
                            <label for="inputPassword4">Password</label>
                            <input type="password" name="password" value="{{ $instrut != null && $instrut->password ? $instrut->password : '' }}" class="form-control" id="inputPassword"
                                placeholder="Password">
                            @error('password')
                                <div style="color: red">{{ $message }}</div>
                            @enderror
                        </div>  
                          @endif
                            <div class="form-group col-md-3">
                                <label for="status">Status</label>
                                <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                                    <label class="switch s-icons s-outline  s-outline-primary  mb-4 mr-2">
                                        @if ($instrut != null)
                                         <input type="checkbox" value="1" name="status" id="inputstatus"
                                                {{ $instrut != null && $instrut->status == '1' ? 'checked' : '' }}>
                                        @else
                                            <input type="checkbox" value="1" name="status" id="inputstatus" checked>
                                        @endif
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="verified">Verified</label>
                                <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                                    <label class="switch s-icons s-outline  s-outline-primary  mb-4 mr-2">
                                        <input name="verified" value="1" {{ $instrut != null && $instrut->verified == '1' ? 'checked' : '' }} id="inputverified" type="checkbox">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="status">Categories</label>
                                <select name="category[]" class="form-control form-small" multiple="multiple">
                                    @foreach ($Categories as $item)
                                        <option value="{{ $item->id }}"{{$instrut != null && in_array($item->id,$instrut->cat)? 'selected' :''}}>{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                @error('category')
                                    <div style="color: red">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-md-3">
                                <label for="status">Profile</label>
                            <div class="invoice-logo">
                                <div class="upload">
                                    @if ($instrut != null && $instrut->photo ? $instrut->photo :'' )
                                    <input type="file" name="photo" id="input-file-max-fs" class="dropify" data-default-file="{{ url('storage/instructor/' . $instrut->photo) }}" data-max-file-size="2M" />
                                    @else
                                    <input type="file" name="photo" id="input-file-max-fs" class="dropify" data-default-file="" data-max-file-size="2M" />
                                    
                                @endif
                                </div>
                            </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="inputAddress2">Description</label>
                                <textarea type="text" id="editor" class="form-control" name="summary"
                                    placeholder="Enter Description">
                                            {{ $instrut != null && $instrut->summary ? $instrut->summary : '' }}
                                           </textarea>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-theme" id="btnSubmit">Submit</button>
                        </div>
                    </form>
                  
                </div>
            </div>
        </div>
    </div>
    @push('script')
        {{-- <script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script data-require="jquery@2.2.4" data-semver="2.2.4"
                src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> --}}
    @endpush
@endsection
