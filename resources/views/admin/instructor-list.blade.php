@extends('admin.layouts.master')
@section('title', 'Skill And More Admin Panel')
@section('content')
    <div class="layout-px-spacing">
        <div class="seperator-header layout-top-spacing">
            <h4 class="">Instructors</h4>
        </div>
        <div class="row layout-spacing">
            <div class="col-lg-12">
                <div class="statbox widget box box-shadow">
                    @if (Session::has('message'))
                    <div class="alert alert-light-primary border-0 mb-4" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>{{ Session::get('message') }} </div>
                @endif
                    <div class="row">
                        <div class="col-lg-10">
                            <b>Instructors</b>
                        </div>
                        <div class="col-lg-2 text-right">
                            <a href="{{ route('admin-instructor-create', 'new') }}" type="button" class="btn btn-theme">Add New</a>
                        </div>
                    </div>
                    <div class="widget-content widget-content-area">
                        <table id="style-2" class="table style-2  table-hover">
                            <thead>
                                <tr>
                                    <th class="checkbox-column dt-no-sorting"> Record Id </th>
                                    <th>Id</th>
                                    <th>First Name</th>
                                    <th>Email</th>
                                    <th class="text-center">Image</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center dt-no-sorting">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($instructor as $key => $item)
                                    <tr>
                                        <td class="checkbox-column"> 1 </td>
                                        <td>{{ $key + 1 }} </td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td class="text-center">
                                            <span><img src="{{ url('storage/instructor/' . $item->photo) }}"
                                                    class="rounded-circle profile-img" alt="avatar"></span>
                                        </td>
                                        <td class="text-center"> <label
                                                class="switch s-icons s-outline  s-outline-primary">
                                                <input type="checkbox" class="toggle-class-instructor" name="status"
                                                    data-id="{{ $item->id }}"
                                                    {{ $item->status == '1' ? 'checked' : '' }}>
                                                <span class="slider round"></span>
                                                </span></td>
                                        <td class="text-center actions-tb-box">
                                            <meta name="csrf-token" content="{{ csrf_token() }}">
                                            <a class="instdeleteRecord" data-id="{{ $item->id }}"><i
                                                    class="fa fa-trash-o"></i></a> |
                                            <a class=""
                                                href="{{ route('admin-instructor-create', $item->id) }}"><i
                                                    class="fa fa-pencil"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('script')
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script>
          
            $(function() {
                $(document).on("change",'.toggle-class-instructor',function() {
                    var status = $(this).prop('checked') == true ? 1 : 0;
                    var user_id = $(this).data('id');
                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: "{{ route('instructor-status') }}",
                        data: {
                            'status': status,
                            'user_id': user_id
                        },
                        success: function(data) {
                            console.log(data.success)
                        }
                    });
                })
            })
            $(".instdeleteRecord").click(function() {
                var row = $(this).parent().parent();
                var id = $(this).data("id");
                var token = $("meta[name='csrf-token']").attr("content");
                var url = '{{ route('instructor-delete', ':id') }}';
                url = url.replace(':id', id);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: url,
                            type: 'DELETE',
                            data: {
                                "id": id,
                                "_token": token,
                            },
                            success: function(response) {
                                if (response.success) {
                                    Swal.fire(
                                        'Deleted!',
                                        'Your file has been deleted.',
                                        'success'
                                        ) 
                                        $(row).hide(2000);
                                }else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: response.error,
                                    })
                                }
                            },
                        });
                       
                    }
                })
            });
        </script>
        <script>
              // var e;
              c2 = $('#style-2').DataTable({
                "dom": "<'dt--top-section mr-0 ml-0'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                    "<'table-responsive'tr>" +
                    "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
                headerCallback: function(e, a, t, n, s) {
                    e.getElementsByTagName("th")[0].innerHTML =
                        '<label class="new-control new-checkbox checkbox-outline-info m-auto">\n<input type="checkbox" class="new-control-input chk-parent select-customers-info" id="customer-all-info">\n<span class="new-control-indicator"></span><span style="visibility:hidden">c</span>\n</label>'
                },
                columnDefs: [{
                    targets: 0,
                    width: "30px",
                    className: "",
                    orderable: !1,
                    render: function(e, a, t, n) {
                        return '<label class="new-control new-checkbox checkbox-outline-info  m-auto">\n<input type="checkbox" class="new-control-input child-chk select-customers-info" id="customer-all-info">\n<span class="new-control-indicator"></span><span style="visibility:hidden">c</span>\n</label>'
                    }
                }],
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                        "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                    },
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                    "sLengthMenu": "Results :  _MENU_",
                },
                "lengthMenu": [5, 10, 20, 50],
                "pageLength": 5
            });
            multiCheck(c2);
        </script>

    @endpush
@endsection
