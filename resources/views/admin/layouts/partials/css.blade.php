<link rel="icon" type="image/x-icon" href="{{ asset('adminassets/img/favicon.ico') }}" />
<link href="{{ asset('adminassets/css/loader.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ asset('adminassets/js/loader.js') }}"></script>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
<link href="{{ asset('adminassets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('adminassets/css/plugins.css') }}" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/plugins/table/datatable/datatables.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/css/forms/theme-checkbox-radio.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/plugins/table/datatable/dt-global_style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/plugins/table/datatable/custom_dt_custom.css') }}">
<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
<link href="{{ asset('adminassets/plugins/apex/apexcharts.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('adminassets/css/dashboard/dash_1.css') }}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
<link href="{{ asset('adminassets/css/scrollspyNav.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('adminassets/plugins/editors/markdown/simplemde.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/css/forms/switches.css') }}">
<link href="{{ asset('adminassets/plugins/flatpickr/flatpickr.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('adminassets/plugins/flatpickr/custom-flatpickr.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/plugins/dropify/dropify.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/plugins/select2/select2.min.css') }}">
<link href="{{ asset('adminassets/css/apps/invoice-edit.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/css/elements/alert.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/css/admin-commen.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('adminassets/css/mdtimepicker.css') }}">
<link href="{{asset('adminassets/css/components/tabs-accordian/custom-tabs.css')}}" rel="stylesheet" type="text/css" />

@stack('css')
