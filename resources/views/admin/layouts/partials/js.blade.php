	<script src="{{asset('adminassets/js/libs/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('adminassets/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{asset('adminassets/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('adminassets/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{asset('adminassets/js/app.js')}}"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="{{asset('adminassets/js/custom.js')}}"></script>
    <script src="{{asset('adminassets/plugins/apex/apexcharts.min.js')}}"></script>
    <script src="{{asset('adminassets/js/dashboard/dash_1.js')}}"></script>
    <script src="{{asset('adminassets/plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('adminassets/js/scrollspyNav.js')}}"></script>
    <script src="{{asset('adminassets/plugins/editors/markdown/simplemde.min.js')}}"></script>
    <script src="{{asset('adminassets/plugins/editors/markdown/custom-markdown.js')}}"></script>

        <script src="{{asset('adminassets/plugins/select2/select2.min.js') }}"></script>
        <script src="{{ asset('adminassets/plugins/select2/custom-select2.js') }}"></script>
        <script src="{{ asset('adminassets/js/ckeditor/ckeditor.js') }}"></script>
        <script src="{{ asset('adminassets/js/ckeditor/ckeditor-inner.js') }}"></script>
        <script src="{{asset('adminassets/plugins/dropify/dropify.min.js')}}"></script>
        <script src="{{asset('adminassets/js/apps/invoice-edit.js')}}"></script>
        <script src="{{asset('adminassets/plugins/flatpickr/flatpickr.js')}}"></script>
        <script src="{{asset('adminassets/plugins/flatpickr/custom-flatpickr.js')}}"></script>
        <script>
            initSample();
        </script>
    @stack('script')
    