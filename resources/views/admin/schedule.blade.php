@extends('admin.layouts.master')
@section('title', 'Skill And More Admin Panel')
<style>
    .error {
        color: red;
    }

        .visi {
            visibility: hidden;
        }

        .datepicker .form-control,
        .timepicker .form-control,
        .datetimepicker .form-control {
            background: #fff;
        }

    
</style>
@section('content')
    <div class="layout-px-spacing">
        <div class="seperator-header layout-top-spacing">
        </div>
        <div id="flFormsGrid" class="col-lg-12 layout-spacing">
            <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>Forms Schedule</h4>
                </div>
            </div>
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                </div>
                <div class="widget-content widget-content-area">
                    <form action="{{ route('schedule-store') }}" method="post" enctype="multipart/form-data" id="form">
                        @csrf
                        <input type="hidden" name="course_id" value="{{ $course_id->id }}">
                        <div class="row">
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Category</label>
                                    <input class="form-control" type="text" readonly value="{{ $cat->name }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Course Name</label>
                                    <input class="form-control" value="{{ course($course_id->name) }}" readonly>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Duration</label>
                                    <input type="number" value="{{ $course_id->duration }}" class="form-control"
                                        readonly placeholder="Duration">
                                    @error('Duration')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Batch Start Date</label>
                                    <div class="form-group mb-0">
                                        <input id="basicFlatpickr" name="on_date"
                                            class="form-control flatpickr flatpickr-input active" type="text"
                                            placeholder="Select Date..">
                                        @error('on_date')
                                            <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <label>Frequency</label>
                                <div class="row">
                                    <div class="form-check mb-2">
                                        <div class="custom-control custom-radio classic-radio-info">
                                            <input type="radio" id="hRadio2" value="W" name="class_frequency"
                                                class="custom-control-input" checked>
                                            <label class="custom-control-label" for="hRadio2">Weakly</label>
                                        </div>
                                    </div>
                                    <div class="form-check mb-2">
                                        <div class="custom-control custom-radio classic-radio-info">
                                            <input type="radio" value="M" id="hRadio1" name="class_frequency"
                                                class="custom-control-input">
                                            <label class="custom-control-label" for="hRadio1">Monthly</label>
                                        </div>
                                    </div>
                                </div>
                                @error('class_frequency')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Status</label>
                                    <div class="form-group mb-0">
                                        <label class="switch s-icons s-outline  s-outline-primary">
                                            <input type="checkbox" class="toggle-class-courses" value="1" name="status"
                                                data-id="" checked>
                                            <span class="slider round"></span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Batch Name</label>
                                    <div class="form-group mb-0">
                                        <input id="" class="form-control" name="batch_name" type="text"
                                            placeholder="Batch">
                                        @error('batch_name')
                                            <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Event Name</label>
                                    <div class="form-group mb-0">
                                        <input class="form-control" name="event_name" type="text" placeholder="Event">
                                        @error('event_name')
                                            <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Duration(in Week)</label>
                                    <div class="form-group mb-0">
                                        <input type="number" class="form-control" name="week" placeholder="Duration">
                                        @error('week')
                                            <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Class (Per Week)</label>
                                    <div class="form-group mb-0">
                                        <select class="custom-select">
                                            <option disabled="disabled" selected="selected">Select Class</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="myTable">
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary " id="myTable">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('script')
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="{{ asset('adminassets/js/mdtimepicker.js') }}"></script>
        <script>
            $('select').change(function() {
                var select = $('#myTable').html('');
                for (var i = 0; i < parseInt($(this).val()); i++) {
                    var row =
                        '<div class="row">\
                                    <div class="col-12 col-sm-4"><div class="form-group"><label>Select Day</label>\
                                        <div><select class="custom-select"  name="schedule[]"><option disabled="disabled" selected="selected">Select Day</option>\
                                            <option value="sunday">Sunday</option>\
                                            <option value="monday">Monday</option>\
                                            <option value="tuesday">Tuesday</option>\
                                            <option value="wednesday">Wednesday</option>\
                                            <option value="thursday">Thursday</option>\
                                            <option value="friday">Friday</option>\
                                            <option value="saturday">Saturday</option>\
                                            </select></div></div></div>\
                                            <div class="col-12 col-sm-4"><div class="form-group"><label>Strat Time</label>\
                                            <div class="form-group mb-0"><input   class="form-control timepicker" name="on_time[]" type="text" placeholder="Time"></div></div></div>\
                                            <div class="col-12 col-sm-4"><div class="form-group"><label>End Time</label>\
                                            <div class="form-group mb-0"><input   class="form-control timepicker" name="end_time[]" type="text" placeholder="Time"></div></div>\
                                            </div></div>';
                    row = $(row);
                    $('#myTable').append(row);
                }
                select.promise().done(function(arg1) {
                    $('.timepicker').mdtimepicker();
                })
            });
        </script>
        <script>
            $(document).ready(function() {
                $('.timepicker').mdtimepicker();
            });
        </script>
        <script>
            $(document).ready(function() {
                $('#form').validate({
                    ignore: '',
                    rules: {
                        on_date: {
                            required: true
                        },
                        batch_name: {
                            required: true,
                        },
                        class_frequency: {
                            required: true,

                        },
                        event_name: {
                            required: true,

                        },
                        week: {
                            required: true,

                        },
                        "schedule[]": {
                            required: true,

                        },
                        'on_time[]': {
                            required: true,

                        },
                        'end_time[]': {
                            required: true,

                        },
                    }
                });
            });
        </script>
        
    @endpush
@endsection
