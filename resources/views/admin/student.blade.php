@extends('admin.layouts.master')
@section('title', 'Skill And More Admin Panel')
@push('css')
    <style>
        .course {
            width: 140px;
            height: 140px;
        }

    </style>
@endpush
@section('content')
    <div class="layout-px-spacing">
        <div class="seperator-header layout-top-spacing">
            <h4 class="">Courses</h4>
        </div>
        <div class="row layout-spacing">
            <div class="col-lg-12">
                <div class="statbox widget box box-shadow">
                    @if (Session::has('message'))
                        <div class="alert alert-light-primary border-0 mb-4" role="alert"> <button type="button"
                                class="close" data-dismiss="alert" aria-label="Close"> <svg
                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg></button>{{ Session::get('message') }} </div>
                    @endif
                    <div class="col-lg-10">
                        <h4>Student Status</h4>
                    </div><br>
                    <div class="row">
                        <div class="col-12 col-sm-2">
                            <div class="form-group">Batch Name:</div>
                            <div class="form-group">Frequency:</div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">{{ batchname($batch[0]->batch_name) }}</div>
                            <div class="form-group">
                                @if ($batch[0]->class_frequency == 'W')
                                    (Week)
                                @elseif($batch[0]->class_frequency == 'M')
                                    (Month)
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="tab-content mb-4" id="border-tabsContent">
                        <div class="tab-pane fade show active" id="border-home" role="tabpanel"
                            aria-labelledby="border-home-tab">
                            <div class="widget-content widget-content-area">
                                <table id="style-2" class="table style-2  table-hover">
                                    <thead>
                                        <tr>
                                            <th class="checkbox-column dt-no-sorting"> Record Id </th>
                                            <th>Id</th>
                                            <th>StudentName</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($course as $key => $item)
                                        @if ($item->schedule_status == 0)
                                            <tr>
                                                <td class="checkbox-column"> 1 </td>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $item->studentName }}</td>
                                                <td>
                                                    @if ($item->status == 'pending')
                                                        <span class="badge badge-info">{{ $item->status }}</span>
                                                    @elseif($item->status == 'active')
                                                        <span class="badge badge-success">{{ $item->status }}</span>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <div style="width:110px">
                                                        @if ($item->status == 'active')
                                                            <a href="{{ route('admin.storestudent', [$item->id, $batch[0]->batch_name]) }}"
                                                                class="btn btn-sm btn-success"><i
                                                                    class="fa fa-check"></i></a>
                                                        @else
                                                            <div style="width:110px">
                                                                <button class="btn btn-sm btn-danger"><i
                                                                        class="fa fa-ban"></i></a>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @push('script')
        <script src="{{ asset('adminassets/js/custom-search.js') }}"></script>
    @endpush
@endsection
