<!DOCTYPE html>
<html>
   <head>
      <title>Sign In</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontawsome.min.css')}}" />
      <link rel="icon" type="image/gif" href="{{asset('assets/images/favicon.png')}}" sizes="16x16">
      <!-- My CSS -->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/common.css')}}" />
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/login-page.css')}}" />
   </head>
   <body>
      <section class="login-outer">
         <div class="login-area">
            <div class="header-area">
               <a href="{{route('home-page')}}" class="d-block text-center">
               <img src="{{asset('assets/images/logo.svg')}}" class="lg-logo" alt="logo">
               </a>
               <h2 class="title text-center">Sign In</h2>
               <div class="social-link">
                  <ul class="d-flex justify-content-center">
                     <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                     <li><a href=""><i class="fab fa-google"></i></a></li>
                     <li><a href=""><i class="fab fa-linkedin-in"></i></a></li>
                  </ul>
               </div>
               <p class="text-center mt-4">or use your email acount</p>

            </div>
            <div class="login-form">
               <form>
                  <div class="custom-input-box">
                     <span><i class="fal fa-envelope"></i></span>
                     <input type="email" name="email" class="form-control" placeholder="Email" autocomplete="off">
                  </div>
                  <div class="custom-input-box">
                     <span><i class="fal fa-lock"></i></span>
                     <input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off">
                     <div>
                        <i class="fas fa-eye-slash"></i>
                     </div>
                  </div>
                  <ul class="row mt-4">
                     <li class="col-lg-6 col-md-6 col-sm-6 col-6 form-group">
                        <input type="checkbox" class="form-check-input" id="keep-login">
                        <label class="form-check-label" for="keep-login">Keep me logged in</label>
                     </li>
                     <li class="col-lg-6 col-md-6 col-sm-6 col-6 d-flex justify-content-end">
                        <a href="javascript:void(0);">Forgot Password?</a>
                     </li>
                  </ul>
                  <input id="authdata" type="hidden" value="Authenticating...">
                  <button class="btn login_btn" type="submit">SIGN IN</button>
                  <!-- <a href="javascript:void(0);" class="btn login_btn">Login</a> -->
                  <p class="text-center mt-4">Not Registered yet ? <a href="{{url('/register')}}" class="link-logs">Create an account</a></p>
               </form>
            </div>
         </div>
         <div>
            <img src="{{asset('assets/images/shapes/boy-num.svg')}}" class="icon-hour">
            <img src="{{asset('assets/images/shapes/arrow-icon.svg')}}" class="arrow-icon">
            <img src="{{asset('assets/images/shapes/arrow-1.svg')}}" class="arrow-1">
         </div>
         <ul class="bg-bubbles">
            <li><img src="{{asset('assets/images/shapes/sp1.svg')}}" ></li>
            <li><img src="{{asset('assets/images/shapes/sp12.svg')}}" ></li>
            <li><img src="{{asset('assets/images/shapes/sp3.svg')}}" ></li>
            <li><img src="{{asset('assets/images/shapes/sp8.svg')}}" ></li>
            <li><img src="{{asset('assets/images/shapes/sp4.svg')}}"></li>
            <li><img src="{{asset('assets/images/shapes/sp5.svg')}}" ></li>
            <li><img src="{{asset('assets/images/shapes/sp13.svg')}}" ></li>
            <li><img src="{{asset('assets/images/shapes//sp9.svg')}}" ></li>
            <li><img src="{{asset('assets/images/shapes/sp10.svg')}}" ></li>
            <li><img src="{{asset('assets/images/shapes/sp11.svg')}}" ></li>
         </ul>
      </section>
      <!-- Bootstrap js  -->
      <script type="text/javascript" src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
   </body>
</html>
