<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <title>Live Online Coding, Art, Music & Sketching Classes for Kids</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="meta description: SkillsnMore is an online learning platform for kids of 5-16 yrs age groups. We offer online coding classes, online programming courses, music classes for children and drawing classes.">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontawsome.min.css')}}" />
      <link rel="icon" type="image/gif" href="{{asset('assets/images/favicon.png')}}" sizes="16x16">
      <link rel="stylesheet" href="{{asset('assets/css/aos.css')}}" />
      <!-- slick slider css -->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/slick.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/slick-theme.css')}}">
      <!-- My CSS -->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/common.css')}}" />
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/header.css')}}" />
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/footer.css')}}" />
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}" />
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}" />
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/select2.min.css')}}" />
      <!-- Global site tag (gtag.js) - Google Analytics -->
        {!! env('gtag') !!}
   </head>
   <body>
      @include('layouts.header')
      @yield('content')
      @include('layouts.footer')
      <!-- include jquery jquery-3.6.0-->
      <script src="{{asset('assets/js/jquery-3.6.0.js')}}"></script>
      <!-- Bootstrap js  -->
      <script type="text/javascript" src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/aos.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/slick.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/header.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/moving.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/js/main.js')}}"></script>
      <script src="{{asset('assets/js/sweetalert.min.js')}}"></script>
      <script src="{{asset('assets/js/jquery.mask.min.js')}}"></script>
      <script src="{{asset('assets/js/select2.min.js')}}"></script>
      <script>
         $('.phone').mask('000-000-0000');
         $('.select2').select2({
             placeholder: "Select an option",
             allowClear: true
        });
      </script>
      <script type="text/javascript">
         jQuery(function() {
             var minimized_elements = $('.read_more');
             minimized_elements.each(function() {
                 var t = $(this).text();
                 if (t.length < 300) return;
                 $(this).html(t.slice(0, 300) + '<span>.</span><a href="javascript:;" class="btn  read-m-team clr-btn more">Read More</a>' +
                     '<span style="display:none;">' + t.slice(300, t.length) +
                     ' <a href="javascript:;" class="btn  read-m-team clr-btn less">Read Less</a></span>');
             });
             $('a.more', minimized_elements).click(function(event) {
                 event.preventDefault();
                 $(this).hide().prev().hide();
                 $(this).next().show();
             });
             $('a.less', minimized_elements).click(function(event) {
                 event.preventDefault();
                 $(this).parent().hide().prev().show().prev().show();
             });
         });
      </script>
      <script>
         AOS.init();
         selected_menu = "{{ $menu_index ?? ''}}";
         
         if (selected_menu != null && selected_menu != "") {
             $('.left-side-nav').removeClass('active');
             console.log("SELECTED MENU-->" + selected_menu)
             $('#' + selected_menu).addClass('active');
             setTimeout(function() {
                 $('#' + selected_menu).closest('.has-child').find('a').click();
             }, 1000);
         }
      </script>
      <script>
         function validateForm(validationform) {
             error = false;
             console.log("ERROR DEFINE->>" + error)
             form = validationform.closest('.modal').find('form');
             form.find('input,select').each(function() {
                 if ($(this).attr('required') == 'required' && ($(this).val() == null || $(this).val() == "")) {
                     console.log("VALUE BLANK->>" + error + "---" + $(this).attr('name'))
                     error = true;
                     $(this).addClass('is-invalid');
                     $(this).after('<span class="text-danger">This field is required!</span>');
                 }
                 if ($(this).hasClass('email-validate')) {
         
                     $(this).removeClass('is-invalid');
                     $(this).parent().find('.text-danger').remove();
                     emailbox = $(this)
                     email = $(this).val()
                     var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                     test = regex.test(email);
                     if (test == false) {
                         error = true;
                         emailbox.addClass('is-invalid');
                         emailbox.after('<span class="text-danger">Invalid email address!</span>');
                     }
                 }
             })
             return error;
         }
         
        $(document).on('click', '.submitform', function() {
            formid = $(this).attr('form-id')
            error = validateForm($(this));
            console.log("ERROR->> " + error)
            if (error == false) {
                $('.bookdemo').prop('disabled', true);
                var formData = new FormData($('#'+formid)[0]); 
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });
                $.ajax({
                    url: "{{ route('signup-teacher') }}",
                    type: "POST",
                    data: formData,
                    success: function(data) {
                        $('.bookdemo').prop('disabled', false);
                        $('.textdanger').hide();
                        $('.modal').modal('hide');
                        $('.thankmsg').modal('show');
                    },
                    error: function(response) {
                        $('.textdanger').html('');
                        $.each(response.responseJSON.errors, function(field_name, error) {
                            $(document).find('[name=' + field_name + ']').after(
                                '<span class="text-strong textdanger">' + error + '</span>')
                        })
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        
        })
         
         $('.modalStateList').change(function() {
             statelist = $(this);
             citylist = $(this).attr('citylist');
             $('#' + citylist).empty();
             // $('#modalCityList').empty();
             stateid = $(this).find('option:selected').attr('state-id');
             console.log("STATE-->" + stateid);
             var sel = document.getElementById(citylist);
             var opt = document.createElement('option');
             opt.appendChild(document.createTextNode('Loading...'));
             opt.value = '';
             sel.appendChild(opt);
             $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': "{{ csrf_token() }}"
                 }
             });
         
             $.ajax({
                 url: "{{ route('front-getcities') }}",
                 type: "POST",
                 data: {
                     stateid: stateid
                 },
                 success: function(data) {
                     $('#' + citylist).empty();
                     var sel = document.getElementById(citylist);
                     var opt = document.createElement('option');
                     opt.appendChild(document.createTextNode('Please Select a City'));
                     opt.value = '';
                     sel.appendChild(opt);
                     $.each(data, function(index) {
                         var sel = document.getElementById(citylist);
                         var opt = document.createElement('option');
                         opt.appendChild(document.createTextNode(data[index].name));
                         sel.appendChild(opt);
         
                     });
                 }
             })
         });
         $(".modal").on('hidden.bs.modal', function() {
         
             $(this).find('form').trigger('reset');;
         });
         jQuery(function() {
            var minimized_elements = $('.read_more_text');
            minimized_elements.each(function() {
                var t = $(this).text();
                if (t.length < 300) return;
                $(this).html(
                    t.slice(0, 300) + '<span>... </span><a href="#" class="more">Read More</a>' +
                    '<span style="display:none;">' + t.slice(300, t.length) + ' <a href="#" class="less">Read Less</a></span>'
                    );
            });
            $('a.more', minimized_elements).click(function(event) {
                event.preventDefault();
                $(this).hide().prev().hide();
                $(this).next().show();
            });
            $('a.less', minimized_elements).click(function(event) {
                event.preventDefault();
                $(this).parent().hide().prev().show().prev().show();
            });
        });
      </script>

      @yield('script')
   </body>
</html>
