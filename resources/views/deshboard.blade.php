@include('layouts.header')
@yield('content')
@include('layouts.footer')
<!-- include jquery jquery-3.6.0-->
<script src="{{asset('assets/js/jquery-3.6.0.js')}}"></script>
<!-- Bootstrap js  -->
<script type="text/javascript" src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/aos.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/slick.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/header.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/moving.js')}}"></script>
<script src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.mask.min.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
<script>
   $('.phone').mask('000-000-0000');
</script>
<script type="text/javascript">
   jQuery(function() {
       var minimized_elements = $('.read_more');
       minimized_elements.each(function() {
           var t = $(this).text();
           if (t.length < 300) return;
           $(this).html(t.slice(0, 300) + '<span>.</span><a href="javascript:;" class="btn  read-m-team clr-btn more">Read More</a>' +
               '<span style="display:none;">' + t.slice(300, t.length) +
               ' <a href="javascript:;" class="btn  read-m-team clr-btn less">Read Less</a></span>');
       });
       $('a.more', minimized_elements).click(function(event) {
           event.preventDefault();
           $(this).hide().prev().hide();
           $(this).next().show();
       });
       $('a.less', minimized_elements).click(function(event) {
           event.preventDefault();
           $(this).parent().hide().prev().show().prev().show();
       });
   });
</script>
<script>
   AOS.init();
   selected_menu = "{{ $menu_index ?? ''}}";
   if (selected_menu != null) {
       $('.left-side-nav').removeClass('active');
       $('#' + selected_menu).addClass('active');
       setTimeout(function() {
           $('#' + selected_menu).closest('.has-child').find('a').click();
       }, 1000);
   }
</script>
<script>
   $(document).on('click', '.sign-up-pop', function() {
       $('.question-s').modal('show');
   })
   $(document).on('click', '.signupteacher', function() {
       $('.question-s').modal('hide');
       $('.signup-teacher').modal('show');
   })
   $(document).on('click', '.signupstudent', function() {
       $('.question-s').modal('hide');
       $('.signup-student').modal('show');
   })
   $(document).on('click', '.sign-in-pop', function() {
       $('.question-s-signin').modal('show');
   })
   $(document).on('click', '.siginTeacher', function() {
       $('.question-s-signin').modal('hide');
       $('.sigin-teacher').modal('show');
   })
   $(document).on('click', '.siginStudent', function() {
       $('.question-s-signin').modal('hide');
       $('.sigin-student').modal('show');
   })
   $(document).on('click', '.explore-c', function() {
       $('.Explore-course').modal('show');
   })
   $(document).on('click', '.partner-pop', function() {
       $('.partnerpop').modal('show');
   })   
   $(document).on('click', '.thankyoupop', function() {
       $('.course-modal').modal('hide');
       $('.thankmsg').modal('show');
   })
   $(document).on('click', '.studentsignup', function() {
       $('.studentsignup').prop('disabled', true);
       var formData = new FormData($('.studentSignup')[0]);
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': "{{ csrf_token() }}"
           }
       });
       $.ajax({
           url: "{{ route('signup-teacher') }}",
           type: "POST",
           data: formData,
           success: function(data) {
               $('.studentsignup').prop('disabled', false);
               $('.textdanger').hide();
               if (data.success == "success") {
                   swal({
                       title: "Success!",
                       text: "Successfully saved operator",
                       icon: "success",
                       button: "Close",
                   })
               }
           },
           error: function(response) {
               $('.textdanger').html('');
               $.each(response.responseJSON.errors, function(field_name, error) {
                   $(document).find('[name=' + field_name + ']').after(
                       '<span class="text-strong textdanger">' + error + '</span>')
               })
           },
           cache: false,
           contentType: false,
           processData: false
       });
       
   })
   $(document).on('click', '.partnersignup', function() {
       $('.partnersignup').prop('disabled', true);
       var formData = new FormData($('.partnerSignup')[0]);
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': "{{ csrf_token() }}"
           }
       });
       $.ajax({
           url: "{{ route('signup-teacher') }}",
           type: "POST",
           data: formData,
           success: function(data) {
               $('.partnersignup').prop('disabled', false);
               $('.textdanger').hide();
               if (data.success == "success") {
                   swal({
                       title: "Success!",
                       text: "Successfully saved operator",
                       icon: "success",
                       button: "Close",
                   })
               }
           },
           error: function(response) {
               $('.textdanger').html('');
               $.each(response.responseJSON.errors, function(field_name, error) {
                   $(document).find('[name=' + field_name + ']').after(
                       '<span class="text-strong textdanger">' + error + '</span>')
               })
           },
           cache: false,
           contentType: false,
           processData: false
       });
       
   })
   $(document).on('click', '.teachersignup', function() {
       $('.teachersignup').prop('disabled', true);
       var formData = new FormData($('.teacherSignup')[0]);
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': "{{ csrf_token() }}"
           }
       });
       $.ajax({
           url: "{{ route('signup-teacher') }}",
           type: "POST",
           data: formData,
           success: function(data) {
               $('.teachersignup').prop('disabled', false);
               $('.textdanger').hide();
               if (data.success == "success") {
                   swal({
                       title: "Success!",
                       text: "Successfully saved operator",
                       icon: "success",
                       button: "Close",
                   })
               }
           },
           error: function(response) {
               $('.textdanger').html('');
               $.each(response.responseJSON.errors, function(field_name, error) {
                   $(document).find('[name=' + field_name + ']').after(
                       '<span class="text-strong textdanger">' + error + '</span>')
               })
           },
           cache: false,
           contentType: false,
           processData: false
       });
       
   })
   $(document).on('click', '.bookdemo', function() {
       $('.bookdemo').prop('disabled', true);
       var formData = new FormData($('.bookDemo')[0]);
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': "{{ csrf_token() }}"
           }
       });
       $.ajax({
           url: "{{ route('signup-teacher') }}",
           type: "POST",
           data: formData,
           success: function(data) {
               $('.bookdemo').prop('disabled', false);
               $('.textdanger').hide();
               if (data.success == "success") {
                   swal({
                       title: "Success!",
                       text: "Successfully saved operator",
                       icon: "success",
                       button: "Close",
                   })
               }
           },
           error: function(response) {
               $('.textdanger').html('');
               $.each(response.responseJSON.errors, function(field_name, error) {
                   $(document).find('[name=' + field_name + ']').after(
                       '<span class="text-strong textdanger">' + error + '</span>')
               })
           },
           cache: false,
           contentType: false,
           processData: false
       });
       
   })
   
   $('.modalStateList').change(function() {
       statelist = $(this);
       citylist = $(this).attr('citylist');
       $('#' + citylist).empty();
           // $('#modalCityList').empty();
           stateid = $(this).find('option:selected').attr('state-id');
           console.log("STATE-->" + stateid);
           var sel = document.getElementById(citylist);
           var opt = document.createElement('option');
           opt.appendChild(document.createTextNode('Loading...'));
           opt.value = '';
           sel.appendChild(opt);
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': "{{ csrf_token() }}"
               }
           });
           
           $.ajax({
               url: "{{ route('front-getcities') }}",
               type: "POST",
               data: {
                   stateid: stateid
               },
               success: function(data) {
                   $('#' + citylist).empty();
                   var sel = document.getElementById(citylist);
                   var opt = document.createElement('option');
                   opt.appendChild(document.createTextNode('Please Select a City'));
                   opt.value = '';
                   sel.appendChild(opt);
                   $.each(data, function(index) {
                       var sel = document.getElementById(citylist);
                       var opt = document.createElement('option');
                       opt.appendChild(document.createTextNode(data[index].name));
                       sel.appendChild(opt);
                       
                   });
               }
           })
       });
   $(".modal").on('hidden.bs.modal', function() {
       
       $(this).find('form').trigger('reset');;
   });
</script>
@yield('script')
</body>
</html>
<!-- @yield('scripts') -->