@extends('dashboard')
@section('content')
@php
$course = isset($_GET['course']) ? $_GET['course'] : "";
@endphp
<section class="inner-page-banner">
   <span class="banner-shape-1 bannershape-animte">
   <img src="{{asset('assets/images/shape-1.png')}}" alt="shape">
   </span>
   <span class="banner-shape-2 bannershape-animte">
   <img src="{{asset('assets/images/shape-2.png')}}" alt="shape">
   </span>
   <span class="banner-shape-3 bannershape-animte">
   <img src="{{asset('assets/images/shape-3.png')}}" alt="shape">
   </span>
   <span class="banner-shape-4 bannershape-animte">
   <img src="{{asset('assets/images/shape-4.png')}}" alt="shape">
   </span>
   <div class="container">
      <h1>Book Demo Class</h1>
   </div>
</section>
<!-- banner End -->
<!-- Breadcum Start -->
<div class="theme-breadcum-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <div class="theme-breadcrum">
               <ul>
                  <li><a href="{{route('home-page')}}">Home</a></li>
                  <li><i class="fal fa-chevron-right"></i></li>
                  <li>Book a Demo Class</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Breadcum End -->
<!-- Our Courses Section Start -->

   <section class="course-detail">
      <div class="container">
        <div class="sub-heading-section mb-5">
            <h2 class="text-center pb-1">Book a Demo Class with our Experts</h2>
            <p class="text-center">Please fill in following information and select a convinient time to schedule a demo.</p>
         </div>
          <div class="row justify-content-center">
              <div class="col-lg-10">

                 <form class="" id="bookDemo-page">
                    <div class="row">
                       <div class="col-md-6">
                          <div class="mb-4">
                             <label class="mb-2">Parent/Guardian's Name<i class="far fa-asterisk"></i></label>
                             <input type="text" name="Parent-Name" class="form-control" required autocomplete="off">
                          </div>
                       </div>
                       <div class="col-md-6">
                          <div class="mb-4">
                             <label class="mb-2">Student's Name<i class="far fa-asterisk"></i></label>
                             <input type="text" name="Student-Name" class="form-control" required autocomplete="off">
                          </div>
                       </div>
                       <div class="col-md-6">
                          <div class="mb-4">
                             <label class="mb-2">Age<i class="far fa-asterisk"></i></label>
                             <input type="text" name="Age" class="form-control" required autocomplete="off">
                          </div>
                       </div>
                       <div class="col-md-6">
                          <div class="mb-4">
                             <label class="mb-2">Mobile Number<i class="far fa-asterisk"></i></label>
                             <input type="text" name="Phone" class="form-control phone" required autocomplete="off">
                          </div>
                       </div>
                       <div class="col-md-6">
                          <div class="mb-4">
                             <label class="mb-2">Email Id<i class="far fa-asterisk"></i></label>
                             <input type="email" name="Email" class="form-control email-validate" required autocomplete="off">
                          </div>
                       </div>
                       <input type="hidden" name="selected_course" value="{{$course_name}}">
                       <div class="col-md-6">
                          <div class="mb-4">
                             <label class="mb-2">Course<i class="far fa-asterisk"></i></label>
                             <select class="form-select form-control select2" name="Subject" aria-label="Default select example" required multiple>
                             @foreach($categories as $modal_cat)
                             <option value="{{$modal_cat->name}}" @if($modal_cat->slug == $course_name) selected @endif>{{$modal_cat->name}}</option>
                             @endforeach
                             </select>
                          </div>
                       </div>
                       <div class="col-md-4">
                          <div class="mb-4">
                             <label class="mb-2">Country<i class="far fa-asterisk"></i></label>
                             <select class="form-select form-control" name="Country" aria-label="Default select example" id="countryList" required>
                             @foreach($country_list as $key => $country)
                             <option value="{{$country->name}}" country-id="{{$country->id}}" @if($country->id == 231) selected @endif>{{$country->name}}</option>
                             @endforeach
                             </select>
                          </div>
                       </div>
                       <div class="col-md-4">
                          <div class="mb-4">
                             <label class="mb-2">State<i class="far fa-asterisk"></i></label>
                             <select class="form-select form-control modalStateList" name="State" aria-label="Default select example" citylist="modalCityList1" id="statesList" required>
                                <option value="">Select a country first</option>
                             </select>
                          </div>
                       </div>
                       <div class="col-md-4">
                          <div class="mb-4">
                             <label class="mb-2">City<i class="far fa-asterisk"></i></label>
                             <select class="form-select form-control" name="City" aria-label="Default select example" id="modalCityList1" required>
                                <option value="">Select State First</option>
                             </select>
                          </div>
                       </div>
                       <div class="col-md-12 outer-demos mt-1">
                          <div class="row">
                             <h6 class="text-head">Please select a slot for demo:</h6>
                             @foreach($dateArr as $key=>$date)
                             <div class="col-lg-4 col-sm-6 outer-slot-card">
                                <label class="form-check slot-cards" for="{{$key}}-1-options">
                                   <input class="form-check-input" type="radio" name="timeslot" id="{{$key}}-1-options" value="{{$date}}-8AM CST" @if($key == 0) checked @endif>
                                   <p class="text-label">{{$date}} @ 8:00 AM CST</p>
                                </label>
                             </div>
                             <div class="col-lg-4 col-sm-6 outer-slot-card">
                                <label class="form-check slot-cards" for="{{$key}}-2-options">
                                   <input class="form-check-input" type="radio" name="timeslot" id="{{$key}}-2-options" value="{{$date}}-9AM CST">
                                   <p class="text-label">{{$date}} @ 9:00 AM CST</p>
                                </label>
                             </div>
                             @endforeach
                             <div class="col-lg-4 col-sm-6  outer-slot-card" >
                                <label class="form-check slot-cards" for="otherOption">
                                   <input class="form-check-input" type="radio" name="timeslot" id="otherOption" value="other">
                                   <p class="text-label">Other</p>
                                </label>
                             </div>
                          </div>
                       </div>
                       <div class="col-md-12 text-center mt-5">
                          <button type="submit" class="theme-btn" form-id="bookDemo-page">Submit</button>
                       </div>
                 </form>
              </div>
          </div>

         </div>
   </section>

@endsection
@section('script')
<script>
   $(document).ready(function(){
       $('#countryList').trigger('change');
   })

   $(document).on('change', '#course-category', function() {
       slug = $(this).val();
       window.location.href = '{{route("front-course-category-all")}}' + "/" + slug;
   })

   $('#bookDemo-page').submit(function(e){
       form = $(this)
       e.preventDefault();
       $('button[type="submit"]').prop('disabled',true)
       formData = new FormData(form[0]);
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': "{{ csrf_token() }}"
           }
       });
       $.ajax({
           url: "{{ route('signup-teacher') }}",
           type: "POST",
           data: formData,
           success: function(data) {
               $('button[type="submit"]').prop('disabled', false);
               $('.textdanger').hide();
               $('.modal').modal('hide');
               $('.thankmsg').modal('show');
               form.trigger('reset')
           },
           error: function(response) {
               $('.textdanger').html('');
               $.each(response.responseJSON.errors, function(field_name, error) {
                   $(document).find('[name=' + field_name + ']').after(
                       '<span class="text-strong textdanger">' + error + '</span>')
               })
           },
           cache: false,
           contentType: false,
           processData: false
       });
   })

   $('#countryList').change(function() {
     $('#statesList').empty();
     // $('#modalCityList').empty();
     countryid = $(this).find('option:selected').attr('country-id');
     console.log("country-->" + countryid);
     var sel = document.getElementById('statesList');
     var opt = document.createElement('option');
     opt.appendChild(document.createTextNode('Loading...'));
     opt.value = '';
     sel.appendChild(opt);
     $.ajaxSetup({
        headers: {
           'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
     });

     $.ajax({
        url: "{{ route('front-getstates') }}",
        type: "POST",
        data: {
           countryid: countryid
        },
        success: function(data) {
           $('#statesList').empty();
           var sel = document.getElementById('statesList');
           var opt = document.createElement('option');
           opt.appendChild(document.createTextNode('Please Select a State'));
           opt.value = '';
           sel.appendChild(opt);
           $.each(data, function(index) {
                 var sel = document.getElementById('statesList');
                 var opt = document.createElement('option');
                 opt.appendChild(document.createTextNode(data[index].name));
                 opt.setAttribute('state-id',data[index].id);
                 opt.value = data[index].name;
                 sel.appendChild(opt);

           });
        }
     })
   });

   $(".thankmsg").on('hidden.bs.modal', function() {
     window.location.href = '{{route("front-course-category-all")}}';
   });
</script>
@endsection
