@extends('dashboard')
@section('content')
<section class="inner-page-banner">
    <span class="banner-shape-1 bannershape-animte">
        <img src="{{asset('assets/images/shape-1.png')}}" alt="shape">
    </span>
    <span class="banner-shape-2 bannershape-animte">
        <img src="{{asset('assets/images/shape-2.png')}}" alt="shape">
    </span>
    <span class="banner-shape-3 bannershape-animte">
        <img src="{{asset('assets/images/shape-3.png')}}" alt="shape">
    </span>
    <span class="banner-shape-4 bannershape-animte">
        <img src="{{asset('assets/images/shape-4.png')}}" alt="shape">
    </span>
    <div class="container">
        <h1>Explore Courses @if(isset($category)) - {{$category->name}} @endif</h1>
    </div>
</section>
<!-- banner End -->
<!-- Breadcum Start -->
<div class="theme-breadcum-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="theme-breadcrum">
                    <ul>
                        <li><a href="{{route('home-page')}}">Home</a></li>
                        <li><i class="fal fa-chevron-right"></i></li>
                        <li><a href="{{route('front-course-category-all')}}">Courses</a></li>
                        @if(isset($category))
                        <li><i class="fal fa-chevron-right"></i></li>
                        <li>{{$category->name}}</li>
                        @endif
                    </ul>
                </div>
            </div> 
        </div>
    </div>
</div>
<!-- Breadcum End -->
<!-- Our Courses Section Start -->

<section class="course-detail">
    <div class="container">
        <div class="row mb-3 mb-lg-5 mb-md-5">
            <div class="col-12">
                <div class="courses-page-list-sec">
                    <div class="courses-page-list-sec-inner">
                        <a href="javascript:void(0);"><i class="fas fa-list-ul"></i><span>Showing 1 - {{count($courses)}} of {{count($courses)}}</span></a>
                    </div>
                    <div class="courses-page-list-sec-dropdown d-flex align-items-center">
                        <label class="me-2">Select Category : </label>
                        <select class="form-select" id="course-category">
                            <option value="">All</option>
                            @foreach($categories as $cat)
                            <option value="{{$cat->slug}}" @if($category!=null && $category->slug == $cat->slug) selected @endif>{{$cat->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($courses as $course)
            <div class="col-lg-4 col-md-6 col-12 course-item-outer">
                <div class="course-item">
                    <div class="course-img">
                        <a href="{{route('front-course-detail',$course->slug)}}" class="img">
                            <img src="{{asset('storage/courses/'.$course->image)}}" alt="{{$course->name}}">
                        </a>
                        <ul>
                            <!-- <li><a href="javascript:void(0);" class="course-tag-orange">Business</a></li> -->
                            <li><a href="javascript:void(0);" class="course-tag-blue">{{$course->coursecategory->name}}</a></li>
                        </ul>
                    </div>
                    <div class="course-content">
                        <a href="{{route('front-course-detail',$course->slug)}}">
                            <h3 class="h3-title text-truncate">{{$course->name}}</h3>
                        </a>
                        <div class="course-des">
                            <p>{!! substr(strip_tags($course->summary),0,125) !!}</p>
                        </div>
                        <div class="course-line"></div>
                        <div class="course-footer">
                            <div class="course-price">
                            Starting from <span>${!! number_format($course->price,2) !!}</span>/class
                            </div>
                            <!-- <div class="course-review">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <p>5.0 (2k)</p>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @if(count($courses) == 0)
            <div class="col-lg-4 mb-3">
                <div class="oc-sec-inner oc-2">
                    <div class="">
                        <img src="{{asset('assets/images/draw.jpg')}}" alt="Course">
                    </div>
                    <div class="oc-sec-inner-content">
                        <h3 class="mb-2"><a href="{{route('front-course-category-all')}}" class="d-block" tabindex="0">View All Courses </a></h3>
                        <p>Sorry! no courses for {{$category->name}} found. Our team is working to bring the variety of courses to you. Meanwhile, please click View all courses to got through the available courses.</p>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    $(document).on('change', '#course-category', function() {
        slug = $(this).val();
        window.location.href = '{{route("front-course-category-all")}}' + "/" + slug;
    })
</script>
@endsection
