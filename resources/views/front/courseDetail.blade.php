@extends('dashboard')
@section('content')
<style type="text/css">

</style>
<!-- banner Start -->
<section class="inner-page-banner">
    <span class="banner-shape-1 bannershape-animte">
        <img src="{{asset('assets/images/shape-1.png')}}" alt="shape">
    </span>
    <span class="banner-shape-2 bannershape-animte">
        <img src="{{asset('assets/images/shape-2.png')}}" alt="shape">
    </span>
    <span class="banner-shape-3 bannershape-animte">
        <img src="{{asset('assets/images/shape-3.png')}}" alt="shape">
    </span>
    <span class="banner-shape-4 bannershape-animte">
        <img src="{{asset('assets/images/shape-4.png')}}" alt="shape">
    </span>
    <div class="container">
        <h1>{{$course->name}}</h1>
    </div>
</section>
<!-- banner End -->
<!-- Breadcum Start -->
<div class="theme-breadcum-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="theme-breadcrum">
                    <ul>
                        <li><a href="{{route('home-page')}}">Home</a></li>
                        <li><i class="fal fa-chevron-right"></i></li>
                        <li><a href="{{route('front-course-category-all')}}">Courses</a></li>
                        <li><i class="fal fa-chevron-right"></i></li>
                        <li>{{$course->name}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcum End -->
<!-- Our Courses Section Start -->

<section class="course-detail">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-7">
                <div class="course-detail-inner">
                    <div class="course-col">
                        <div class="course-detail-title">
                            <h2> Course Infomation </h2>
                        </div>
                        <div class="course-col-text">
                            {!! $course->summary !!}
                        </div>
                    </div>

                    <div class="course-col">
                        <div class="course-detail-title">
                            <h2> What will I learn? </h2>
                        </div>
                        <div class="course-col-text">
                            {!! $course->learning !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-5">
                <div class="course-sidebar">
                <div class="cs-col">
                        <div class="enroll-box">
                            <div class="course-detail-title text-center">
                                <button class="theme-btn theme-btn2" type="button" onclick="document.location.href='{{route('course-schedule-demo',[$course->coursecategory->slug,'course'=>$course->slug])}}'">Book a Demo Class Now!</button>
                            </div>

                            <div class="enroll-body">
                            </div>
                        </div>
                    </div>

                    <div class="cs-col">
                        <div class="enroll-box">
                            <div class="course-detail-title">
                                <h2> Get The Course </h2>
                            </div>

                            <div class="enroll-body">
                                <div class="enroll-text">
                                    <p>Perfect for kids of age range {{$course->age_range}} </p>
                                </div>

                                <div class="enroll-detail">
                                    <div class="enroll-item">
                                        <span>
                                            <div class="get-course-icon">
                                                <img width="10" src="{{asset('assets/images/get-the-course-icon-1.svg')}}" alt="icon">
                                            </div>
                                            Price:
                                        </span>
                                        <span> ${{$course->price}}<small>/Per Class</small> </span>
                                    </div>
                                    <div class="enroll-item">
                                        <span>
                                            <div class="get-course-icon">
                                                <img width="17" src="{{asset('assets/images/get-the-course-icon-2.svg')}}" alt="icon">
                                            </div>
                                            Duration:
                                        </span>
                                        <span> {{$course->duration}} minutes </span>
                                    </div>
                                    <div class="enroll-item">
                                        <span>
                                            <div class="get-course-icon">
                                                <img width="22" src="{{asset('assets/images/get-the-course-icon-3.svg')}}" alt="icon">
                                            </div>
                                            Class Frequency :
                                        </span>
                                        @php
                                        $frequency = explode(',',$course->frequency);
                                        @endphp
                                        <span class="enroll-checkbox">
                                            @foreach($frequency as $key=>$class)
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="duration" id="duration-{{$class}}" value="{{$class}}" @if($key==0) checked @endif>
                                                <label class="form-check-label" for="duration-{{$class}}">
                                                    {{$class}}<small>/week</small>
                                                </label>
                                            </div>
                                            @endforeach
                                        </span>
                                    </div>
                                    <div class="enroll-item">
                                        <span>
                                            <div class="get-course-icon">
                                                <img width="10" src="{{asset('assets/images/get-the-course-icon-1.svg')}}" alt="icon">
                                            </div>
                                            Monthly Price :
                                        </span>
                                        <span id="priceSection" class="coursePricebox">${{$course->price*4*$frequency[0]}}<small>/Per Month</small> </span>
                                    </div>
                                    <span class="enroll-course-note"> Note: for 4 Classes of 60 minutes each, total 4 Hours per month</span>
                                </div>
                                <button type="button" data-bs-toggle="modal" data-bs-target="#getTheCourse-modal" class="theme-btn">Get this Course</button>
                            </div>
                        </div>
                    </div>
                    <div class="cs-col">
                        <div class="instructor-detail">
                            <div class="course-detail-title">
                                <h2> About The Instructor </h2>
                            </div>
                            <div class="insDet-head">
                                <div class="insDet-img">
                                    <img src="{{asset('storage/instructor/'.$course->instructorDetail->photo)}}" class="img-fluid" alt="">
                                </div>
                                <h5><a href="javascript:void(0);"> {{$course->instructorDetail->name}}</a> </h5>
                                <span> Member Since {!! date('M, Y',strtotime($course->instructorDetail->created_at)) !!} </span>
                            </div>
                            <div class="insDet-body">
                                <p>{{strip_tags ($course->instructorDetail->summary)}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Our Courses Section End -->

<!-- Online Courses Section Start -->
<section class="online-course-sec">
    <div class="container">
        <div class="row align-items-center mb-4">
            <div class="col-lg-8">
                <div class="sub-heading-section">
                    <h5 class="text-uppercase">OUR ONLINE COURSES</h5>
                    <h2 class="pb-0 mb-0">Find The Right Online Course For You</h2>
                </div>
            </div>
        </div>
        <div class="row course-slider-inners justify-content-center">
            @foreach($courses as $moreCourse)
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="course-item shadow-sm">
                        <div class="course-img">
                            <a href="{{route('front-course-detail',$moreCourse->slug)}}" class="img">
                                <img src="{{asset('storage/courses/'.$moreCourse->image)}}" alt="{{$moreCourse->name}}">
                            </a>
                            <ul>
                                <li><a href="javascript:void(0);" class="course-tag-blue">{{$moreCourse->coursecategory->name}}</a></li>
                            </ul>
                    </div>
                    <div class="course-content">
                        <a href="{{route('front-course-detail',$moreCourse->slug)}}">
                            <h3 class="h3-title text-truncate">{{$moreCourse->name}}</h3>
                        </a>
                        <div class="course-des">
                            <p>{!! substr(strip_tags($moreCourse->summary),0,100) !!}</p>
                        </div>
                            <div class="course-line"></div>
                            <div class="course-footer">
                                <div class="course-price">
                                    Starting from <span>${!! number_format($course->price,2) !!}</span>/class
                                </div>
                                <!-- <div class="course-review">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <p>5.0 (2k)</p>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="text-center d-block d-lg-none pt-5">
            <a href="{{route('front-course-category-all')}}" class="theme-btn">View All Course</a>
        </div>
    </div>
</section>
<!-- Online Courses Section End -->


<!-- Modal -->
<div class="modal fade getTheCourse-modal" tabindex="-1" id="getTheCourse-modal">
    <div class="modal-dialog  modal-lg modal-dialog-centered">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Get this Course</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <!-- Modal body -->
            <div class="modal-body question-card">
                <p>Please fill in basic information about you and student.</p>
                <form class="row bookDemo" id="get_this_course_form">
                    <input type="hidden" name="pricePerClass" value="{{$course->price}}" id="pricePerClass">
                    <input type="hidden" name="frequency" id="frequency" value="">
                    <input type="hidden" name="course_id" value="{{$course->id}}">

                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="mb-2">Parent's/Guardian's Name<i class="far fa-asterisk"></i></label>
                            <input type="text" name="parentName" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="mb-2">Student's Name<i class="far fa-asterisk"></i></label>
                            <input type="text" name="studentName" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="mb-2">Student's Age<i class="far fa-asterisk"></i></label>
                            <select name="age" class="form-control" required>
                                @for($i=4;$i<19;$i++)
                                    <option value="{{$i}} years">{{$i}} Years</option>
                                @endfor
                                <option value="18+">18+ years</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="mb-2">School Name</label>
                            <input class="form-control" name="school" value="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="mb-2">Contact Number<i class="far fa-asterisk"></i></label>
                            <input type="text" name="phone" class="form-control phone" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="mb-2">Email Id<i class="far fa-asterisk"></i></label>
                            <input type="email" name="email" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="mb-4">
                            <label class="mb-2"> Address </label>
                            <input class="form-control" type="text" name="address" />
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="mb-4">
                                    <label class="mb-2">Country<i class="far fa-asterisk"></i></label>
                                    <select class="form-select form-control" name="Country" aria-label="Default select example" id="countryList" required>
                                        @foreach($country_list as $key => $country)
                                            <option value="{{$country->name}}" country-id="{{$country->id}}" @if($country->id == 231) selected @endif>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-4">
                                    <label class="mb-2">State<i class="far fa-asterisk"></i></label>
                                    <select class="form-select form-control" name="state" id="stateList" aria-label="Default select example">
                                        <option value="">Select a country first</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-4">
                                    <label class="mb-2">City<i class="far fa-asterisk"></i></label>
                                    <select class="form-select form-control" name="city" id="cityList" aria-label="Default select example">
                                        <option value="">Select a state first</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12 text-center pt-4">
                                <button class="theme-btn" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $('input[name="duration"]').change(function() {
        classcount = $(this).val();
        pricePerClass = $('#pricePerClass').val();
        console.log("COURSE DURATION--> " + classcount)
        totalprice = classcount * pricePerClass * 4;
        classes = classcount * 4;
        $('#priceSection').html('$' + totalprice + '<small>/Per Month</small>');
        $(".enroll-course-note").html("Note: For " + classes + " Classes of 60 minutes each, total " + classes + " Hours per month");
    })

    $('#stateList').change(function() {
        $('#cityList').empty();
        stateid = this.value;
        var statename = $(this).find("option:selected").text();
        $('#selected-state').val(statename);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });

        $.ajax({
            url: "{{ route('front-getcities') }}",
            type: "POST",
            data: {
                stateid: stateid
            },
            success: function(data) {
                var sel = document.getElementById('cityList');
                var opt = document.createElement('option');
                opt.appendChild(document.createTextNode('Please Select a City'));
                opt.value = '';
                sel.appendChild(opt);
                $.each(data, function(index) {
                    var sel = document.getElementById('cityList');
                    var opt = document.createElement('option');
                    opt.appendChild(document.createTextNode(data[index].name));
                    sel.appendChild(opt);

                });
            }
        })
    });

    $('#getTheCourse-modal').on('shown.bs.modal', function() {
        $('#frequency').val($('input[name="duration"]:checked').val());
    })

    $('#get_this_course_form').submit(function(e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });

        $.ajax({
            url: "{{ route('user-create-subscription') }}",
            type: "POST",
            data: formData,
            success: function(data) {
                if (data.status == 'success') {
                    window.location.href = "{{route('user-create-payment','')}}" + "/" + data.subscription_id;
                }
                // $('#companySelection-modal-wrapper').modal('hide');
                // $('#company-modal-wrapper').modal('hide');

                // $('#companyList').append('<option value="' + data.id + '" selected>' + data.name + '</option>');
                // $('#companyList').val(data.id);
                // $('#companyList').trigger('change');
            },
            error: function(data) {

            },
            cache: false,
            contentType: false,
            processData: false
        });
    })
    $('#countryList').change(function() {
      $('#stateList').empty();
      // $('#modalCityList').empty();
      countryid = $(this).find('option:selected').attr('country-id');
      console.log("country-->" + countryid);
      var sel = document.getElementById('stateList');
      var opt = document.createElement('option');
      opt.appendChild(document.createTextNode('Loading...'));
      opt.value = '';
      sel.appendChild(opt);
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
         }
      });

      $.ajax({
         url: "{{ route('front-getstates') }}",
         type: "POST",
         data: {
            countryid: countryid
         },
         success: function(data) {
            $('#stateList').empty();
            var sel = document.getElementById('stateList');
            var opt = document.createElement('option');
            opt.appendChild(document.createTextNode('Please Select a State'));
            opt.value = '';
            sel.appendChild(opt);
            $.each(data, function(index) {
                  var sel = document.getElementById('stateList');
                  var opt = document.createElement('option');
                  opt.appendChild(document.createTextNode(data[index].name));
                  opt.setAttribute('state-id',data[index].id);
                  opt.value = data[index].name;
                  sel.appendChild(opt);

            });
         }
      })
   });
    $(document).ready(function(){
        $('#countryList').trigger('change');
    })
</script>
@endsection
