@extends('dashboard')
@section('content')
<!-- banner Start -->
<section class="inner-page-banner instructor-detail-banner" style="background-image:url({{URL::asset('assets/images/profile-banner.svg')}});">
   <div class="container">
      <div class="row align-items-center">
         <div class="col-lg-3 col-sm-4">
            <div class="instructor-banner-img">
               <div class="instructor-image-inner">
                  <img src="{{asset('assets/images/founder/'.$instructor->photo)}}" class="img-fluid" alt="instructor">
               </div>
            </div>
         </div>
         <div class="col-lg-8 col-sm-7">
            <div class="instructor-banner-caption">
               <h1> {{$instructor->name}} </h1>
               <span>
                  @foreach($instructor->categories as $key=>$cat){{$key == 0 ? $cat->category()->first()->name : ", ".$cat->category()->first()->name}}@endforeach
               </span>
               <div class="instructor-rating d-flex align-items-center">
                  <i class="fa fa-star" aria-hidden="true"></i>
                  <i class="fa fa-star" aria-hidden="true"></i>
                  <i class="fa fa-star" aria-hidden="true"></i>
                  <i class="fa fa-star" aria-hidden="true"></i>
                  <i class="fa fa-star" aria-hidden="true"></i>
                  <p>5.00(2k)</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section class="instructor-summary">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <div class="instructor-summary-inner">
               <div class="title mb-4">
                  <h3>Summary</h3>
               </div>
               {!! $instructor->summary !!}
            </div>
         </div>
      </div>
   </div>
</section>
@if(count($instructor->gallery) > 0)
   <section class="gallery-section">
      <div class="container">
         <div class="sub-heading-section">
            <h5 class="text-uppercase">gallery</h5>
            <h2 class="pb-0 mb-0"> View Sample Work by {{$instructor->name}} </h2>
         </div>
         <div class="row">
            @foreach($instructor->gallery as $gallery)
               <div class="col-lg-4">
                  <div class="gallery-section-inner">
                     <img src="{{asset('assets/images/'.$gallery->image)}}" alt="Sketching">
                     <div class="content">
                        <h3><a href="javascript:void(0);">Sketching by Navita</a></h3>
                        <p>SkillsnMore offers live interactive online arts and craft courses for kids. Taught by art experts</p>
                     </div>
                  </div>
               </div>
            @endforeach
         </div>
      </div>
   </section>
@endif
<section class="online-course-sec" id="online-course-pf">
   <div class="container">
      <div class="row align-items-center mb-4">
         <div class="col-lg-12">
            <div class="sub-heading-section">
               <h5 class="text-uppercase">EXPLORE COURSES</h5>
               <h2 class="pb-0 mb-0">Courses by {{$instructor->name}}</h2>
            </div>
         </div>
      </div>
      <div class="pf-course row">
         @foreach($courses as $course)
            <div class="col-lg-4 col-md-6 col-12">
               <div class="course-item">
                  <div class="course-img">
                     <a href="javascript:void(0);" class="img">
                        <img src="{{asset('assets/images/'.$course->image)}}" alt="Vedic Maths with Ruchi">
                     </a>
                     <ul>
                        <li><a href="javascript:void(0);" class="course-tag-blue">{{$course->coursecategory->name}}</a></li>
                     </ul>
                  </div>
                  <div class="course-content">
                     <a href="javascript:void(0);">
                        <h3 class="h3-title text-truncate">{{$course->name}}</h3>
                     </a>
                     <div class="course-des">
                        <p>{!! substr(strip_tags($course->summary),0,125) !!}</p>
                     </div>
                     <div class="course-line"></div>
                     <div class="course-footer">
                        <div class="course-price">
                           starting from <span>${!! number_format($course->price,2) !!}</span>/class
                        </div>
                        <!-- <div class="course-review">
                           <i class="fa fa-star" aria-hidden="true"></i>
                           <p>5.0 (2k)</p>
                        </div> -->
                     </div>
                  </div>
               </div>
            </div>
         @endforeach
      </div>
      <div class="text-center d-block d-lg-none pt-5">
         <a href="javascript:void(0);" class="theme-btn">View All Course</a>
      </div>
   </div>
</section>

@endsection
@section('scripts')
@endsection
