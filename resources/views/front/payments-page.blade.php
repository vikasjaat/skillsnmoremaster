<!Doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <title>Live Online Coding, Art, Music & Sketching Classes for Kids</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="meta description: SkillsnMore is an online learning platform for kids of 5-16 yrs age groups. We offer online coding classes, online programming courses, music classes for children and drawing classes.">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontawsome.min.css')}}" />
      <link rel="icon" type="image/gif" href="{{asset('assets/images/favicon.png')}}" sizes="16x16">
      <link rel="stylesheet" href="{{asset('assets/css/aos.css')}}" />
      <!-- slick slider css -->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/slick.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/slick-theme.css')}}">
      <!-- My CSS -->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/common.css')}}" />
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/header.css')}}" />
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/footer.css')}}" />
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}" />
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}" />
   </head>
   <body>
      <!-- header-start -->
      <header class="t-header">
         <div class="container">
            <a class="payment-logo d-block m-auto mt-4" href="{{route('home-page')}}"><img src="{{asset('assets/images/logo.svg')}}"></a>
         </div>
      </header>
      <!-- header-end -->
    
      <section class="payment-details">
         <div class="container">
            <div class="row">
               <div class="col-lg-6">
                  <div class="cs-col">
                     <div class="enroll-box">
                        <h5> Course - {{$course->name}} </h5>
                        <p> {{$course->age_range}} </p>
                        <div class="enroll-body pt-2">
                           <div class="enroll-detail">
                              <div class="enroll-item">
                                 <span>
                                    <div class="get-course-icon">
                                       <img width="10" src="{{asset('assets/images/get-the-course-icon-1.svg')}}" alt="icon">
                                    </div>
                                    Price (Each class):
                                 </span>
                                 <span> ${!! number_format($course->price,2) !!}<small>/Per Class</small> </span>
                              </div>
                              <div class="enroll-item">
                                 <span>
                                    <div class="get-course-icon">
                                       <img width="17" src="{{asset('assets/images/get-the-course-icon-2.svg')}}" alt="icon">
                                    </div>
                                    Duration :
                                 </span>
                                 <span> {{$course->duration}} minutes </span>
                              </div>
                              <div class="enroll-item">
                                 <span>
                                    <div class="get-course-icon">
                                       <img width="22" src="{{asset('assets/images/get-the-course-icon-3.svg')}}" alt="icon">
                                    </div>
                                    Class Frequency :
                                 </span>
                                 <span> {{$userSubscription->frequency}}/week </span>
                              </div>
                              <div class="enroll-item">
                                 <span>
                                    <div class="get-course-icon">
                                       <img width="10" src="{{asset('assets/images/get-the-course-icon-1.svg')}}" alt="icon">
                                    </div>
                                    Monthly Price (Payable Price) :
                                 </span>
                                 <span id="priceSection" class="coursePricebox"> ${!! number_format($course->price * $userSubscription->frequency * 4,2) !!}<small>/Per Month</small> </span>
                              </div>
                              <span class="enroll-course-note"> Note: for {{$userSubscription->frequency * 4}} Classes of {{$course->duration}} minutes each, total {{$userSubscription->frequency * 4}} Hours per month</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="cs-col">
                     <h5>Payment details</h5>
                     <p>Complete your purchase by providing your purchase details</p>
                     <form class="row pt-2" id="paymentForm">
                        <input type="hidden" name="susbcriptionId" value="{{$userSubscription->subscription_id}}">
                        <input type="hidden" name="paymentMethod" value="">
                        <div class="col-md-12">
                           <div class="mb-4 card-credit">
                              <!-- <span><i class="fas fa-credit-card-front"></i></span> -->
                              <label class="mb-2">Card Number<i class="far fa-asterisk"></i></label>
                              <div id="card-element" class="mt-2"></div>
                              <!-- <input type="text" name="" class="form-control" placeholder="Card number" autocomplete="false"> -->
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="mb-4">
                              <label class="mb-2">Card Holder Name<i class="far fa-asterisk"></i></label>
                              <input type="text" name="name" id="card-holder-name" class="form-control" required>
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="mb-4">
                              <label class="mb-2">Billing Address<i class="far fa-asterisk"></i></label>
                              <input type="text" name="address" class="form-control" placeholder="Address" autocomplete="false" required>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="mb-4">
                              <label class="mb-2">Zip<i class="far fa-asterisk"></i></label>
                              <input type="text" name="pincode" class="form-control zipcode" autocomplete="false" required>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="mb-4">
                              <label class="mb-2">State<i class="far fa-asterisk"></i></label>
                              <select name="state" class="form-control" required>
                                 <option disabled selected value="">Select a state</option>
                                 @foreach($states as $state)
                                 <option>{{$state->name}}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                        <!-- <div class="d-flex justify-content-between">
                           <p>Subtotal</p>
                           <p>$29.00</p>
                           </div>
                           <div class="d-flex justify-content-between">
                           <p>Vat(20%)</p>
                           <p>$5.80</p>
                           </div>
                           <div class="d-flex justify-content-between">
                           <p class="fw-bold">Total</p>
                           <p class="fw-bold">$34.80</p>
                           </div> -->
                        <button type="button" class="btn-block btn btn-primary mt-3" id="card-button">Pay ${!! number_format($course->price * $userSubscription->frequency * 4,2) !!}</button>
                        <small class="text-center mt-3"><span><i class="far fa-lock-alt me-2"></i></span>payments are secure and encrypted</small>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      @include('layouts.footer')
      <script src="https://js.stripe.com/v3/"></script>
      <script>
         $('.zipcode').mask('00000')
         
         const stripe = Stripe('{{$stripe_key}}');
         const elements = stripe.elements();
         const cardElement = elements.create('card', {
             style: {
                 base: {
                     iconColor: '#7e3d97',
                     color: '#212529',
                     lineHeight: "52px",
                     fontWeight: 400,
                     fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
                     fontSize: '16px',
                     fontSmoothing: 'antialiased',
         
                     ':-webkit-autofill': {
                         color: '#212529',
                     },
                     '::placeholder': {
                         color: '#212529',
                     },
                 },
                 invalid: {
                     iconColor: '#eb1c26',
                     color: '#eb1c26',
                 },
             },
         });
         
         cardElement.mount('#card-element');
      </script>
      <script>
         $('input').keyup(function(){
             $(this).removeClass('is-invalid');
             $(this).parent().find('.text-danger').remove();
         })
         
         $(document).on('change','select',function(){
             $(this).removeClass('is-invalid');
             $(this).parent().find('.text-danger').remove();
         })
         
         const cardHolderName = document.getElementById('card-holder-name');
         const cardButton = document.getElementById('card-button');
         
         cardButton.addEventListener('click', async (e) => {
         
             $('#paymentForm').find('input,select').each(function(){
                 if($(this).attr('required') == 'required' && ($(this).val() == "" || $(this).val()==null)){
                     $(this).addClass('is-invalid');
                     $(this).after('<span class="text-danger">This information is required to proceed</span>')
                 }
             })
         
             $('#card-button').attr('disabled',true);
             const {
                 paymentMethod,
                 error
             } = await stripe.createPaymentMethod(
                 'card', cardElement, {
                     billing_details: {
                         name: cardHolderName.value
                     }
                 }
             );
             if (error) {
         
                 swal('Error!', 'Card number is not valid, please try again!', 'error');
                 $('#card-button').attr('disabled',false);
             } else {
         
                 $('input[name="paymentMethod"]').val(paymentMethod.id);
         
                 var formData = new FormData($('#paymentForm')[0]);
         
                 $.ajaxSetup({
                     headers: {
                         'X-CSRF-TOKEN': "{{ csrf_token() }}"
                     }
                 });
                 $.ajax({
                     url: "{{route('store-user-payment-method')}}",
                     type: "POST",
                     data: formData,
                     success: function(data) {
                         if(data.status == 'success')
                         {
                             window.location.href = '{{route("payment-thank-you","")}}'+'/'+data.subscriptionId;
                         }
                         else if(data.status == 'error')
                         {
                             swal('Error!', 'Something went wrong, please try again.', 'error')
                             .then((value) => {
                                 location.reload();
                             });
                         }
                     },
                     error: function(data) {
         
                     },
                     cache: false,
                     contentType: false,
                     processData: false
                 })
             }
         });
      </script>