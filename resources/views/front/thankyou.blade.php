@extends('dashboard')
@section('content')

    <style type="text/css">

    </style>
    <!-- banner Start -->
    <section class="inner-page-banner">
        <span class="banner-shape-1 bannershape-animte">
            <img src="{{asset('assets/images/shape-1.png')}}" alt="shape">
        </span>
        <span class="banner-shape-2 bannershape-animte">
            <img src="{{asset('assets/images/shape-2.png')}}" alt="shape">
        </span>
        <span class="banner-shape-3 bannershape-animte">
            <img src="{{asset('assets/images/shape-3.png')}}" alt="shape">
        </span>
        <span class="banner-shape-4 bannershape-animte">
            <img src="{{asset('assets/images/shape-4.png')}}" alt="shape">
        </span>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="thankYou-img mb-3">
                        <img src="{{asset('assets/images/thankyou.svg')}}" width="200" alt="Thank You" />
                    </div>
                    <h1 class="fs-1"> Thank You, enjoy!</h1>

                    <div class="thanks-content mt-2">
                        <p class="text-dark">Congratulations! you have subscribed the course successfully. Please note the transaction id and subscription id for future reference and for any additional details, please feel free to reach out to us at contact@skillsnmore.com</p>
                        <p class="text-dark">Transaction ID: <strong> {{$sub->stripe_subscription}} </strong> </p>
                        <p class="text-dark">Subscription ID: <strong> {{$sub->subscription_id}} </strong></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner End -->

@endsection
@section('script')

@endsection