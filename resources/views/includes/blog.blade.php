<!-- blog-section-start -->
<section class="blg-section">
   <div class="container">
      <div class="sub-heading-section">
         <h5 class="text-center text-uppercase">Our Blog</h5>
         <h2 class="text-center">Latest Blog &amp; News</h2>
      </div>
      <div class="row justify-content-center blg-slider-section">
         <div class="col-lg-4 col-md-6">
            <div class="blg-inner">
               <div class="blg-img">
                  <img src="{{asset('assets/images/blog1.jpg')}}" alt="Blog">
               </div>
               <div class="blg-content">
                  <h3 class="mb-0"><a href="javascript:void(0);" class="d-block">Proin feugiat tortor non neque eleifend.</a></h3>
                  <div class="blg-comment">
                     <p class="mb-0"><i class="far fa-calendar-alt"></i><a href="javascript:void(0);">07 Jan, 2022</a></p>
                     <p class="mb-0"><i class="far fa-comment-dots"></i><a href="javascript:void(0);">3 Comments</a></p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-6">
            <div class="blg-inner">
               <div class="blg-img">
                  <img src="{{asset('assets/images/blog2.jpg')}}" alt="Blog">
               </div>
               <div class="blg-content">
                  <h3 class="mb-0"><a href="javascript:void(0);" class="d-block">Proin feugiat tortor non neque eleifend.</a></h3>
                  <div class="blg-comment">
                     <p class="mb-0"><i class="far fa-calendar-alt"></i><a href="javascript:void(0);">07 Jan, 2022</a></p>
                     <p class="mb-0"><i class="far fa-comment-dots"></i><a href="javascript:void(0);">3 Comments</a></p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-6">
            <div class="blg-inner">
               <div class="blg-img">
                  <img src="{{asset('assets/images/blog3.jpg')}}" alt="Blog">
               </div>
               <div class="blg-content">
                  <h3 class="mb-0"><a href="javascript:void(0);" class="d-block">Proin feugiat tortor non neque eleifend.</a></h3>
                  <div class="blg-comment">
                     <p class="mb-0"><i class="far fa-calendar-alt"></i><a href="javascript:void(0);">07 Jan, 2022</a></p>
                     <p class="mb-0"><i class="far fa-comment-dots"></i><a href="javascript:void(0);">3 Comments</a></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- blog-section-end -->