<section class="category-section-new" id="couse-exp">
    <div class="container">
        <div class="sub-heading-section">
            <h5 class="text-center text-uppercase">Course Categories</h5>
            <h2 class="text-center ">Explore Our Courses</h2>
        </div>
        <ul class="row justify-content-center">
            <li class="col-xl-3 col-lg-4 col-md-6 col-6">
                <div class="category-inner">
                    <div class="category-inner-img">
                        <a href="{{route('front-course-category-all')}}"><img src="{{asset('assets/images/course-icon.svg')}}" alt="category"></a>
                    </div>
                    <div class="content">
                        <h3 class="mb-0"><a href="{{route('front-course-category-all')}}">All Courses</a></h3>
                        <p class="mb-0"><span class="age-text">Age 5 to 16</span></p>
                        <a href="{{route('front-course-category-all')}}" class="theme-btn">Explore<i class="fal fa-chevron-right"></i></a>
                    </div>
                </div>
            </li>
            @foreach($include_categories as $category)
            <li class="col-xl-3 col-lg-4 col-md-6 col-6">
                <div class="category-inner">
                    <div class="category-inner-img">
                        <a href="{{route('front-course-category',$category->slug)}}"><img src="{{asset('storage/category/'.$category->icon)}}" alt="category"></a>
                    </div>
                    <div class="content">
                        <h3 class="mb-0"><a href="{{route('front-course-category',$category->slug)}}">{{$category->name}}</a></h3>
                        <p class="mb-0"><span class="age-text">Age 5 to 16</span></p>
                        <a href="{{route('front-course-category',$category->slug)}}" class="theme-btn">Explore<i class="fal fa-chevron-right"></i></a>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
    </div>
    <div class="bg-shapess">
        <img src="{{asset('assets/images/bg-icons/sky-arrow.svg')}}" alt="category" class="sky-arrow">
        <img src="{{asset('assets/images/bg-icons/math.svg')}}" class="math">
        <img src="{{asset('assets/images/bg-icons/star-1.svg')}}" class="star1">
        <img src="{{asset('assets/images/bg-icons/star2.svg')}}" class="star2">
        <img src="{{asset('assets/images/bg-icons/trign-pink.svg')}}" class="tringle-pink">
        <img src="{{asset('assets/images/bg-icons/abcd.svg')}}" class="abcd-shape">

    </div>
</section>