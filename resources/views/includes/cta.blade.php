<!-- cta-section start-->
<section class="cta">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 text-center">
                <div class="cta-card">
                    <h2 class="text-white mb-0">Book Your Free Demo</h2>
                    <p class="text-white mt-3" mt-3>Tell us a bit about your child and we will come up with a customized learning journey for them.</p>
                    {{-- <button class="theme-btn" type="button" data-bs-toggle="modal" data-bs-target="#course-modal">Book Your Free Demo</button> --}}
                    <button class="theme-btn" type="button" onclick="document.location.href='{{route('front-course-schedule-demo')}}'">Book Your Free Demo</button>
                </div>

            </div>
            <div class="col-lg-6 text-center mt-4 mt-lg-0">
                <div class="cta-card">
                    <h2 class="text-white mb-0">Partner with Us</h2>
                    <p class="text-white mt-3">Collaborate with us if you are passionate about teaching and would love to shape up young minds.</p>
                    <button type="button" class="theme-btn2 theme-btn partner-pop" data-bs-toggle="modal" data-bs-target="#partner-modal">Partner with Us</a>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- cta-section end-->
