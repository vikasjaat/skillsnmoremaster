<section class="expert-section">
   <div class="container">
      <div class="sub-heading-section">
         <h5 class="text-uppercase text-center">BEST INSTRUCTORS</h5>
         <h2 class="text-center">Meet Our Qualified Expert Instructors</h2>
      </div>
      <div class="experts-slider">
         @foreach($instructors as $instructor)
         <div>
            <div class="experts-inner">
               <div class="img-sec">
                  <img src="{{asset('storage/instructor/'.$instructor->photo)}}" alt="Expert">
                  
                  <!-- <ul>
                     <li><a href="javascript:void(0);"><i class="fab fa-facebook"></i></a></li>
                     <li><a href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                     <li><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                  </ul> -->
               </div>
               <h3><a href="javascript:void(0);" class="d-block">{{$instructor->name}}</a></h3>  
               <h6>{{$instructor->categories()->first()->category()->first()->name}}</h6>
               <p class="read_more_text">{{strip_tags($instructor->summary)}}</p>
            </div>
         </div>
         @endforeach
         
      </div>
   </div>
</section>
