<!-- Price Section Start -->
<section class="price-section">
   <div class="container">
      <div class="sub-heading-section">
         <h5 class="text-center text-uppercase">PRICING TABLE</h5>
         <h2 class="text-center">All Inclusive Pricing</h2>
      </div>
      <div class="row justify-content-center price-slider-section">
         <div class="col-lg-4 col-md-6 price-sec-inner">
            <div class="price-inner-details">
               <h4>Sliver Plan</h4>
               <p>Perfect for small marketing teams</p>
               <h2>$59<span>/Month</span></h2>
               <ul>
                  <li>Course Discussions</li>
                  <li>Content Librar</li>
                  <li>1-hour Mentorship</li>
                  <li>Online Course</li>
                  <li>Support 24x7</li>
               </ul>
               <a href="javascript:void(0);" class="theme-btn">Choose Plan</a>
            </div>
         </div>
         <div class="col-lg-4 col-md-6 price-sec-inner">
            <div class="price-inner-details">
               <h4>Gold Plan</h4>
               <span class="plr-text text-white text-uppercase">Popular</span>
               <p>Perfect for small marketing teams</p>
               <h2>$69<span>/Month</span></h2>
               <ul>
                  <li>Course Discussions</li>
                  <li>Content Librar</li>
                  <li>1-hour Mentorship</li>
                  <li>Online Course</li>
                  <li>Support 24x7</li>
               </ul>
               <a href="javascript:void(0);" class="theme-btn">Choose Plan</a>
            </div>
         </div>
         <div class="col-lg-4 col-md-6 price-sec-inner">
            <div class="price-inner-details">
               <h4>Diamond Plan</h4>
               <p>Perfect for small marketing teams</p>
               <h2>$79<span>/Month</span></h2>
               <ul>
                  <li>Course Discussions</li>
                  <li>Content Librar</li>
                  <li>1-hour Mentorship</li>
                  <li>Online Course</li>
                  <li>Support 24x7</li>
               </ul>
               <a href="javascript:void(0);" class="theme-btn">Choose Plan</a>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Price Section End -->