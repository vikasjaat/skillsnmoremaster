<!-- testimonial-section-start -->
<section class="testmonila-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-6">
            <div class="sub-heading-section">
               <h5 class="text-uppercase">Testimonial</h5>
               <h2 class="">What do our clients say about us</h2>
               <p>Learn and grow with SkillsnMore. You can finally feel good about your kid’s screen time. Our courses help deliver playful learning across various subjects helping kids build skills they need through lessons and activities they enjoy.</p>
            </div>
         </div>
         <div class="col-lg-6">
            <div class="testimonial-slider">

               <div>
                  <div class="testimonials-details">
                     <p>"I love the teachers at SkillsnMore. They all make coding easier for me to understand and also have fun while learning. With the colourful animation and easy explanations, coding has become a lot more fun for me".</p>
                     <div class="testimonials-content-img">
                        <div class="testimonial-img">
                           <img src="{{asset('assets/images/testimonials/reaha-basra.jpg')}}" alt="Reaha basra">
                        </div>
                        <div class="testimonial-content">
                           <h4>Rhea Basra</h4>
                           <p>Student</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div>
                  <div class="testimonials-details">
                     <p>"I learnt to paint with SkillsnMore. My teacher helped me a lot to explain colours and different techniques. I learn to focus more on using my brush with precision".</p>
                     <div class="testimonials-content-img">
                        <div class="testimonial-img">
                           <img src="{{asset('assets/images/testimonials/sama-kaushal.jpg')}}" alt="Sama kaushal">
                        </div>
                        <div class="testimonial-content">
                           <h4>Sama Kaushal</h4>
                           <p>Student</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div>
                  <div class="testimonials-details">
                     <p>"The concepts are well presented and greatly explained and the content of the course curated is perfect. This course is best for students like me who are scared of calculations."</p>
                     <div class="testimonials-content-img">
                        <div class="testimonial-img">
                           <img src="{{asset('assets/images/testimonials/gia-basra.jpg')}}" alt="Gia Basra">
                        </div>
                        <div class="testimonial-content">
                           <h4>Gia  Basra</h4>
                           <p>Student</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div>
                  <div class="testimonials-details">
                     <p>"I am learning music from Arjun sir for a while now and he is one of the best teachers. He has immense knowledge and experience and I love the way he teaches music and I enjoy it a lot."</p>
                     <div class="testimonials-content-img">
                        <div class="testimonial-img">
                           <img src="{{asset('assets/images/testimonials/sidhi-kausal.jpg')}}" alt="Sidhi kausal">
                        </div>
                        <div class="testimonial-content">
                           <h4>Siddhi Kaushal</h4>
                           <p>Student</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- testimonial-section-end -->