@extends('layouts.instructor.master')
@section('title', 'Add-Course')
@push('css')
    <style>
        .input-switch {
            display: none;
        }

        .label-switch {
            display: inline-block;
            position: relative;
        }

        .label-switch::before,
        .label-switch::after {
            content: "";
            display: inline-block;
            cursor: pointer;
            transition: all 0.5s;
        }

        .label-switch::before {
            width: 3em;
            height: 1em;
            border: 1px solid #757575;
            border-radius: 4em;
            background: #888888;
        }

        .label-switch::after {
            position: absolute;
            left: 0;
            top: -20%;
            width: 1.5em;
            height: 1.5em;
            border: 1px solid #757575;
            border-radius: 4em;
            background: #ffffff;
        }

        .input-switch:checked~.label-switch::before {
            background: #00a900;
            border-color: #008e00;
        }

        .input-switch:checked~.label-switch::after {
            left: unset;
            right: 0;
            background: #00ce00;
            border-color: #009a00;
        }

        .info-text {
            display: inline-block;
        }

        .info-text::before {
            content: "Not active";
        }

        .input-switch:checked~.info-text::before {
            content: "Active";
        }

    </style>
    <script>
        tinymce.init({
            selector: '#mytextarea'
        });
    </script>
@endpush
@section('content')
    <div class="container-fluid card-header">
        <div class="row" class="card-header">
            <div class="col-xl-11 mx-auto">
                <h6 class="mb-0 text-uppercase">{{ $title }}</h6>
                <hr />
                <div class="card border-top border-0 border-4 border-primary">
                    <div class="card-body p-5">
                        <div class="card-title d-flex align-items-center">
                            <div><i class="bx bxs-user me-1 font-22 text-primary"></i>
                            </div>
                            <h5 class="mb-0 text-primary">
                                @if (isset($data))Update Course
                                @else
                                    Course Registration @endif
                            </h5>
                        </div>
                        <hr>

                        <form class="row g-3" action="{{ $url }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="slug" id="inputslug"
                                value="@if (isset($data)) {{ $data->slug = substr($data->slug, 0, strpos($data->slug, '-')) }} @endif{{ old('slug') }}">
                            <input type="hidden" name="id" id="id"
                                value="@if (isset($data)) {{ $data->id }} @endif">
                            <div class="col-md-6">
                                <label for="inputcname" class="form-label">Course Name</label>
                                <input type="text" name="cname" class="form-control" id="inputcname"
                                    placeholder="Course Name..."
                                    value="@if (isset($data)) {{ $data->name = substr($data->name, 0, strpos($data->name, 'By')) }} @endif{{ old('cname') }}">
                                <span class="text-danger">
                                    @error('cname')
                                        {{ $message }}
                                    @enderror
                                </span>
                            </div>
                            <div class="col-md-6">
                                <label for="inputLastName" class="form-label">Select Categories</label>
                                <select class="form-select mb-3" name="categories" aria-label="Default select example">
                                    <option value="">Select Categories</option>
                                    @foreach ($cat as $item)
                                        <option value="{{ $item->id }}"
                                            @if (isset($data)) @if ($item->id == $data->category) selected @endif
                                            @endif>
                                            {{ $item->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <span class="text-danger">
                                    @error('categories')
                                        {{ $message }}
                                    @enderror
                                </span>
                            </div>
                            <div class="col-md-6">
                                <label for="inputPrice" class="form-label">Price</label>
                                <input type="number" name="price" class="form-control" id="inputPrice"
                                    placeholder="Price..."
                                    value="@if (isset($data)) {{ $data->price }} @endif{{ old('price') }}">
                                <span class="text-danger">
                                    @error('price')
                                        {{ $message }}
                                    @enderror
                                </span>
                            </div>
                            <div class="col-md-6">
                                <label for="inputfrequency" class="form-label">Frequency(week)</label>
                                <input type="number" name="frequency" class="form-control" id="inputfrequency"
                                    placeholder="Frequency..."
                                    value="@if (isset($data)) {{ $data->frequency }} @endif{{ old('frequency') }}">
                                <span class="text-danger">
                                    @error('frequency')
                                        {{ $message }}
                                    @enderror
                                </span>
                            </div>
                            <div class="col-md-6">
                                <label for="inputSummary" class="form-label">Age Range</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" name="arangeto" placeholder="Age Range..."
                                        value="@if (isset($data)) {{ explode(',', $data->age_range)[0] }} @endif{{ old('arangeto') }}">
                                    <span class="input-group-text">To</span>
                                    <input type="number" class="form-control" name="arange" placeholder="Age Range..."
                                        value="@if (isset($data)) {{ explode(',', $data->age_range)[1] }} @endif{{ old('arange') }}">
                                    <span class="text-danger">
                                        @error('arangeto')
                                            {{ $message }}
                                        @enderror
                                        @error('arange')
                                            {{ $message }}
                                        @enderror
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="inputduraction" class="form-label">Duration(minutes)</label>
                                <input type="number" class="form-control" name="duration" placeholder="Duration Minute..."
                                    id="inputduraction"
                                    value="@if (isset($data)) {{ $data->price }} @endif{{ old('duration') }}">
                                <span class="text-danger">
                                    @error('duration')
                                        {{ $message }}
                                    @enderror
                                </span>
                            </div>
                            <div class="col-12">
                                <label for="inputSummary" class="form-label">Summary</label>
                                <textarea id="editor" name="editor">
                                            @if (isset($data)){{ $data->learning }}@endif{{ old('editor') }}
                                        </textarea>
                                <span class="text-danger">
                                    @error('editor')
                                        {{ $message }}
                                    @enderror
                                </span>
                            </div>
                            <div class="col-12">
                                <label for="inputSummary" class="form-label">Learning</label>
                                <textarea class="form-control" id="inputAddress" name="learning" placeholder="Learning..." rows="3">
@if (isset($data)){{ $data->learning }}@endif{{ old('learning') }}
</textarea>
                                <span class="text-danger">
                                    @error('learning')
                                        {{ $message }}
                                    @enderror
                                </span>

                            </div>
                            <div class="col-md-12">
                                <div class="form-check form-switch">
                                    <div class="form-group">
                                        <input class='input-switch' type="checkbox" id="demo" checked name="status"
                                            value="@if (isset($data)) {{ $data->price }} @endif 1" />
                                        <label class="label-switch" for="demo"></label>
                                        <span class="info-text"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label" for="customFile">Course Select Image</label>
                                <input type="file" name="image" id="customFile" type="file"
                                    accept=".jpg, .png, image/jpeg, image/png" value="">
                                <span class="text-danger">
                                    @error('image')
                                        {{ $message }}
                                    @enderror
                                </span>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary px-5">
                                    @if (isset($data))Update Course
                                    @else
                                        Register Course @endif
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script src="{{ asset('adminassets/js/form-text-editor.js') }}"></script>
        <script src="{{ asset('adminassets/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('adminassets/js/cdntiny.min.js') }}"></script>

        {{-- // ---------------slug-Function--------------------------- --}}
        <script>
            $(document).on('keyup', '#inputcname', function() {
                var name = $(this).val();
                var slug = name.toLowerCase().trim().replace(/ /g, '-');
                $("#inputslug").val(slug);
            });
        </script>
        <script src="{{ asset('instassets/js/ckeditor/ckeditor.js') }}"></script>
        <script src="{{ asset('instassets/js/ckeditor/ckeditor-inner.js') }}"></script>
        <script>
            initSample();
        </script>
    @endpush
@endsection
