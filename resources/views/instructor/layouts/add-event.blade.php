@extends('layouts.instructor.master')
@section('title', 'View-Course')
@push('css')
    <link rel="stylesheet" href="{{ asset('instassets/plugins/datatables/datatables.min.css') }}">
@endpush
@push('script')
@endpush
@section('content')
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">Course Schedule</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="">Course Schedule</a></li>
                        <li class="breadcrumb-item active">Schedule Details</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="">
            <div class="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="about-info">
                            <div class="media mt-3 d-flex">
                                <img src="{{ asset('storage/courses/' . $course->image) }}" alt="Course Image">
                                <div class="media-body flex-grow-1">
                                    <ul>
                                        <li>
                                            <span class="title-span">Course : </span>
                                            <span class="info-span">{{ course($course->name) }}</span>
                                        </li>
                                        <li>
                                            <span class="title-span">Duration : </span>
                                            <span class="info-span">{{ $course->duration }}</span>
                                        </li>
                                        <li>
                                            <span class="title-span">Frequency : </span>
                                            <span class="info-span">{{ $course->frequency }} @if ($course->class_frequency == 'W')
                                                    (Week)
                                                @elseif($course->class_frequency == 'M')
                                                    (Month)
                                                @endif
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>                   
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="">
                                <div class="">
                                    <table class="datatable table table-stripped">
                                        <thead>
                                            <div class="table-responsive">
                                                <tr class="text-center">
                                                    <th>Batch Name</th>
                                                    <th>Start Date</th>
                                                    <th>Total Student</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                    <th>Send Info</th>
                                                    <th>Duration(Per Week) </th>
                                                </tr>
                                        </thead>
                                        <tbody id="myTable">
                                            @foreach ($schedule as $key => $item)
                                                @if (isset($schedule[$key - 1]) && $schedule[$key]->batch_name != $schedule[$key - 1]->batch_name)
                                                    <tr class="text-center">
                                                        <td>{{ batchname($item->batch_name) }}</td>
                                                        <td>{{ date('d-M-y', strtotime($item->on_date)) }}</td>
                                                        <td>260</td>
                                                        <td>
                                                            @if ($item->status == '1')
                                                                <span class="badge badge-success"> Active</span>
                                                            @else
                                                                <span class="badge badge-danger"> Inactive</span>
                                                            @endif
                                                        </td>
                                                        <td> <a href="{{ route('instructor.addstudent', [$item->batch_name]) }}"
                                                                class="btn btn-sm bg-success"><i
                                                                    class="far fa-plus"></i></a>
                                                            <a href="{{ route('instructor.showclass', [$item->batch_name]) }}"
                                                                class="btn btn-sm bg-warning"><i
                                                                    class="far fa-eye"></i></a>

                                                        </td>
                                                        <td><a href="{{ route('instructor.showclass', [$item->batch_name]) }}"
                                                                class="btn btn-sm bg-success">whatsapp  </a></td>
                                                        <td>
                                                            <span class="info-span">
                                                                @if ($item->class_frequency == 'M')
                                                                    Month
                                                                @elseif($item->class_frequency == 'D')
                                                                    Every Day
                                                                @elseif($item->class_frequency == 'W')
                                                                    Week
                                                                @endif
                                                            </span>
                                                        </td>
                                                    </tr>
                                                @elseif($key == 0)
                                                    <tr class="text-center">
                                                        <td>{{ batchname($item->batch_name) }}</td>
                                                        <td>{{ date('d-M-y', strtotime($item->on_date)) }}</td>
                                                        <td>260</td>
                                                        <td>
                                                            @if ($item->status == '1')
                                                                <span class="badge badge-success"> Active</span>
                                                            @else
                                                                <span class="badge badge-danger"> Inactive</span>
                                                            @endif
                                                        </td>
                                                        <td> <a href="{{ route('instructor.addstudent', [$item->batch_name]) }}"
                                                                class="btn btn-sm bg-success"><i
                                                                    class="far fa-plus"></i></a>
                                                            <a href="{{ route('instructor.showclass', [$item->batch_name]) }}"
                                                                class="btn btn-sm bg-warning"><i
                                                                    class="far fa-eye"></i></a>

                                                        </td>
                                                        
                                                        <td><a href="{{ route('instructor.showclass', [$item->batch_name]) }}"
                                                            class="btn btn-sm bg-success">whatsapp
                                                        </a></td>
                                                        <td>
                                                            <span class="info-span">
                                                                @if ($item->class_frequency == 'M')
                                                                    Month
                                                                @elseif($item->class_frequency == 'D')
                                                                    Every Day
                                                                @elseif($item->class_frequency == 'W')
                                                                    Week
                                                                @endif
                                                            </span>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
@endpush
