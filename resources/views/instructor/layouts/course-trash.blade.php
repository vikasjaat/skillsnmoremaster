@extends('layouts.instructor.master')
@section('title', 'Trash-Course')
@push('css')
    <link rel="stylesheet" href="{{ asset('instassets/plugins/datatables/datatables.min.css') }}">
@endpush
@push('script')
@endpush
@section('content')
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Category Tables</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('instructor.dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Course Tables</li>
                    </ul>
                </div>
                <div class="col">
                    <a href="{{route('instructor.course')}}">
                    <button class="btn btn-info d-inline-block m-2 float-end ">Go To Course List</button></a>
                </div>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
        <div class="form-group row">
            <label class="col-form-label col-md-2">Select Category</label>
            <div class="col-md-10">
                <select class="form-control form-select" id="myInput">
                    <option value="">Select Category</option>
                    @foreach ($cat as $item)
                        <option value="{{ $item->name }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatable table table-stripped">
                                <thead>
                                    <tr>
                                        <th>SR.</th>
                                        <th>Course Name</th>
                                        <th>Category Name</th>
                                        <th>Price</th>
                                        <th>Frequency</th>
                                        <th>Duration</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="myTable">
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach ($courselist as $item)
                                        <tr>
                                            <td> {{ $i }}</td>
                                            <td> {{ $item->name }}</td>
                                            <td>{{ $item->cat_name }}</td>
                                            <td>{{ $item->price }}</td>
                                            <td>{{ $item->frequency }}</td>
                                            <td>{{ $item->duration }} Minute</td>
                                            <td>                                                
                                                <a href="{{ route('instructor.restore', [$item->id]) }}"
                                                    class="btn btn-sm bg-success"><i
                                                        class="trash-restore-alt"></i>Restore</a>
                                            </td>
                                        </tr>
                                        @php
                                            $i++;
                                        @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script src="{{ asset('instassets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('instassets/plugins/datatables/datatables.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $("#myInput").on("click", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable tr").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>
    @endpush
@endsection