@extends('layouts.instructor.master')
@section('title', 'List-Course')
@push('css')
    <link rel="stylesheet" href="{{ asset('instassets/plugins/datatables/datatables.min.css') }}">
    <style>
        .input-switch {
            display: none;
        }

        .label-switch {
            display: inline-block;
            position: relative;
        }

        .label-switch::before,
        .label-switch::after {
            content: "";
            display: inline-block;
            cursor: pointer;
            transition: all 0.5s;
        }

        .label-switch::before {
            width: 3em;
            height: 1em;
            border: 1px solid #757575;
            border-radius: 4em;
            background: #888888;
        }

        .label-switch::after {
            position: absolute;
            left: 0;
            top: -20%;
            width: 1.5em;
            height: 1.5em;
            border: 1px solid #757575;
            border-radius: 4em;
            background: #ffffff;
        }

        .input-switch:checked~.label-switch::before {
            background: #00a900;
            border-color: #008e00;
        }

        .input-switch:checked~.label-switch::after {
            left: unset;
            right: 0;
            background: #00ce00;
            border-color: #009a00;
        }

        .info-text {
            display: inline-block;
        }

        .info-text::before {
            content: "Inactive";
        }

        .input-switch:checked~.info-text::before {
            content: "Active";
        }

    </style>
@endpush
@section('content')
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Course Tables</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('instructor.dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Course Tables</li>
                    </ul>
                </div>
                <div class="row justify-content-between">
                    <div class="col-md-4 top-nav-search">
                        <form action="{{ route('instructor.course') }}" method="POST">
                            @csrf
                            <input type="search" class="form-control" name="search"
                                value="@if (isset($search)) {{ $search }} @endif"
                                placeholder="Search here">
                            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                        </form>
                    </div>
                    <div class="col-md-6 align-self-end">
                        <a href="{{ route('instructor.course') }}">
                            <button class="btn btn-info d-inline-block m-2">Search Reset</button></a>
                        <a href="{{ route('instructor.trashview') }}">
                            <button class="btn btn-danger d-inline-block m-2">Go To Tresh</button></a>
                        <a href="{{ route('instructor.addcourse') }}">
                            <button class="btn btn-success d-inline-block m-2">Add Course</button></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label col-md-2">Select Category</label>
            <div class="col-md-10">
                <select class="form-control form-select" id="myInput">
                    <option value="">Select Category</option>
                    @foreach ($cat as $item)
                        <option value="{{ $item->name }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="">
                    <div class="">
                        <div class="table-responsive">
                            <table class="datatable table table-stripped">
                                <thead>
                                    <tr class="text-center">
                                        <th>SR.</th>
                                        <th>Course</th>
                                        <th>Category</th>
                                        <th>Frequency</th>
                                        <th>Duration</th>
                                        <th>Status</th>
                                        <th>Schedule</th>
                                    </tr>
                                </thead>
                                <tbody id="myTable">
                                    @if (isset($courselist))
                                        @foreach ($courselist as $key => $item)
                                            <tr>
                                                <td> {{ $key + 1 }}</td>
                                                <td> {{ course($item->name) }}</td>
                                                <td>{{ $item->cat_name }}</td>
                                                <td>{{ $item->frequency }}</td>
                                                <td>{{ $item->duration }} Mintue</td>
                                                <td class="text-center">
                                                    @if ($item->status == '1')
                                                        <span class="badge badge-success"> Active</span>
                                                    @else
                                                        <span class="badge badge-danger"> Inactive</span>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <div style="width:110px">
                                                        <a href="{{ route('instructor.createschedule', [$item->id]) }}"
                                                            class="btn btn-sm btn-primary"><i
                                                                class="fas fa-plus"></i></a>
                                                        <a href="{{ route('instructor.addevent', [$item->id]) }}"
                                                            class="btn btn-sm bg-warning"><i class="far fa-eye"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('instassets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('instassets/plugins/datatables/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#myInput").on("click", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
        if ($('.datatable').length > 0) {
            $('.datatable').DataTable({
                "bFilter": false,
            });
        }
    </script>
@endpush