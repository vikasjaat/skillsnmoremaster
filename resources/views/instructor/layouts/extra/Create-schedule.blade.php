@extends('layouts.instructor.master')
@section('title', 'Add-Schedule')
@push('css')
    <link rel="stylesheet" href="{{ asset('instassets/plugins/datatables/datatables.min.css') }}">
@endpush
@push('script')
@endpush
@section('content')
    <form>
        <div class="row">
            <div class="card">
                <div class="row">
                    <div class="col-12">
                        <h5 class="form-title"><span>Course-Schedule-Table</span></h5>
                    </div>
                    <input type="hidden" class="form-control" value="PRE1534">
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label>Category</label>
                            <input type="text" class="form-control" value="Music">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label>Course Name</label>
                            <input type="text" class="form-control" value="Pop-Music">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label>Duraction Minute</label>
                            <input type="text" class="form-control" value="3600 Mint">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label>Course Start Date</label>
                            <input type="text" class="form-control" value="23 Jul 2020">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label>Start Time</label>
                            <input type="text" class="form-control" value="01:00 PM">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label>End Time</label>
                            <input type="text" class="form-control" value="01:45 PM">
                        </div>
                    </div>
                </div>

                <button type="button" class="btn btn-primary">Regester</button>
            </div>
        </div>
    </form>

        @push('scripts')
            <script src="{{ asset('instassets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
            <script src="{{ asset('instassets/plugins/datatables/datatables.min.js') }}"></script>
            <script>
                $(document).ready(function() {
                    $("#myInput").on("click", function() {
                        var value = $(this).val().toLowerCase();
                        $("#myTable tr").filter(function() {
                            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                        });
                    });
                });
                if ($('.datatable').length > 0) {
                    $('.datatable').DataTable({
                        "bFilter": false,
                    });
                }
            </script>
        @endpush
    @endsection
