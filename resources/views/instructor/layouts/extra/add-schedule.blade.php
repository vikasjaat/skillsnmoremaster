@extends('layouts.instructor.master')
@section('title', 'Add-Schedule')
@push('css')
    <link rel="stylesheet" href="{{ asset('instassets/plugins/datatables/datatables.min.css') }}">
    <style>
        .input-switch {
            display: none;
        }

        .label-switch {
            display: inline-block;
            position: relative;
        }

        .label-switch::before,
        .label-switch::after {
            content: "";
            display: inline-block;
            cursor: pointer;
            transition: all 0.5s;
        }

        .label-switch::before {
            width: 3em;
            height: 1em;
            border: 1px solid #757575;
            border-radius: 4em;
            background: #888888;
        }

        .label-switch::after {
            position: absolute;
            left: 0;
            top: -20%;
            width: 1.5em;
            height: 1.5em;
            border: 1px solid #757575;
            border-radius: 4em;
            background: #ffffff;
        }

        .input-switch:checked~.label-switch::before {
            background: #00a900;
            border-color: #008e00;
        }

        .input-switch:checked~.label-switch::after {
            left: unset;
            right: 0;
            background: #00ce00;
            border-color: #009a00;
        }

        .info-text {
            display: inline-block;
        }

        .info-text::before {
            content: "Not active";
        }

        .input-switch:checked~.info-text::before {
            content: "Active";
        }

    </style>
@endpush
@push('script')
@endpush
@section('content')
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add-Schedule</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    @if (isset($course))
                        <form action="{{ route('instructor.savesche') }}" method="POST">
                            @foreach ($cat as $item)
                                @if ($item->id == $course->category)
                                    <div class="row">
                                        <div class="card">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h5 class="form-title"><span>Course-Schedule</span></h5>
                                                </div>
                                                <input type="hidden" class="form-control" value="PRE1534">
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label>Category</label>
                                                        <input type="text" readonly class="form-control"
                                                            value="{{ $item->name }}">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label>Course Name</label>
                                                        <input type="text" readonly class="form-control"
                                                            value="{{ $course->name }}">

                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label>Duraction Minute</label>
                                                        <input type="" readonly class="form-control"
                                                            value="{{ $course->duration }}">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="col-md-6">
                                                        <div class="form-check form-switch">
                                                            <div class="form-group">
                                                                <input class='input-switch' type="checkbox" id="demo"
                                                                    checked name="status"
                                                                    value="@if (isset($data)) {{ $data->price }} @endif 1" />
                                                                <label class="label-switch" for="demo"></label>
                                                                <span class="info-text"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                                
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label>On Date</label>
                                                        <input type="date" name="on_date" class="form-control" value="">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label>End Date</label>
                                                        <input type="date" name="end_date" class="form-control" value="">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label>Start Time</label>
                                                        <input type="time" name="on_time" class="form-control"
                                                            value="01:00 PM">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label>End Time</label>

                                                        <input type="time" name="end_time" class="form-control"
                                                            value="01:45 PM">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                    @endif
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary">Regester</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            $(document).ready(function() {
                @isset($course)
                    new bootstrap.Modal(document.getElementById('exampleModal')).show();
                @endisset
                $("#myInput").on("click", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable tr").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>
    @endpush
@endsection
