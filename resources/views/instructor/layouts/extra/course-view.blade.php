@extends('layouts.instructor.master')
@section('title', 'View-Course')
@push('css')
    <link rel="stylesheet" href="{{ asset('instassets/plugins/datatables/datatables.min.css') }}">
@endpush
@push('script')
@endpush
@section('content')
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">Course Details</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="">Course</a></li>
                        <li class="breadcrumb-item active">Course Details</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="about-info">
                            <h4>About Course</h4>
                            <div class="media mt-3 d-flex">
                                <img src="{{ asset('storage/courses/' . $courselist->image) }}" alt="Course Image">
                                <div class="media-body flex-grow-1">
                                    <ul>
                                        <li>
                                            <span class="title-span">Course Name : </span>
                                            <span class="info-span">{{ $courselist->name }}</span>
                                        </li>
                                        <li>
                                            <span class="title-span">Category : </span>
                                            <span class="info-span">{{ $courselist->cat_name }}</span>
                                        </li>
                                        <li>
                                            <span class="title-span">Price : </span>
                                            <span class="info-span">{{ $courselist->price }} $</span>
                                        </li>
                                        <li>
                                            <span class="title-span">Duration : </span>
                                            <span class="info-span">{{ $courselist->duration }} Min.</span>
                                        </li>
                                        <li>
                                            <span class="title-span">Frequency : </span>
                                            <span class="info-span">{{ $courselist->frequency }}</span>
                                        </li>
                                        <li>
                                            <span class="title-span">Status : </span>
                                            @if ($courselist->status == '1')
                                                <span class="badge badge-success"> Active</span>
                                            @else
                                                <span class="badge badge-danger"> Inactive</span>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <h4>Summary</h4>
                                    @php
                                        echo $courselist->summary;
                                    @endphp
                                </div>
                            </div>
                            <div class="row follow-sec">
                                <div class="col-md-4 mb-3">
                                    <div class="blue-box">
                                        <h3>2850</h3>
                                        <p>Join Student</p>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="blue-box">
                                        <h3>2050</h3>
                                        <p>Panding Student</p>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="blue-box">
                                        <h3>2950</h3>
                                        <p>Complete Student</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-12">
                                    <h4>Video</h4>
                                    <p> No Videos</p>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-12">
                                    <h5>Published</h5>
                                    <p>No</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
        </div>
    </div>
    @push('scripts')
    @endpush
@endsection
