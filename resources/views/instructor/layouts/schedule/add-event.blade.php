@extends('layouts.instructor.master')
@section('title', 'Add-Student')
@push('css')
    <link rel="stylesheet" href="{{ asset('instassets/css//mdtimepicker.css') }}">

    <style>
        .visi {
            visibility: hidden;
        }

        .datepicker .form-control,
        .timepicker .form-control,
        .datetimepicker .form-control {
            background: #fff;
        }

    </style>
@endpush
@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('instructor.storeschedule') }}" method="POST">
                @csrf
                <div class="content container-fluid">
                    <div class="page-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="page-title">Add Schedule</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="">Schedule</a></li>
                                    <li class="breadcrumb-item active">Add Schedule</li>
                                </ul>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-12">
                            <h5 class="form-title"><span>Course-Schedule</span></h5>
                        </div>
                        <input type="hidden" class="form-control" name="course_id" value="{{ $data->id }}">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label>Category</label>
                                <input type="text" readonly class="form-control" value="{{ $cat->name }}">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label>Course Name</label>
                                <input type="text" readonly class="form-control" value="{{ $data->name }}">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label>Duraction Minute</label>
                                <input type="" readonly class="form-control" value="{{ $data->duration }}">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label>Batch Start Date</label>
                                <input type="date" name="start_at" class="form-control" value="">
                            </div>
                            <span class="text-danger">
                                @error('start_at')
                                    {{ $message }}
                                @enderror
                            </span>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label>Batch Name</label>
                                <input type="text" name="batch_name" class="form-control" value="" placeholder="Enter Batch Name">
                            </div>
                            <span class="text-danger">
                                @error('batch_name')
                                    {{ $message }}
                                @enderror
                            </span>
                        </div>
                        <div class="col-12 col-sm-3">
                            <div class="col-md-4">
                                <div class="form-check form-switch">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input " name="status" type="checkbox" id="demo"
                                                value="1" checked>
                                            <label class="form-check-label" for="demo"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <span class="text-danger">
                                @error('status')
                                    {{ $message }}
                                @enderror
                            </span>
                        </div>
                        <div class="col-12 col-sm-3">
                            <label>Class Frequency</label>
                            <div>
                                <div class="form-check form-check-inline ">
                                    <input class="form-check-input" type="radio"  name="class_frequency"
                                        id="inlineRadio2" value="W">
                                    <label class="form-check-label" for="inlineRadio2">weekly</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="class_frequency" id="inlineRadio3"
                                        value="M">
                                    <label class="form-check-label" for="inlineRadio3">monthly</label>
                                </div>
                            </div>
                            <span class="text-danger">
                                @error('class_frequency')
                                    {{ $message }}
                                @enderror
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-12">
                                <h5 class="form-title"><span></span></h5>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label for="evv">Event Name</label>
                                    <input type="text" id="evv" name="eventname" value="" class="form-control" placeholder=" Enter Class Name">
                                </div>
                                <span class="text-danger">
                                    @error('eventname')
                                        {{ $message }}
                                    @enderror
                                </span>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Duraction(Week)</label>
                                    <input type="number" name="week" value="" class="form-control" placeholder="Enter Duraction(Week)">
                                </div>
                                <span class="text-danger">
                                    @error('week')
                                        {{ $message }}
                                    @enderror
                                </span>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label>Class(Per Week)</label>
                                    <select class="form-control form-select" id="inclass">
                                        <option selected value="">Select</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                    </select>
                                </div>                           
                             </div>
                            <div id="myTable">
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" id="myTable">Submit</button>
            </form>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('instassets/js/mdtimepicker.js') }}"></script>
    <script>
        $('select').change(function() {
            var select = $('#myTable').html('');
            for (var i = 0; i < parseInt($(this).val()); i++) {
                var row =
                    '<div class="form-group row">\
                                        <div class="col-3">\
                                    <div class="form-group">\
                                    <label>Select Day</label>\
                                    <select class="form-control" name="schedule[]" form-select>\
                                        <option>-- Select --</option>\
                                        <option value="sunday">Sunday</option>\
                                        <option value="monday" >Monday</option>\
                                        <option value="tuesday">Tuesday</option>\
                                        <option value="wednesday">Wednesday</option>\
                                        <option value="thrusday">Thursday</option>\
                                        <option value="friday">Friday</option>\
                                        <option value="saturday">Saturday</option>\
                                        </select>\
                                        </div>\
                                        </div>\
                                        <div class="col-3">\
                                            <div class="form-group">\
                                                <label>Start Time</label>\
                                            <input type="text" name="on_time[]" class="form-control timepicker"  value="">\
                                            </div>\
                                            </div>\
                                            <div class="col-3">\
                                                <div class="form-group">\
                                                    <label>End Time</label>\
                                                <input type="text" name="end_time[]" class="form-control timepicker" value="">\
                                                </div>\
                                                </div>\
                                                </div>';
                row = $(row);
                $('#myTable').append(row);
            }
            select.promise().done(function(arg1) {
                $('.timepicker').mdtimepicker();
            })
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.timepicker').mdtimepicker();
        });
    </script>
@endpush
