@extends('layouts.instructor.master')
@section('title', 'List-Course')
@push('css')
    <link rel="stylesheet" href="{{ asset('instassets/plugins/datatables/datatables.min.css') }}">
    <style>
        .input-switch {
            display: none;
        }

        .label-switch {
            display: inline-block;
            position: relative;
        }

        .label-switch::before,
        .label-switch::after {
            content: "";
            display: inline-block;
            cursor: pointer;
            transition: all 0.5s;
        }

        .label-switch::before {
            width: 3em;
            height: 1em;
            border: 1px solid #757575;
            border-radius: 4em;
            background: #888888;
        }

        .label-switch::after {
            position: absolute;
            left: 0;
            top: -20%;
            width: 1.5em;
            height: 1.5em;
            border: 1px solid #757575;
            border-radius: 4em;
            background: #ffffff;
        }

        .input-switch:checked~.label-switch::before {
            background: #00a900;
            border-color: #008e00;
        }

        .input-switch:checked~.label-switch::after {
            left: unset;
            right: 0;
            background: #00ce00;
            border-color: #009a00;
        }

        .info-text {
            display: inline-block;
        }

        .info-text::before {
            content: "Inactive";
        }

        .input-switch:checked~.info-text::before {
            content: "Active";
        }

    </style>
@endpush
@section('content')
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Schedule Tables</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('instructor.dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Schedule Tables</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="about-info">
                <div class="media mt-3 d-flex">
                    {{-- <img src="" alt="Course Image"> --}}
                    <div class="media-body flex-grow-1">
                        <ul>
                            <li>
                                <span class="title-span">Batch Name : </span>
                                <span class="info-span">{{ batchname($batch[0]->batch_name) }}</span>
                            </li>
                            <li>
                                <span class="title-span">Frequency : </span>
                                <span class="info-span">
                                    @if ('W' == 'W')
                                        (Week)
                                    @elseif('M' == 'M')
                                        (Month)
                                    @endif
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="">
                    <div class="">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0 datatable">
                                <thead>
                                    <tr>
                                        <th class="text-center">Id</th>
                                        <th>Student Name</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="myTable">
                                    @foreach ($course as $key => $item)
                                        @if ($item->schedule_status == 0)
                                            <tr>
                                                <td class="text-center">{{ $key + 1 }}</td>
                                                <td>{{ $item->studentName }}</td>
                                                <td>
                                                    @if ($item->status == 'pending')
                                                        <span class="badge badge-danger">{{ $item->status }}</span>
                                                    @else
                                                        <span class="badge badge badge-success">{{ $item->status }}</span>
                                                    @endif
                                                </td>

                                                <td class="text-center">
                                                    <div style="width:110px">
                                                        @if ($item->status == 'active')
                                                            <a href="{{ route('instructor.storestudent', [$item->id, $batch[0]->batch_name]) }}"
                                                                class="btn btn-sm btn-success"><i
                                                                    class="fa fa-check"></i></a>
                                                        @else
                                                            <div style="width:110px">
                                                                <button class="btn btn-sm btn-danger"><i
                                                                        class="fa fa-ban"></i></a>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('instassets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('instassets/plugins/datatables/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#myInput").on("click", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
        if ($('.datatable').length > 0) {
            $('.datatable').DataTable({
                "bFilter": false,
            });
        }
    </script>
    {{-- <script>
               $('#SubmitForm').on('submit', function(e) {
            e.preventDefault();
            let name = $('#InputName').val();
            let lname = $('#InputlName').val();
            let fname = $('#InputfName').val();
            let email = $('#InputEmail').val();
            let password = $('#InputPassword').val();
            let dataobj = {
                "_token": "{{ csrf_token() }}",
                name: name,
                lname: lname,
                fname: fname,
                email: email,
                password: password,
            };
            if ($('#edit-id').val() != "") {
                let id = $('#edit-id').val();
                $.extend(dataobj, {
                    id: id
                });
            }
            // console.log(dataobj);
            // console.log(name+lname+fname+email+password);
            $.ajax({
                url: '{{ route('Submit') }}',
                type: "POST",
                data: dataobj,
                success: function(response) {
                    loaddata();
                    $('#successMsg').show();
                    $('#InputName').val("");
                    $('#InputlName').val("");
                    $('#InputfName').val("");
                    $('#InputEmail').val("");
                    $('#InputPassword').val("");
                    $('#submit').text("Submit");
                    // console.log(response);
                },
                error: function(response) {
                    $('#nameErrorMsg').text(response.responseJSON.errors.name);
                    $('#lnameErrorMsg').text(response.responseJSON.errors.lname);
                    $('#fnameErrorMsg').text(response.responseJSON.errors.fname);
                    $('#emailErrorMsg').text(response.responseJSON.errors.email);
                    $('#passwordErrorMsg').text(response.responseJSON.errors.password);
                }
            });
        });
    </script> --}}
@endpush
