@extends('layouts.instructor.master')
@section('title', 'Add-Student')
@push('css')
@endpush
@section('content')
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">Schedule Time Table</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="">Schedule</a></li>
                        <li class="breadcrumb-item active">Time Table Details</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="">
            <div class="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="about-info">
                            <div class="media mt-3 d-flex">
                                <img src="{{ asset('storage/courses/' . $users[0]->image) }}" alt="Course Image">
                                <div class="media-body flex-grow-1">
                                    <ul>
                                        <li>
                                            <span class="title-span">Course : </span>
                                            <span class="info-span"> {{ course($users[0]->name) }}</span>
                                        </li>
                                        <li>
                                            <span class="title-span">Batch Name : </span>
                                            <span class="info-span">{{ batchname($timetable[0]->batch_name) }}</span>
                                        </li>
                                        <li>
                                            <span class="title-span">Duration : </span>
                                            <span class="info-span"> {{ $users[0]->duration }}(Minute)</span>
                                        </li>
                                        <li>
                                            <span class="title-span">Frequency : </span>
                                            <span class="info-span">
                                                @if ($timetable[0]->class_frequency == 'W')
                                                    Weekly
                                                @elseif($timetable[0]->class_frequency == 'M')
                                                    Monthly
                                                @endif
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 d-flex">
                        <div class="card flex-fill">
                            <div class="card-header">
                                <ul role="tablist" class="nav nav-pills bg-warring card-header-pills">
                                    <li class="nav-item">
                                        <a href="#tab-4" data-bs-toggle="tab" class="nav-link active">Student Table </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#tab-5" data-bs-toggle="tab" class="nav-link">Schedule Table</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content pt-0">
                                    <div role="tabpanel" id="tab-4" class="tab-pane fade show active">
                                        <table class="datatable table table-stripped">
                                            <div class="table-responsive">
                                                <thead>
                                                    <tr class="text-center">
                                                        <th>Id</th>
                                                        <th>Name</th>
                                                        <th>Course Name</th>
                                                        <th>Batch Name</th>
                                                        <th>Start Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="myTable">
                                                    @foreach ($user as $item => $value)
                                                        <tr class="text-center">
                                                            <td>{{ $item + 1 }}</td>
                                                            <td>{{ $value['studentName'] }}</td>
                                                            <td> {{ course($users[0]->name) }}</td>
                                                            <td>{{ batchname($timetable[0]->batch_name) }}</td>
                                                            <td>{{ $timetable[0]->date }}</td>
                                                            <td><a href=""
                                                                    class="btn btn-sm bg-success">whatsapp
                                                                </a></td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </div>
                                        </table>
                                    </div>
                                    <div role="tabpanel" id="tab-5" class="tab-pane fade text-center">
                                        <table class="datatable table table-stripped">
                                            <thead>
                                                <div class="table-responsive">
                                                    <tr class="text-center">
                                                        <th>Date</th>
                                                        <th>Day</th>
                                                        <th>Start Time</th>
                                                        <th>End Time</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                            </thead>
                                            <tbody id="myTable">
                                                @foreach ($timetable as $item)
                                                    <tr class="text-center">
                                                        <td>{{ date('d-M-y', strtotime($item->date)) }}</td>
                                                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $item->date)->format('l') }}
                                                        </td>
                                                        <td>{{ date('g:i a', strtotime($item->on_time)) }}</td>
                                                        <td>{{ date('g:i a', strtotime($item->end_time)) }}</td>
                                                        <td>
                                                            @if ($item->status == '1')
                                                                <span class="badge badge-success"> Active</span>
                                                            @else
                                                                <span class="badge badge-danger">
                                                                    Inactive</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a href="" class="btn btn-sm bg-info"><i
                                                                    class="far fa-edit"></i></a>
                                                            <a href="" class="btn btn-sm bg-danger"><i
                                                                    class="far fa-trash"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @push('scripts')
        <script>
            $(".tab-list li").on("click", function() {
                var tabId = ".tab-list li#" + $(this).attr("id");
                var tabDivId = ".tabs-content#content-" + $(this).attr("id");
                if (!$(this).hasClass("active")) {
                    $(".tab-list li").removeClass("active");
                    $(this).addClass("active");
                    $(".tabs-content").removeClass("active");
                    $(tabDivId).addClass("active");
                }
            });
        </script>
    @endpush
