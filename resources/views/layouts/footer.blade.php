<!-- footer-start -->
<footer class="t-footer">
   <div class="container">
      <div class="row">
         <div class="col-lg-3 col-md-6">
            <div class="footer-logo">
               <a href="{{route('home-page')}}"><img src="{{asset('assets/images/logo.svg')}}"></a>
               <p>Developed by experts, our fun approach of learning goes beyond traditional ways of learning to build confidence, promote analytical skills and creativity and promote a lifelong love of learning.
               </p>
            </div>
         </div>
         <div class="col-lg-3 col-md-6">
            <div class="link-section">
               <h4 class="quick-link-tittle">Courses</h4>
               <ul class="quick-link-list">
                  <li><a href="javascript:void(0);">Art & Craft</a></li>
                  <li><a href="javascript:void(0);">Fitness</a></li>
                  <li><a href="javascript:void(0);">Mathematics</a></li>
                  <li><a href="javascript:void(0);">Music</a></li>
                  <li><a href="javascript:void(0);">Sketching & Drawing</a></li>
                  <li><a href="javascript:void(0);">Vedic Maths</a></li>
                  <li><a href="javascript:void(0);">Coding</a></li>
               </ul>
            </div>
         </div>
         <div class="col-lg-3 col-md-6">
            <div class="link-section">
               <h4 class="quick-link-tittle">Important Link</h4>
               <ul class="quick-link-list">
                  <li><a href="{{route('about-us')}}" class="signinstudent">About Us</a></li>
                  <li><a href="{{route('contact-us')}}" class="signinstudent">Contact Us</a></li>
                  <li><a href="{{url('/register')}}">Register</a></li>
                  <li><a href="{{url('/terms-and-conditions')}}">Terms & Condition</a></li>
                  <li><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
                  <li><a href="javascript:void(0);">FAQ</a></li>
               </ul>
            </div>
         </div>
         <div class="col-lg-3 col-md-6">
            <h4 class="quick-link-tittle">Join Us</h4>
            <div class="social-media-icon">
               <a href="https://www.facebook.com/skillsnmore/" target="_blank"><i class="fab fa-facebook-f"></i></a>
               <a href="https://www.instagram.com/skills_n_more/" target="_blank"><i class="fab fa-instagram"></i></a>
               <a href="https://twitter.com/skillsnmore" target="_blank"><i class="fab fa-twitter"></i></a>
               <a href="https://www.linkedin.com/company/skillsnmore" target="_blank"><i class="fab fa-linkedin-in"></i></a>
            </div>
         </div>
      </div>
   </div>
   <div class="copyright-section">
      <div class="container">
         <p class="text-center mb-0">Copyright &copy; 2022 <a href="{{route('home-page')}}" class="d-inline-block">SkillsnMore</a> All rights reserved.</p>
      </div>
   </div>
</footer>
<!-- footer-end -->
<div class="shape-bubble-wrapper">
   <div class="bubbleshape bubbleshape1"></div>
   <div class="bubbleshape bubbleshape2"></div>
   <div class="bubbleshape bubbleshape3"></div>
   <div class="bubbleshape bubbleshape4"></div>
   <div class="bubbleshape bubbleshape5"></div>
   <div class="bubbleshape bubbleshape6"></div>
   <div class="bubbleshape bubbleshape7"></div>
   <div class="bubbleshape bubbleshape8"></div>
   <div class="bubbleshape bubbleshape9"></div>
   <div class="bubbleshape bubbleshape10"></div>
</div>
<!-- sign-in category modal start -->
<div class="modal fade question-s-signin sign-card" id="userSignType-modal" tabindex="-1">
   <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Please select Category</h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>
         </div>
         <div class="modal-body question-card">
            <div class="row">
               <div class="col-md-6 col-sm-6">
                  <a class="card signupteacher" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#InstructorSignIN">
                     <img src="{{asset('assets/images/question/instructor.svg')}}" class="c-pic">
                     <p> I am an Instructor</p>
                     <div>
                        <img src="{{asset('assets/images/question/pen.svg')}}" class="pencil-pic">
                     </div>
                     <div class="icon-click">
                        <img src="{{asset('assets/images/question/click.svg')}}" class="">
                     </div>
                  </a>
               </div>
               <div class="col-md-6 col-sm-6">
                  <a class="card signupstudent" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#StudentSignIN">
                     <img src="{{asset('assets/images/question/studentj.svg')}}" class="c-pic">
                     <p> I am a Student</p>
                     <div>
                        <img src="{{asset('assets/images/question/pencilj.svg')}}" class="pencil-pic">
                     </div>
                     <div class="icon-click">
                        <img src="{{asset('assets/images/question/click.svg')}}" class="">
                     </div>
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- sign-in category modal end -->
<!-- sign-up category modal start -->
<div class="modal fade question-s sign-card" id="userType-modal" tabindex="-1">
   <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Please select Category</h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>
         </div>
         <div class="modal-body question-card">
            <div class="row">
               <div class="col-md-6 col-sm-6 col-6">
                  <a class="card signupteacher" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#instructor-signup-modal">
                     <img src="{{asset('assets/images/question/instructor.svg')}}" class="c-pic">
                     <p> I am an Instructor</p>
                     <div>
                        <img src="{{asset('assets/images/question/pen.svg')}}" class="pencil-pic">
                     </div>
                     <div class="icon-click">
                        <img src="{{asset('assets/images/question/click.svg')}}" class="">
                     </div>
                  </a>
               </div>
               <div class="col-md-6 col-sm-6 col-6">
                  <a class="card signupstudent" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#student-signup-modal">
                     <img src="{{asset('assets/images/question/studentj.svg')}}" class="c-pic">
                     <p> I am a Student</p>
                     <div>
                        <img src="{{asset('assets/images/question/pencilj.svg')}}" class="pencil-pic">
                     </div>
                     <div class="icon-click">
                        <img src="{{asset('assets/images/question/click.svg')}}" class="">
                     </div>
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- sign-up category modal end -->
<!-- Course Modal Start -->
<!-- The Modal -->
<div class="modal fade" id="course-modal" tabindex="-1">
    <div class="modal-dialog  modal-lg modal-dialog-centered">
        <form class="modal-content" id="bookDemo">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Book Your Demo Class</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <!-- Modal body -->
            <div class="modal-body question-card">
                <div class="row bookDemo">
                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Student's Name<i class="far fa-asterisk"></i></label>
                            <input type="text" name="FirstName" class="form-control" required>
                        </div>
                     </div>

                     <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Parent/Guardian's Name<i class="far fa-asterisk"></i></label>
                            <input type="text" name="Parent-Name" class="form-control" required>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="mb-4">
                            <label class="mb-2">Student's Name<i class="far fa-asterisk"></i></label>
                            <input type="text" name="Student-Name" class="form-control" required>
                        </div>
                     </div>

                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Age<i class="far fa-asterisk"></i></label>
                            <input type="text" name="Age" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Mobile Number<i class="far fa-asterisk"></i></label>
                            <input type="text" name="Phone" class="form-control phone" required>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Email Id<i class="far fa-asterisk"></i></label>
                            <input type="email" name="Email" class="form-control email-validate" required>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Course<i class="far fa-asterisk"></i></label>
                            <select class="form-select form-control" name="Subject" aria-label="Default select example" required>
                                <option selected="">I Am Interested In</option>
                                @foreach($modal_categories as $modal_cat)
                                    <option value="{{$modal_cat->name}}">{{$modal_cat->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="mb-4">
                                    <label class="mb-2">State<i class="far fa-asterisk"></i></label>
                                    <select class="form-select form-control modalStateList" name="State" aria-label="Default select example" citylist="modalCityList1" required>
                                        <option value="">Select a state</option>
                                        @foreach($modal_state_list as $key => $modal_state)
                                        <option value="{{$modal_state->name}}" state-id="{{$modal_state->id}}">{{$modal_state->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="mb-4">
                                    <label class="mb-2">City<i class="far fa-asterisk"></i></label>
                                    <select class="form-select form-control" name="City" aria-label="Default select example" id="modalCityList1" required>
                                        <option value="">Select State First</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="theme-btn submitform" form-id="bookdemo">Submit</button>
            </div>
        </form>
    </div>
</div>
<!-- Course Modal End -->
<!-- sign-in popup start -->
<div class="modal fade question-s-signin sign-card" tabindex="-1">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <form class="modal-content" id="bookDemo">
            <div class="modal-header">
                <h4 class="modal-title">Please select Category</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <div class="modal-body question-card">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <a class="card signupteacher" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#instructor-signup-modal">
                            <img src="{{asset('assets/images/question/instructor.svg')}}" class="c-pic">
                            <p> I am an Instructor</p>
                            <div>
                                <img src="{{asset('assets/images/question/pen.svg')}}" class="pencil-pic">
                            </div>
                            <div class="icon-click">
                                <img src="{{asset('assets/images/question/click.svg')}}" class="">
                            </div>
                            <!--   <div class="icon-s-card">
                        <img src="{{asset('assets/images/teacher.svg')}}">
                        </div> -->
                        </a>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <a class="card signupstudent" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#student-signup-modal">
                            <img src="{{asset('assets/images/question/studentj.svg')}}" class="c-pic">
                            <p> I am a Student</p>
                            <div>
                                <img src="{{asset('assets/images/question/pencilj.svg')}}" class="pencil-pic">
                            </div>
                            <div class="icon-click">
                                <img src="{{asset('assets/images/question/click.svg')}}" class="">
                            </div>
                            <!--   <div class="icon-s-card">
                        <img src="{{asset('assets/images/student.svg')}}">
                        </div> -->
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- sign-in popup end -->
<div class="modal fade question-s sign-card" id="userType-modal" tabindex="-1">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <form class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Please select Category</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <div class="modal-body question-card">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-6">
                        <a class="card signupteacher" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#instructor-signup-modal">
                            <img src="{{asset('assets/images/question/instructor.svg')}}" class="c-pic">
                            <p> I am an Instructor</p>
                            <div>
                                <img src="{{asset('assets/images/question/pen.svg')}}" class="pencil-pic">
                            </div>
                            <div class="icon-click">
                                <img src="{{asset('assets/images/question/click.svg')}}" class="">
                            </div>
                            <!--   <div class="icon-s-card">
                        <img src="{{asset('assets/images/teacher.svg')}}">
                        </div> -->
                        </a>
                    </div>
                    <div class="col-md-6 col-sm-6 col-6">
                        <a class="card signupstudent" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#student-signup-modal">
                            <img src="{{asset('assets/images/question/studentj.svg')}}" class="c-pic">
                            <p> I am a Student</p>
                            <div>
                                <img src="{{asset('assets/images/question/pencilj.svg')}}" class="pencil-pic">
                            </div>
                            <div class="icon-click">
                                <img src="{{asset('assets/images/question/click.svg')}}" class="">
                            </div>
                            <!--   <div class="icon-s-card">
                        <img src="{{asset('assets/images/student.svg')}}">
                        </div> -->
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade sigin-teacher " tabindex="-1">
    <div class="modal-dialog  modal-dialog-centered">
        <form class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Instructor's Sign In</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <div class="modal-body question-card">
                <div class="mb-4">
                    <label class="mb-2">UserName</label>
                    <input type="text" name="" class="form-control">
                </div>
                <div class="mb-4">
                    <label class="mb-2">Password</label>
                    <input type="password" name="" class="form-control">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="theme-btn" data-bs-dismiss="modal">Submit</button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade sigin-student" tabindex="-1">
    <div class="modal-dialog  modal-dialog-centered">
        <form class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Students's Sign In</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <div class="modal-body question-card">
                <div class="mb-4">
                    <label class="mb-2">UserName</label>
                    <input type="text" name="" class="form-control">
                </div>
                <div class="mb-4">
                    <label class="mb-2">Password</label>
                    <input type="password" name="" class="form-control">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="theme-btn">Submit</button>
            </div>
        </form>
    </div>
</div>
<!-- sign-in popup end -->
<div class="modal fade signup-teacher" id="instructor-signup-modal" tabindex="-1">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <form class="modal-content" id="teachersignup">
            <div class="modal-header">
                <h5 class="modal-title">Instructor's Sign Up</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <div class="modal-body question-card">
                <div class="row teacherSignup" autocomplete="off">
                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">First Name<i class="far fa-asterisk"></i></label>
                            <input type="text" name="FirstName" class="form-control" required>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Last Name<i class="far fa-asterisk"></i></label>
                            <input type="text" name="FirstName" class="form-control" required>
                        </div>
                     </div>

                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Email Id<i class="far fa-asterisk"></i></label>
                            <input type="email" name="Email" class="form-control email-validate" required>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Category<i class="far fa-asterisk"></i></label>
                            <select class="form-select form-control" name="Category" aria-label="Default select example" required>
                                <option selected="">I Am Interested In</option>
                                @foreach($modal_categories as $modal_cat)
                                <option value="{{$modal_cat->name}}">{{$modal_cat->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Phone no<i class="far fa-asterisk"></i></label>
                            <input type="text" name="Phone" class="form-control phone" required>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Experience<i class="far fa-asterisk"></i></label>
                            <input type="text" name="Exprience" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="mb-4">
                                    <label class="mb-2">State<i class="far fa-asterisk"></i></label>
                                    <select class="form-select form-control modalStateList" name="State" aria-label="Default select example" citylist="modalCityList2" required>
                                        <option value="">Select a state</option>
                                        @foreach($modal_state_list as $key => $modal_state)
                                        <option value="{{$modal_state->name}}" state-id="{{$modal_state->id}}">{{$modal_state->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6">
                                <div class="mb-4">
                                    <label class="mb-2">City<i class="far fa-asterisk"></i></label>
                                    <select class="form-select form-control" name="City" aria-label="Default select example" id="modalCityList2" required>
                                        <option value="">Select State First</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="theme-btn submitform" form-id="teachersignup">Submit</button>
            </div>
        </form>
    </div>
</div>
<!-- sign-up Instructor modal end -->
<!-- sign-up Student modal start -->
<div class="modal fade signup-student" id="student-signup-modal" tabindex="-1">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <form class="modal-content" id="studentsignup">
            <div class="modal-header">
                <h5 class="modal-title">Student's Sign Up</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <div class="modal-body question-card">
                <div class="row studentSignup">
                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Student Name<i class="far fa-asterisk"></i></label>
                            <input type="text" name="Name" class="form-control" required>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Parent/Guardian's Name<i class="far fa-asterisk"></i></label>
                            <input type="text" name="Name" class="form-control" required>
                        </div>
                     </div>

                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">School Name</label>
                            <input type="text" name="schoolName" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Grade<i class="far fa-asterisk"></i></label>
                            <input type="text" name="Grade" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Contact Number<i class="far fa-asterisk"></i></label>
                            <input type="text" name="Phone" class="form-control phone" required>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="mb-4">
                            <label class="mb-2">Email Id<i class="far fa-asterisk"></i></label>
                            <input type="email" name="Email" class="form-control email-validate" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="mb-4">
                                    <label class="mb-2">State<i class="far fa-asterisk"></i></label>
                                    <select class="form-select form-control modalStateList" name="State" aria-label="Default select example" citylist="modalCityList3" required>
                                        <option value="">Select a state</option>
                                        @foreach($modal_state_list as $key => $modal_state)
                                        <option value="{{$modal_state->name}}" state-id="{{$modal_state->id}}">{{$modal_state->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="mb-4">
                                    <label class="mb-2">City<i class="far fa-asterisk"></i></label>
                                    <select class="form-select form-control" name="City" aria-label="Default select example" id="modalCityList3" required>
                                        <option value="">Select State First</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="theme-btn submitform" form-id="studentsignup">Submit</button>
            </div>
        </form>
    </div>
</div>
<!-- sign-up Student modal end -->
<!-- sign-up partners modal start -->
<div class="modal fade partnerpop" id="partner-modal" tabindex="-1">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <form class="modal-content" id="partnersignup">
            <div class="modal-header">
                <h5 class="modal-title">Instructor's Sign Up</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>
            </div>
            <div class="modal-body question-card">
                <div class="row partnerSignup">
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="mb-2">Name<i class="far fa-asterisk"></i></label>
                            <input type="text" name="Name" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="mb-2">Email Id<i class="far fa-asterisk"></i></label>
                            <input type="email" name="Email" class="form-control email-validate" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="mb-2">Phone no<i class="far fa-asterisk"></i></label>
                            <input type="text" name="Phone" class="form-control phone" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="mb-2">Category<i class="far fa-asterisk"></i></label>
                            <select class="form-select form-control" name="Category" aria-label="Default select example" required>
                                <option selected="">I Am Interested In</option>
                                <option value="1">Partnership Opportunities</option>
                                <option value="1">Join as an Instructor</option>
                                <option value="2">Other</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="mb-4">
                            <label for="exampleFormControlTextarea1" class="form-label">Message<i class="far fa-asterisk"></i></label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" name="Message" rows="5" required></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="theme-btn submitform" form-id="partnersignup">Submit</button>
            </div>
        </form>
    </div>
</div>
<!-- sign-up partners modal end -->
<!-- thanks message start modal -->
<div class="modal fade thankmsg" id="thankYou-modal" tabindex="-1">
   <div class="modal-dialog  modal-dialog-centered">
      <div class="modal-content border-gradient border-gradient-purple ">
         <div class="modal-header border-0">
            <button type="button" class="btn-close" data-bs-dismiss="modal"><i class="fal fa-times"></i></button>
         </div>
         <div class="modal-body text-center">
            <div class="modal-body text-center">
                <div>
                    <img src="{{asset('assets/images/thanks-msg/mail-colord-01.svg')}}" alt="icon" class="thanks-icon">
                </div>
                <h4>Thank You for connecting with us!</h4>
            </div>
        </div>
    </div>
</div>
