<!-- header-start -->
<div id="custom-overlay"></div>
<header class="t-header fixed-top">
   <div class="top-bar d-none d-lg-block">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-lg-10 col-md-10">
               <div class="top-bar-left d-flex">

                  <i class="fas fa-envelope"></i><a href="mailto:contact@skillsnmore.com">contact@skillsnmore.com</a>
               </div>
            </div>
            <div class="col-lg-2 col-md-2">
               <div class="top-bar-right text-end">
                  <a href="https://www.facebook.com/skillsnmore/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                  <a href="https://www.instagram.com/skills_n_more/" target="_blank"><i class="fab fa-instagram"></i></a>
                  <a href="https://twitter.com/skillsnmore" target="_blank"><i class="fab fa-twitter"></i></a>
                  <a href="https://www.linkedin.com/company/skillsnmore" target="_blank"><i class="fab fa-linkedin-in"></i></a>
               </div>
            </div>
         </div>
      </div>
   </div>
   <nav class="t-navbar navbar navbar-expand-lg">
      <div class="container">
         <a class="navbar-brand" href="{{route('home-page')}}"><img src="{{asset('assets/images/logo.svg')}}"></a>
         <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
         <span></span>
         <span></span>
         <span></span>
         </button>
         <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
               <li class="nav-item">
                  <a class="nav-link" href="{{route('home-page')}}" id="index_menu">Home</a>
               </li>
               <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Courses
                  </a>
                  <ul class="dropdown-menu crs-drp" aria-labelledby="navbarDropdown">
                     <li>
                        <a class="dropdown-item" href="{{route('front-course-category-all')}}">
                        <span class="icon"><img src="{{asset('assets/images/course-icon.svg')}}"></span>
                        All Courses
                        </a>
                     </li>
                     @foreach($menu_categories as $menu_category)
                     <li>
                        <a class="dropdown-item" href="{{route('front-course-category',$menu_category->slug)}}">
                        <span class="icon"><img src="{{asset('storage/category/'.$menu_category->icon)}}"></span>
                        {{$menu_category->name}}
                        </a>
                     </li>
                     @endforeach
                  </ul>
               </li>

               <li class="nav-item">
                  <a class="nav-link" href="{{route('about-us')}}" id="aboutus_menu">About Us</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="{{route('contact-us')}}" id="contact_menu">Contact Us</a>
               </li>
               <!-- <li class="nav-item">
                  <a class="nav-link sign-in-pop" href="javascript:void(0);">Sign In</a>
                  </li> -->
               <li class="nav-item">
                  <a class="nav-link sign-up-pop" data-bs-toggle="modal" data-bs-target="#userType-modal" href="javascript:void(0);">Sign Up</a>
               </li>
            </ul>
            <form class="d-none d-lg-flex justify-content-end">
               <button class="theme-btn" type="button" onclick="document.location.href='{{route('front-course-schedule-demo')}}'">Book Your Free Demo</button>
            </form>
         </div>
      </div>
   </nav>
</header>
<!-- header-end -->
