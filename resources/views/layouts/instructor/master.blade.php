<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>@yield('title')</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('layouts.instructor.partials.css')
</head>
<body>
    <div class="main-wrapper">
        <!--navigation-->
        @include('layouts.instructor.partials.header')
        <!-- SideBAr -->
        @include('layouts.instructor.partials.sidebar')
        <!--Content-->
        <div class="page-wrapper">
            @yield('content')
            <footer>
                <p>Copyright © 2022 Skill More.</p>
            </footer>
        </div>
    </div>
    @include('layouts.instructor.partials.js')
</body>
</html>
