
<link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}">
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,500;0,600;0,700;1,400&amp;display=swap">
<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/fontawsome.min.css')}}">
@stack('css')
<link rel="stylesheet" href="{{asset('assets/css/style-teacher.css')}}">