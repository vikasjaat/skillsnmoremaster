
        <div class="header">

            <div class="header-left">
                <a href="{{route('instructor.dashboard')}}" class="logo">
                    <img src="{{asset('assets/images/logo.png')}}" alt="Logo">
                </a>
                <a href="" class="logo logo-small">
                    <img src="{{asset('assets/images/logo-small.png')}}" alt="Logo" width="30" height="30">
                </a>
            </div>
            <a href="javascript:void(0);" id="toggle_btn">
                <i class="fas fa-align-left"></i>
            </a>
            
            <div class="top-nav-search">
                <form>
                    <p class="page-title">Welcome {{Auth::guard('inst')->user()->name}}</p>
                    {{-- <input type="search" class="form-control" name="search" value="" placeholder="Search here">
                    <button class="btn" type="submit"><i class="fas fa-search"></i></button> --}}
                </form>
            </div>


            <a class="mobile_btn" id="mobile_btn">
                <i class="fas fa-bars"></i>
            </a>


            <ul class="nav user-menu">

                <li class="nav-item dropdown noti-dropdown">
                    <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
                        <i class="far fa-bell"></i> <span class="badge badge-pill">3</span>
                    </a>                
                </li>


                <li class="nav-item dropdown has-arrow">
                    <a href="#" class="dropdown-toggle nav-link" data-bs-toggle="dropdown">
                        <span class="user-img"><img class="rounded-circle" src="{{asset('instassets/img/profiles/avatar-01.jpg')}}"
                                width="31" alt="Ryan Taylor"></span>
                    </a>
                    <div class="dropdown-menu">
                        <div class="user-header">
                            <div class="avatar avatar-sm">
                                <img src="{{asset('instassets/img/profiles/avatar-01.jpg')}}" alt="User Image"
                                    class="avatar-img rounded-circle">
                            </div>
                            <div class="user-text">
                                <h6>
                                    @if (Auth::guard('inst')->user()->name)
                                        {{Auth::guard('inst')->user()->name}}
                                    @endif
                                </h6>
                                <p class="text-muted mb-0"> @if (Auth::guard('inst')->user()->name)
                                    Instructor
                                @endif</p>
                            </div>
                        </div>
                        <a class="dropdown-item" href="">My Profile</a>
                        <a class="dropdown-item" href="{{route('instructor.logout')}}">Logout</a>
                    </div>
                </li>

            </ul>

        </div>