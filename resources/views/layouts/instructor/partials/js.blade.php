<script src="{{asset('assets/js/jquery-3.6.0.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('instassets/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('instassets/plugins/apexchart/apexcharts.min.js')}}"></script>
<script src="{{asset('instassets/plugins/apexchart/chart-data.js')}}"></script>
<script src="{{asset('assets/js/teacher-script.js')}}"></script>
@stack('scripts')
