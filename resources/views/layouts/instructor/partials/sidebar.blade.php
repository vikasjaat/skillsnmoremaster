<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li class="menu-title">
                    <span>Main Menu</span>
                </li>
                <li>
                    <a href="{{route('instructor.dashboard')}}"><i class="fas fa-user-graduate"></i> <span> Dashboard</span> <span
                            class=""></span></a>
                    {{-- <ul>
                        <li><a href="" >Tests</a></li>
                        <li><a href="">Questions</a></li>
                        <li><a href="">Live Lessons</a></li>
                        <li><a href="">Live Lesson Slots</a></li>
                    </ul> --}}
                </li>

                {{-- <li class="submenu">
                    <a href="#"><i class="fas fa-user-graduate"></i> <span> Students</span> <span
                            class="menu-arrow"></span></a>
                    <ul>
                        <li><a href="" class="active">Student List</a></li>
                        <li><a href="l">Student View</a></li>
                        <li><a href="">Student Edit</a></li>
                    </ul>
                </li> --}}

                <li class="submenu">
                    <a href="#"><i class="fas fa-building"></i> <span>Courses Management</span> <span
                            class="menu-arrow"></span></a>
                    <ul>
                        <li><a href="{{route('instructor.addcourse')}}">Add Courses</a></li>
                        <li><a href="{{route('instructor.course')}}">List Courses</a></li>
                        {{-- <li><a href="{{route('instructor.addschedule')}}">Add-Schedule</a></li> --}}
                        <li><a href="{{route('instructor.trashview')}}">Trash Course</a></li>


                    </ul>
                </li>
                {{-- <li class="submenu">
                    <a href="#"><i class="fa-thin fa-users"></i> <span>Student Management</span> <span
                            class="menu-arrow"></span></a>
                    <ul>
                        <li><a href="{{route('instructor.addstudent')}}">List Student</a></li>
                        <li><a href="">Complete Student List</a></li>
                        <li><a href="">Trash Student</a></li>


                    </ul>
                </li> --}}
                {{-- <li class="submenu">
                    <a href="#"><i class="fas fa-building"></i> <span>Lessons Management</span> <span
                            class="menu-arrow"></span></a>
                    <ul>
                        <li><a href="" class="active">Lessons List</a></li>
                        <li><a href="">Lessons View</a></li>
                        <li><a href="">Lessons Edit</a></li>

                    </ul>
                </li>
                <li class="menu-title">
                    <span>Management</span>
                </li>
                <li class="submenu">
                    <a href="#"><i class="fas fa-file-invoice-dollar"></i> <span> Reports</span> <span
                            class="menu-arrow"></span></a>
                    <ul>
                        <li><a href="">Sales</a></li>
                        <li><a href="">Students</a></li>
                        <li><a href="">Messages</a></li>
                        <li><a href="">Reviews</a></li>
                   </ul>
                </li>
                <li class="submenu">
                    <a href="#"><i class="fas fa-file-invoice-dollar"></i> <span> Accounts</span> <span
                            class="menu-arrow"></span></a>
                    <ul>
                        <li><a href="">Fees Collection</a></li>
                        <li><a href="">Expenses</a></li>
                        <li><a href="">Salary</a></li>
                        <li><a href="">Add Fees</a></li>
                        <li><a href="">Add Expenses</a></li>
                        <li><a href="">Add Salary</a></li>
                    </ul>
                </li>
                <li>
                    <a href=""><i class="fas fa-holly-berry"></i> <span>Holiday</span></a>
                </li>
                <li>
                    <a href=""><i class="fas fa-comment-dollar"></i> <span>Fees</span></a>
                </li>
                <li>
                    <a href=""><i class="fas fa-clipboard-list"></i> <span>Exam list</span></a>
                </li>
                <li>
                    <a href=""><i class="fas fa-calendar-day"></i> <span>Events</span></a>
                </li>
                <li>
                    <a href=""><i class="fas fa-table"></i> <span>Time Table</span></a>
                </li>
                <li>
                    <a href=""><i class="fas fa-book"></i> <span>Library</span></a>
                </li>                 --}}
            </ul>
        </div>
    </div>
</div>