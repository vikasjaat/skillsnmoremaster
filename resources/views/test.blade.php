@extends('dashboard')
@section('content')
    <section class="inner-page-banner">
        <span class="banner-shape-1 bannershape-animte">
            <img src="{{asset('assets/images/shape-1.png')}}" alt="shape">
        </span>
        <span class="banner-shape-2 bannershape-animte">
            <img src="{{asset('assets/images/shape-2.png')}}" alt="shape">
        </span>
        <span class="banner-shape-3 bannershape-animte">
            <img src="{{asset('assets/images/shape-3.png')}}" alt="shape">
        </span>
        <span class="banner-shape-4 bannershape-animte">
            <img src="{{asset('assets/images/shape-4.png')}}" alt="shape">
        </span>
        <div class="container">
            <h1>About Us</h1>
        </div>
    </section>
    <input id="card-holder-name" type="text">

    <!-- Stripe Elements Placeholder -->
    <div id="card-element"></div>

    <button id="card-button">
        Process Payment
    </button>

@endsection
@section('script')
    <script src="https://js.stripe.com/v3/"></script>

    <script>
        const stripe = Stripe('{{$stripe_key}}');
        
        const elements = stripe.elements();
        const cardElement = elements.create('card');

        cardElement.mount('#card-element');
    </script>
    <script>
        const cardHolderName = document.getElementById('card-holder-name');
        const cardButton = document.getElementById('card-button');

        cardButton.addEventListener('click', async (e) => {
            const { paymentMethod, error } = await stripe.createPaymentMethod(
                'card', cardElement, {
                    billing_details: { name: cardHolderName.value }
                }
            );

            if (error) {
                // Display "error.message" to the user...
            } else {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });

                $.ajax({
                    url: "{{route('store-user-payment-method')}}",
                    type: "POST",
                    data: {
                        payment_method: paymentMethod.id
                    },
                    success: function(data) {

                    },
                    error: function(data) {

                    } 
                })
            }
        });
    </script>
@endsection