<!DOCTYPE html>
<html>
   <head>
      <title>Sign Up</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontawsome.min.css')}}" />
      <link rel="icon" type="image/gif" href="{{asset('assets/images/favicon.png')}}" sizes="16x16">
      <!-- My CSS -->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/common.css')}}" />
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}" />
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/login-page.css')}}" />
   </head>
   <body>
      <section class="login-outer">
         <div class="register-page login-area">
            <div class="header-area">
               <a href="{{route('home-page')}}" class="d-block text-center">
               <img src="{{asset('assets/images/logo.svg')}}" class="lg-logo" alt="logo">
               </a>
               <h2 class="title text-center">Sign Up</h2>
               {{-- <div class="social-link">
                  <ul class="d-flex justify-content-center">
                     <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                     <li><a href=""><i class="fab fa-google"></i></a></li>
                     <li><a href=""><i class="fab fa-linkedin-in"></i></a></li>
                  </ul>
               </div>
               <p class="text-center mt-4">or open a new account</p> --}}
               <!-- <p class="text">SiteStaff Admin Panel</p> -->
            </div>
            <div class="login-form">
               <form class="row">
                  <div class="col-lg-6 col-md-6">
                     <div class="custom-input-box">
                        <span><i class="fal fa-user"></i></span>
                        <input type="text" name="studentname" class="form-control" placeholder="Student Name*" autocomplete="off">
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6">
                     <div class="custom-input-box">
                        <span><i class="fal fa-user"></i></span>
                        <input type="text" name="parentsname" class="form-control" placeholder="Parent's Name*" autocomplete="off">
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6">
                     <div class="custom-input-box">
                        <span><i class="fal fa-school"></i></span>
                        <input type="text" name="schoolname" class="form-control" placeholder="School Name*" autocomplete="off">
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6">
                     <div class="custom-input-box">
                        <span><i class="fal fa-user-graduate"></i></span>
                        <input type="text" name="grade" class="form-control" placeholder="Grade*" autocomplete="off">
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6">
                     <div class="custom-input-box">
                        <span><i class="fal fa-mobile"></i></span>
                        <input type="text" name="contactnumber" class="form-control" placeholder="Contact Number*" autocomplete="off">
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6">
                    <div class="custom-input-box">
                       <span><i class="fas fa-child"></i></span>
                       <input type="text" name="email" class="form-control" placeholder="Age" autocomplete="off">
                    </div>
                 </div>

                  <div class="col-lg-6 col-md-6">
                     <div class="custom-input-box">
                        <span><i class="fal fa-home"></i></span>
                        <select class="form-control form-select">
                           <option>Select a State*</option>
                           <option>0</option>
                           <option>1</option>
                           <option>2</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6">
                     <div class="custom-input-box">
                        <span><i class="fal fa-home"></i></span>
                        <select class="form-control form-select">
                           <option>Select State first*</option>
                           <option>0</option>
                           <option>1</option>
                           <option>2</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6">
                    <div class="custom-input-box">
                       <span><i class="fal fa-envelope"></i></span>
                       <input type="email" name="email" class="form-control" placeholder="Email Id*" autocomplete="off">
                    </div>
                 </div>
                  <div class="col-lg-6 col-md-6">
                     <div class="custom-input-box">
                        <span><i class="fal fa-lock"></i></span>
                        <input type="password" name="password" class="form-control" placeholder="Password*" autocomplete="off">
                        <div>
                           <i class="fas fa-eye-slash"></i>
                        </div>
                     </div>
                  </div>
                  {{-- <div class="col-lg-6 col-md-6">
                     <div class="custom-input-box">
                        <span><i class="fal fa-lock"></i></span>
                        <input type="password" name="password" class="form-control" placeholder="Confirm Password*" autocomplete="off">
                        <div>
                           <i class="fas fa-eye-slash"></i>
                        </div>
                     </div>
                  </div> --}}
                  <div class="col-lg-12">
                     <label class="mt-4" for="pay"><input type="checkbox" class="form-check-input" id="pay"> I agree to the <a href="javascript:void(0);" class="link-logs">Terms &amp; Conditions </a>  and <a href="javascript:void(0);" class="link-logs">Privacy Policies </a> of SkillsnMore.</label>
                  </div>

                  <div class="col-lg-12">
                     <!-- <input id="authdata" type="hidden" value="Authenticating..."> -->
                     <button class="btn login_btn" type="submit">SIGN UP</button>
                  </div>
                  <p class="text-center mt-4">Already have any account? <a href="{{url('/login')}}" class="link-logs">Sign in</a></p>
               </form>
            </div>
         </div>
         <div>
            <img src="{{asset('assets/images/shapes/boy-num.svg')}}" class="icon-hour d-none d-lg-block">
            <img src="{{asset('assets/images/shapes/arrow-icon.svg')}}" class="arrow-icon">
            <img src="{{asset('assets/images/shapes/arrow-1.svg')}}" class="arrow-1">
         </div>
         <ul class="bg-bubbles">
            <li><img src="{{asset('assets/images/shapes/sp1.svg')}}" class="star2"></li>
            <li><img src="{{asset('assets/images/shapes/sp12.svg')}}" class="abcd-shape"></li>
            <li><img src="{{asset('assets/images/shapes/sp3.svg')}}" class="math"></li>
            <li><img src="{{asset('assets/images/shapes/sp8.svg')}}" alt="category" class="code-shape"></li>
            <li><img src="{{asset('assets/images/shapes/sp4.svg')}}" alt="category"></li>
            <li><img src="{{asset('assets/images/shapes/sp5.svg')}}" alt="category" class="code-shape"></li>
            <li><img src="{{asset('assets/images/shapes/sp13.svg')}}" alt="category" class="code-shape"></li>
            <li><img src="{{asset('assets/images/shapes//sp9.svg')}}" alt="category" class="code-shape"></li>
            <li><img src="{{asset('assets/images/shapes/sp10.svg')}}" alt="category" class="code-shape"></li>
            <li><img src="{{asset('assets/images/shapes/sp11.svg')}}" alt="category" class="code-shape"></li>
         </ul>
      </section>
      <!-- Bootstrap js  -->
      <script type="text/javascript" src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
   </body>
</html>
