{{-- <!doctype html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <title>@yield('title')</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('admin.layouts.partials.css')
</head>

<body>
    @yield('content')
    @include('admin.layouts.partials.js')
</body>

</html> --}}



<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from designreset.com/cork/ltr/demo4/auth_login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Nov 2021 10:06:00 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    @include('user.layouts.partials.css')
</head>
<body class="form">
    
    @yield('content')
    
    @include('user.layouts.partials.js')

    
    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
   

</body>

<!-- Mirrored from designreset.com/cork/ltr/demo4/auth_login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Nov 2021 10:06:00 GMT -->
</html>
