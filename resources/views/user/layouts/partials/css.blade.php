
<link rel="icon" type="image/x-icon" href="{{asset('adminassets/img/favicon.ico')}}"/>
<link href="{{asset('adminassets/css/loader.css')}}" rel="stylesheet" type="text/css" />
<script src="{{asset('adminassets/js/loader.js')}}"></script>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
<link href="{{asset('adminassets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('adminassets/css/plugins.css')}}" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
<link href="{{asset('adminassets/plugins/apex/apexcharts.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('adminassets/css/dashboard/dash_1.css')}}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
@stack('css')