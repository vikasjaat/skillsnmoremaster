	<script src="{{asset('adminassets/js/libs/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('adminassets/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{asset('adminassets/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('adminassets/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{asset('adminassets/js/app.js')}}"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="{{asset('adminassets/js/custom.js')}}"></script>
    <script src="{{asset('adminassets/plugins/apex/apexcharts.min.js')}}"></script>
    <script src="{{asset('adminassets/js/dashboard/dash_1.js')}}"></script>
    @stack('script')
    