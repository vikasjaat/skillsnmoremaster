<?php

use App\Http\Controllers\inst\auth\AuthenticatedSessionController;
use App\Http\Controllers\inst\auth\ConfirmablePasswordController;
use App\Http\Controllers\inst\auth\EmailVerificationNotificationController;
use App\Http\Controllers\inst\auth\EmailVerificationPromptController;
use App\Http\Controllers\inst\auth\NewPasswordController;
use App\Http\Controllers\inst\auth\PasswordResetLinkController;
use App\Http\Controllers\inst\auth\RegisteredUserController;
use App\Http\Controllers\inst\auth\VerifyEmailController;
use App\Http\Controllers\inst\layoutController\InstructorController;
use App\Http\Controllers\inst\layoutcontroller\ScheduleController;
use App\Http\Controllers\inst\layoutController\StudentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::prefix("instructor")->name("instructor.")->group(function () {    
    Route::get('/login', [AuthenticatedSessionController::class, 'create'])->middleware('guest:inst')->name('login');    
    Route::post('/login', [AuthenticatedSessionController::class, 'store'])->middleware('guest:inst')->name('login');    
    Route::get('/logout', [AuthenticatedSessionController::class, 'destroy'])->middleware('instructor')->name('logout');
    Route::group(['middleware'=>'instructor'],function(){
        // -----------------InstructorController------Course Route---------------------------------
        Route::any('/course', [InstructorController::class, 'index'])->name('course');
        Route::get('/course-view/{id}', [InstructorController::class, 'courseview'])->name('courseview');
        Route::get('/add-Course', [InstructorController::class, 'addcourse'])->name('addcourse');
        Route::post('/Save-Course', [InstructorController::class, 'store'])->name('store');
        Route::get('/trash/{id}', [InstructorController::class, 'trash'])->name('trash');    
        Route::get('/restore/{id}', [InstructorController::class, 'restore'])->name('restore');
        Route::get('/trash-course', [InstructorController::class, 'trashview'])->name('trashview');
        // ---------ScheduleController----batch and schedule route------------------------
        Route::get('/show-schedule/{id}', [ScheduleController::class, 'showschedule'])->name('addevent');
        Route::get('/create-class/{id}', [ScheduleController::class, 'createschedule'])->name('createschedule');
        Route::get('/show-class/{batch_name}', [ScheduleController::class, 'showclass'])->name('showclass');
        Route::get('/add-student/{batch_name}', [ScheduleController::class, 'addstudent'])->name('addstudent');
        Route::get('/store-student/{id}/{batch_name}', [ScheduleController::class, 'storestudent'])->name('storestudent');
        Route::post('/save-event', [ScheduleController::class, 'storeschedule'])->name('storeschedule');
        // --------------------------Student-Route--------------------
        // Route::get('/student-course', [StudentController::class, 'index'])->name('addstudent');
        // Route::post('/student-ourse', [StudentController::class, 'store'])->name('addstudent');
        
    });
});
