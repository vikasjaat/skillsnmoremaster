<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home-page');
Route::get('/about-us', 'HomeController@aboutus')->name('about-us');
Route::get('/contact-us', 'HomeController@contactus')->name('contact-us');

Route::prefix('courses')->group(function(){
    Route::get('/', 'HomeController@courseCategory')->name('front-course-category-all');
    Route::get('/detail/{course_name}', 'HomeController@courseDetailPage')->name('front-course-detail');
    Route::get('/scheduledemo', 'HomeController@scheduleDemo')->name('front-course-schedule-demo');
    Route::get('/scheduledemo/{course_name}', 'HomeController@scheduleDemo')->name('course-schedule-demo');
    Route::get('/{category}', 'HomeController@courseCategory')->name('front-course-category');
});

Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/terms-and-conditions', 'HomeController@termscondition')->name('terms-and-conditions');
Route::get('/privacy-policy', 'HomeController@privacypolicy')->name('privacy-policy');

// Route::get('/payments', 'User\UserController@createPayment')->name('payments');
Route::prefix('instructor')->group(function(){
    Route::get('/profile/{slug}', 'HomeController@instructorprofile')->name('instructorprofile');
});

Route::post('/signup-teacher', 'HomeController@signupTeacher')->name('signup-teacher');

Route::post('/createsubscription', 'User\UserController@index')->name('user-create-subscription');
Route::get('/subscribe/{subscriptionId}', 'User\UserController@createPayment')->name('user-create-payment');
Route::post('/savePaymentMethod', 'User\UserController@savePaymentMethod')->name('store-user-payment-method');
Route::get('/thankyou/{subscriptionId}', 'User\UserController@thankYou')->name('payment-thank-you');
// Route::get('testproduct', 'HomeController@testProduct');

Route::get('/dashboard', function () {
    return view('user.dashboard');
})->middleware(['auth'])->name('dashboard');

Route::post('/getcities', 'HomeController@getcities')->name('front-getcities');
Route::post('/getstates', 'HomeController@getstates')->name('front-getstates');


require __DIR__ . '/auth.php';
///.....................start admin Route...................//////////

Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->middleware(['admin'])->name('admin.dashboard');

    Route::get('/order', function () {
        return view('admin.order');
    })->name('admin.order');
    Route::get('/instructor','Admin\InstructorController@index')->name('admin-instructor');
    Route::get('/instructor/create/{id}','Admin\InstructorController@create')->name('admin-instructor-create');
    Route::post('/instructor/create','Admin\InstructorController@store')->name('admin-instructor-store');
    Route::get('/instructor/status','Admin\InstructorController@instatus')->name('instructor-status');
    Route::delete('delete/{id}','Admin\InstructorController@insdelete')->name('instructor-delete');

    Route::get('/categories','Admin\CategoryController@index')->name('admin-categories');
    Route::get('/categories/create/{id}','Admin\CategoryController@create')->name('admin-categories-create');
    Route::post('/categories/create','Admin\CategoryController@store')->name('admin-categories-store');
    Route::get('/categories/status','Admin\CategoryController@categories_status')->name('instructor-categories');
    Route::delete('/delete-category/{id}','Admin\CategoryController@delete_category')->name('delete-category');
    
    Route::get('/courses','Admin\CoursesController@index')->name('admin-courses');
    Route::get('/courses/create/{id}','Admin\CoursesController@create')->name('admin-courses-create');
    Route::post('/courses/create','Admin\CoursesController@store')->name('admin-courses-store');
    Route::get('/courses/status','Admin\CoursesController@courses_status')->name('courses-status');
    Route::get('/category-instructor/{id}','Admin\CoursesController@fetchinstructor');
    Route::delete('/delete-courses/{id}','Admin\CoursesController@delete_courses')->name('delete-courses');

    Route::get('/schedule/{id}','Admin\CoursesController@schedule')->name('schedule-add');
    Route::post('/schedule','Admin\CoursesController@schedulestore')->name('schedule-store');
    Route::get('/batch/{id}','Admin\CoursesController@batchindex')->name('batch-show');
    Route::get('/batch-schedule/{batch_name}','Admin\CoursesController@batchschedule')->name('batch-schedule-show');

    Route::get('/batch-student/{batch_name}','Admin\CoursesController@batchstudent')->name('batch-student-show');
    Route::get('/store-student/{id}/{batch_name}','Admin\CoursesController@storestudent')->name('admin.storestudent');
    Route::post('/schedule/status','Admin\CoursesController@schedule_status')->name('admin-schedule-status');


  

});
require __DIR__ . '/admin_auth.php';
///.....................and admin Route...................////



//------------------instructor Route--------------------------//
Route::get('/instructor/dashboard', function () {
    return view('instructor.dashboard.dashboard');
})->middleware('instructor')->name('instructor.dashboard');
require __DIR__ . '/inst_auth.php';
